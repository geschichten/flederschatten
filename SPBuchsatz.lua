-- This is a sample custom writer for pandoc.  It produces output
-- that is very similar to that of pandoc's HTML writer.
-- There is one new feature: code blocks marked with class 'dot'
-- are piped through graphviz and images are included in the HTML
-- output using 'data:' URLs. The image format can be controlled
-- via the `image_format` metadata field.
--
-- Invoke with: pandoc -t sample.lua
--
-- Note:  you need not have lua installed on your system to use this
-- custom writer.  However, if you do have lua installed, you can
-- use it to test changes to the script.  'lua sample.lua' will
-- produce informative error messages if your code contains
-- syntax errors.

local pipe = pandoc.pipe
local stringify = (require "pandoc.utils").stringify

-- The global variable PANDOC_DOCUMENT contains the full AST of
-- the document which is going to be written. It can be used to
-- configure the writer.
local meta = PANDOC_DOCUMENT.meta

-- Chose the image format based on the value of the
-- `image_format` meta value.
local image_format = meta.image_format
  and stringify(meta.image_format)
  or "png"
local image_mime_type = ({
    jpeg = "image/jpeg",
    jpg = "image/jpeg",
    gif = "image/gif",
    png = "image/png",
    svg = "image/svg+xml",
  })[image_format]
  or error("unsupported image format `" .. img_format .. "`")

-- Character escaping
local function escape(s, in_attribute)
  return s:gsub("[<>&%%\"']",
    function(x)
      if x == '<' then
        return '<'
      elseif x == '>' then
        return '>'
      elseif x == '&' then
        return '&'
      elseif x == '%' then
        return '\\%'
      elseif x == '"' then
        return '&quot;'
      elseif x == "'" then
        return '&#39;'
      else
        return x
      end
    end)
end



-- Helper function to convert an attributes table into
-- a string that can be put into HTML tags.
local function attributes(attr)
  local attr_table = {}
  for x,y in pairs(attr) do
    if y and y ~= "" then
      table.insert(attr_table, ' ' .. x .. '="' .. escape(y,true) .. '"')
    end
  end
  return table.concat(attr_table)
end

-- Table to store footnotes, so they can be included at the end.
local notes = {}

-- Blocksep is used to separate block elements.
function Blocksep()
  return "\n"
end

-- This function is called once for the whole document. Parameters:
-- body is a string, metadata is a table, variables is a table.
-- This gives you a fragment.  You could use the metadata table to
-- fill variables in a custom lua template.  Or, pass `--template=...`
-- to pandoc, and pandoc will add do the template processing as
-- usual.
function Doc(body, metadata, variables)
  local buffer = {}
  local function add(s)
    table.insert(buffer, s)
  end
  add(body)
  if #notes > 0 then
    add('<ol class="footnotes">')
    for _,note in pairs(notes) do
      add(note)
    end
    add('</ol>')
  end
  return table.concat(buffer,'\n') .. '\n'
end

-- The functions that follow render corresponding pandoc elements.
-- s is always a string, attr is always a table of attributes, and
-- items is always an array of strings (the items in a list).
-- Comments indicate the types of other variables.

function Str(s)
  return escape(s)
end

function Space()
  return " "
end

function SoftBreak()
  return " "
end

function LineBreak()
  return "\n"
end

function Emph(s)
  return "\\kursiv{" .. s .. "}"
end

function Strong(s)
  return "\\fett{" .. s .. "}"
end

function Subscript(s)
  return "<sub>" .. s .. "</sub>"
end

function Superscript(s)
  return "<sup>" .. s .. "</sup>"
end

function SmallCaps(s)
  return '<span style="font-variant: small-caps;">' .. s .. '</span>'
end

function Strikeout(s)
  return '<del>' .. s .. '</del>'
end

--function Link(s, src, tit, attr)
--  return "\\url{" .. escape(src,true) .. "}{" .. escape(s,true) .. "}"
--end
-- in printed books it does not make sense to have a name for a link other than the source
function Link(s, src, tit, attr)
  return "\\url{" .. escape(src,true) .. "}"
end

function Image(s, src, tit, attr)
--  return "<img src='" .. escape(src,true) .. "' title='" ..
--         escape(tit,true) .. "'/>"
  return "\\BildBreiteFix{" .. "0.8" .. "}{" .. escape(src, true) .. "}"
end

function Code(s, attr)
  return "<code" .. attributes(attr) .. ">" .. escape(s) .. "</code>"
end

function InlineMath(s)
  return "$" .. escape(s) .. "$"
end

function DisplayMath(s)
  return "\\[" .. escape(s) .. "\\]"
end

function SingleQuoted(s)
  return "›" .. s .. "‹"
end

function DoubleQuoted(s)
  return "»" .. s .. "«"
end

function Note(s)
  return "\\Fussnote{" .. s .. "}"

 -- local num = #notes + 1
 -- -- insert the back reference right before the final closing tag.
 -- s = string.gsub(s,
 --         '(.*)</', '%1 <a href="#fnref' .. num ..  '">&#8617;</a></')
 -- -- add a list item with the note to the note table.
 -- table.insert(notes, '<li id="fn' .. num .. '">' .. s .. '</li>')
 -- -- return the footnote reference, linked to the note.
 -- return '<a id="fnref' .. num .. '" href="#fn' .. num ..
 --           '"><sup>' .. num .. '</sup></a>'
end

function Span(s, attr)
  return "<span" .. attributes(attr) .. ">" .. s .. "</span>"
end

function RawInline(format, str)
  if format == "tex" then
    return str
  else
    return ''
  end
end

function Cite(s, cs)
  local ids = {}
  for _,cit in ipairs(cs) do
    table.insert(ids, cit.citationId)
  end
  return "<span class=\"cite\" data-citation-ids=\"" .. table.concat(ids, ",") ..
    "\">" .. s .. "</span>"
end

function Plain(s)
  return s
end

function Para(s)
  return "" .. s .. ""
end

-- lev is an integer, the header level.
-- \subchapter{}
function Header(lev, s, attr)
  if lev == 1 then
    if attr.class and string.match(' ' .. attr.class .. ' ', ' unnumbered ') then
      if string.match(' ' .. attr.class .. ' ', ' prolog ') then
        return "\\KapitelSpezial{\\normalfont{" .. s .. "}}"
        -- return "\\KapitelSpezial{" .. s .. "}"
      else
        return "\\KapitelSpezial{" .. s .. "}"
      end
    else
      return "\\Kapitel{" .. s .. "}"
    end
  elseif lev == 2 then
    return "\\section*{" .. s .. "}"
  else
    return "\\SzenentrennerLeer\\Begriffliste{Einfach}{" .. s .. "}"
  end
end

function BlockQuote(s)
  return "\\BeginnEinrueckung\n" .. s .. "\n\\EndeEinrueckung"
end

function HorizontalRule()
  return "\\Szenentrenner"--\n\\vertikalDehnen"
end

function LineBlock(ls)
  return '<div style="white-space: pre-line;">' .. table.concat(ls, '\n') ..
         '</div>'
end

function CodeBlock(s, attr)
  -- If code block has class 'dot', pipe the contents through dot
  -- and base64, and include the base64-encoded png as a data: URL.
  if attr.class and string.match(' ' .. attr.class .. ' ',' dot ') then
    local img = pipe("base64", {}, pipe("dot", {"-T" .. image_format}, s))
    return '<img src="data:' .. image_mime_type .. ';base64,' .. img .. '"/>'
  -- otherwise treat as code (one could pipe through a highlighter)
  else
    return "<pre><code" .. attributes(attr) .. ">" .. escape(s) ..
           "</code></pre>"
  end
end

function BulletList(items)
  local buffer = {}
  for _, item in pairs(items) do
    table.insert(buffer, "	\\item " .. item)
  end
  return "\\begin{itemize}\n" .. table.concat(buffer, "\n") .. "\n\\end{itemize}"
end

function OrderedList(items)
  local buffer = {}
  for _, item in pairs(items) do
    table.insert(buffer, "	\\item " .. item)
  end
  return "\\begin{enumerate}\n" .. table.concat(buffer, "\n") .. "\n\\end{enumerate}"
end

function DefinitionList(items)
  local buffer = {}
  for _,item in pairs(items) do
    local k, v = next(item)
    table.insert(buffer, "	\\Begriff{" .. k .. "}{" .. table.concat(v, " ") .. "}\n")
  end
  return "\n" .. table.concat(buffer, "\n") .. "\n"
end

-- Convert pandoc alignment to something HTML can use.
-- align is AlignLeft, AlignRight, AlignCenter, or AlignDefault.
function html_align(align)
  if align == 'AlignLeft' then
    return 'left'
  elseif align == 'AlignRight' then
    return 'right'
  elseif align == 'AlignCenter' then
    return 'center'
  else
    return 'left'
  end
end

function CaptionedImage(src, tit, caption, attr)
--   return '<div class="figure">\n<img src="' .. escape(src,true) ..
--      '" title="' .. escape(tit,true) .. '"/>\n' ..
--      '<p class="caption">' .. caption .. '</p>\n</div>'
  if attr.width then
    return "\\BildBreiteFix{" .. attr.width .. "}{" .. escape(src, true) .. "}"
  else
    return "\\neueBildseite{" .. escape(src, true) .. "}"
  end
end

-- Caption is a string, aligns is an array of strings,
-- widths is an array of floats, headers is an array of
-- strings, rows is an array of arrays of strings.
function Table(caption, aligns, widths, headers, rows)
  local buffer = {}
  local function add(s)
    table.insert(buffer, s)
  end
  -- ToDo: make the number of cs variable
  add("\\BeginnTabelle{ccccc}")
  if caption ~= "" then
    add("<caption>" .. caption .. "</caption>")
  end
  if widths and widths[1] ~= 0 then
    for _, w in pairs(widths) do
      add('<col width="' .. string.format("%.0f%%", w * 100) .. '" />')
    end
  end
  local header_row = {}
  local header_elements = {}
  local empty_header = true
  for i, h in pairs(headers) do
    table.insert(header_elements," " ..  h .. " ")
    empty_header = empty_header and h == ""
  end
  table.insert(header_row, table.concat(header_elements, '&'))
  if empty_header then
    head = ""
  else
    for _,h in pairs(header_row) do
      add(h .. "\\\\\\hline")
    end
  end
  local line_row = {}
  for _, row in pairs(rows) do
    local line_elements = {}
    local line_table = {}
    for i,c in pairs(row) do
      table.insert(line_elements, " " .. c .. " ")
    end
    table.insert(line_row, table.concat(line_elements, '&') .. "\\\\")
  end
  add(table.concat(line_row, "\n"))
  add('\\EndeTabelle')
  return table.concat(buffer,'\n')
end

function RawBlock(format, str)
  if format == "tex" then
    return str
  else
    return ''
  end
end

function Div(s, attr)
  return "<div" .. attributes(attr) .. ">\n" .. s .. "</div>"
end

-- The following code will produce runtime warnings when you haven't defined
-- all of the functions you need for the custom writer, so it's useful
-- to include when you're working on a writer.
local meta = {}
meta.__index =
  function(_, key)
    io.stderr:write(string.format("WARNING: Undefined function '%s'\n",key))
    return function() return "" end
  end
setmetatable(_G, meta)

