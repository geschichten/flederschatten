# Flederschatten

Ein neuer Roman.

## Wie könnt ihr euch hier zurechtfinden?

- In der Datei ``Spoiler-Konzept.md`` liegt das gesamte Konzept. Es
spoilert alles.
- Im Ordner ``chapters`` liegen die Kapitel, also alles, was hinterher im Buch
als Inhalt landet.
- Die Ansicht kann im Browser meist getogglet werden zwischen jener, wie die
Markdownfiles (die mit der Endung \*.md) original aussehen, und einer gerenderten, und
einer Anzeigeansicht, die etwas mehr homepage-like ist. Das dürften
die Zeichen \</\> und das Textdate-Symbol daneben oberhalb der
angezeigten Datei sein, die es umschalten. 
- Im oberen Teil des ``Makefile`` kann die Reihenfolge der Kapitel nachgesehen
werden, in der sie am Ende im Buch landen.

## Aber warum diese Formate? Was machst du damit?

Für mich ist es am einfachsten, in plaintext zu schreiben. Daher schreibe ich
in der Sprache Markdown. Die Sprache hat in dem, was sie kann, gewisse Ähnlichkeiten
mit html. Es gibt gewisse Zeichen, die Text etwa kursiv machen, Überschriften
definieren, sowas.

Diese Dateien sind im Makefile als Inputs definiert. Im Makefile wird definiert, was
mit den Inputs geschehen soll. Gebe ich dann ``make`` in die Kommando-Zeile
ein, wird aus den Dateien alles gebaut, was ich darin definiert habe, wofür sich
Input-Dateien geändert haben, sodass es einen Neubau braucht.

Dieses Makefile baut:

- Eine html-Datei aus allem.
- Eine epub-Datei aus allem.
- Eine tex-Datei aus allem, unter Abwandlung einiger Befehle: Die ``SPBuchsatz.lua``-Datei
setzt etwa Befehle für Kapitel nicht in die LaTeX-eigenen um, sondern in die
Befehle für SPBuchsatz.
- Eine ContentNotes-tex-Datei, um sie gesondert einbinden zu können.
- Eine pdf mit SPBuchsatz. Dazu braucht es die zuvor erstellten tex-Dateien. Für diesen
Schritt ist die Abhängigkeit SPBuchsatz und eine leichte Anpassung von Nöten. Falls
ihr das Makefile benutzen wollt, müsstet ihr den Teil auskommentieren, oder die Abhängigkeit,
SPBuchsatz, in einem Ordner ``deps`` ablegen.
- Des weiteren baut das Makefile aus den Kapiteltiteln einzelne Markdown-Files mit
ein paar definierenden ersten Zeilen, sodass der homepage-generator jekyll daraus
einzelne html-Seiten für meine Homepage generieren kann.
