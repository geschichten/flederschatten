Konzept
=======

**Hier lege ich alle Spoiler ab.**

**Spoiler!**

**Alles wird hier gespoilert.**

*Ich hoffe es ist nun klar, dass Leute sich hier **spoilern** würden.*

**Ab hier beginnen die Spoiler:**

# Weltenbau, außen rum

Die MyrieZange-Welt, also eine utopische Welt. Wir lernen kennen:

- Lebensmitteldrucker.
- Der Hauptcharakter Mirash lebt in einer einsamen Hütte in einem Wald
und trifft Leute ausschließlich in Virtualitäten. Vielleicht haben wir mal
einen Spaziergang. So stelle ich mir das aktuell vor.
- Andere Charaktere, zum Treffen?
- Auf Kontinent Maerdha.

# Lunascerade

## Stil, die Spielwelt

Das Spiel ist MMPG (Massive MultiPlayer Game) und mindestes
ein bisschen gothic. Fork, als die Hauptstadt Maerdhas irgendwann kurz vor der Erfindung
von Autos entwicklungs-technisch abgebogen ist. Es ist immer düster, ob
Zwielicht, Mondschein oder Regen. Bodennebel, Nässe sind eine Sache. Außerdem
wurde die Stadt halb unter Wasser gesetzt. Überall sind Häuser, Leute können
sich darin Basen bauen.

Statt Autos gibt Fahrräder und Fahrzeuge, vor allem Flugfahrzeuge, die irgendwelche
Leute mal entworfen haben, die nie funktioniert haben. Das Flugzeug, wo
die Flügel flattern und am Fahrrad angebaut sind. Solche Dinge. Die in dieser
Parallelwelt funktionieren.

Der Kleidungsstil ist im Wesentlichen schwarz mit bunten Einsätzen, in Spitze oder
Glitzer, mit bauschigen oder kurzen filigranen Röcken, oder eng anliegenden
Hosen. Vor allem tragen alle Leute schicke Masken, mit Glitzer oder Spitze eben.

Dort, wo die Stadt zu Ende ist, wird Arda in
der Welt zwar einigermaßen vollständig, aber
zu einer viel früheren Zeit dargestellt. Direkt um die Stadt herum gibt es lediglich
ein bisschen ältere Landwirtschaft oder so, vielleicht Wald, aber alles geht sehr
zügig in eine Welt vor den Dinos über, wo alles karg ist, Metereoiten auf
den Planeten krachen, Lawa und all dies.

## Regeln

### Spielziel

Es gibt fünf Fraktionen, die gegeneinander kämpfen. Das war's eigentlich.

### Das bisschen Motivation für das Spielziel

Eigentlich braucht es ja keinen weiteren Grund zum Kämpfen, als dass Leute
gern in verkriegten Fraktionen gegeneinander kämpfen wollen. Das versucht
das Spiel auch kaum per Story zu motivieren, es gibt lediglich Anlässe für Akut-Kämpfe:

Es spawnen Puzzle-Stücke an Orten, wo schwer drankommen ist. Es gibt
6 (?) Formen für Puzzlestücke, und wenn eine Fraktion von jeder Form
eines hat, können diese zusammen für eine neu erlernbare Fähigkeit der ganzen
Fraktion eingetauscht werden. Die Fähigkeiten sind dabei nicht sehr
ermächtigend, sondern vor allem fancy. Es macht mehr Spaß mit
mehr Vielfalt.

### Sterben/Respawn

Wer im Spiel stirbt, droppt alles und respawnt (wo?). Außerdem verliert die
Person ein Level.

### Level

Es gibt:

- Eine Lebensbar mit der aktuellen Lebensenergie. Mit mehr Leveln wird diese länger (oder saugt sich weniger schnell leer.) Gelevelt
wird, indem gelernt oder gelehrt wird.
- Es gibt für jede Fähigkeit eine eigene Bar. Die
Fähigkeit muss erst erlernt werden und dann
braucht sich pro Angriff diese Bar leer, die sich mit der Zeit (oder irgendwodurch?) wieder volllädt. Aber nur durch Lehre (siehe später) kann
die Bar selbst verlägert zu werden.

### Elemente/Magie

Die fünf Fraktionen gehören jeweils zu einem
Element. Die fünf Elemente mit zugeordneter Farbe für
Farbeinsätze und Farbe fürs Wappen sind:

- Schwerkraft - Dunkelgrün + Rosa (fürs Wappen)
- Optik - Violett + Gelb
- Reibung - Cyan + Dunkelblau
- Magnetismus - Orange + Schwarz
- Zeit - Schwarz + Hellgrau

Jedes Element kann erlernt werden, aber nur die Bar für Fähigkeiten der eigenen saugt
sich nur in einem reasonable Maß leer.

Die Elemente sind nicht alle gleich stark. Je stärker, desto schwieriger/langsamer geht
das Skillen. Die obige Reihenfolge bezieht sich auf die Stärke.

Entsprechend ergibt sich, dass die Schwerkraftgruppe
sehr groß ist und fluktuiert, weil viele
Leute, die nur jeweils ein paar Stündchen an
ein paar Tagen in der Woche spielen, am ehesten dort
unterkommen. Und die Zeitgruppe ist relativ klein und trotzdem relativ mächtig.

### Beispiele für Fähigkeiten für die Elemente:

- Magnetismus
	- Werfen eines Pols, der für einen Moment alles aus Eisen anzieht.
	- Schleudern von einer wiederholbaren Metallscheibe.
	- Anziehen von Geschossen auf "Blitzableiter".
	- Magnetisierung fremder Schwerter, die dann nicht mehr treffen
	oder wo kleben bleiben.
	- Fesseln, die in der Nähe von Magnetys nicht lösbar sind.
	- Bewegen schwerer Gegenstände.
	- Mechaniken, die nur für sie nutzbar sind.
	- Benutzen von bestimmten Fahrzeugen?
	- Schwingen an mit Wurfmagneten, die an Eisen geworfen werden, Spiderman-like?
	- Sie brauchen nur Ringe an Leute dranzuheften, um sie an sich zu heften.
	- Schwebende Kreisel in Ringfeldern.
	- Stapeln von Pfennigen.
	- Die Stadt ist als Brückenstadt mit Eisenpfeilern durchsetzt, durch
	die Felder erzeugt werden können.
	- Etwas wie Kommunikation über Funk? Sie bekommen Ortsdaten geleakt?
	- Sprühnebel-Metall-Zauber.
	- Maxel? (wie magnetische Pixel)
	- Metal zusammenverformen?
	- Klappleitern (die ein Klackergeräusch machen.)
	- Diamagnetismus, Menschen können geschwebt werden.

- Optik
	- Erscheinungsbild vom Selbst ablösen und einen halben Körper breit
	verschieben.
	- Unsichtbar sein.
	- Dinge, die sehr weit weg sind, nahe sehen.
	- Um die Ecke gucken.
	- Blenden oder Licht ausmachen.
	- Laserdinge.
	- So tun, als wär wesen wer anderes?

- Reibung
	- Vorübergehend Reibung wegnehmen, was dafür sorgt, dass z. B. Leute nicht mehr
	loslaufen können, Leute nicht mehr bremsen können, Schrauben
	aus der Decke rutschen, sowas.
	- Sehr leise Fortbewegen durch Gleiten.
	- Fall bremsen durch Luftreibung.

- Zeit
	- Teleportieren (Im Prinzip wird mit dem Teleport simuliert, dass
	eine Person weitergegangen ist, während sie die Zeit angehalten hat, nur, dass
	natürlich nicht solange das Spiel gefroren wird, sondern einfach der Ortswechsel
	instant passiert.)
	- Gefrieren (Aber Zeitpersonen können die Welt oder Teile davon für ein paar Momente
	bewegungsunfähig machen)
	- Platzieren von Geschossen/geworfenen Messern an Orten, dies
	erst später losfliegen.
	- Die Vergangenheit wieder abspielen lassen.
	- Die verschweißte Tür, die in die Vergangenheit gereist wird, als
	sie nicht verschweißt war, um sie zu durchschreiten. Verschweißerscheinung.
	- Wetter ändern.
	- Apfel faulen.

### Mentoring-System

Ab Level fünf können Personen lehren. Nur durch
Lehre, wenn diese gut bewertet wird, können
sie Bars verlängern, also Fähigkeiten weiter
ausbauen, länger und stärker nutzen, sowas. Es
ist also ein nicht anti- sodern umgekehrt-kapitalistisches
Lehr-/Lernsystem sozusagen: Nicht
die Lernenden sitzen am Losing-End und müssen
alles ertragen, sondern die Lehrenden müssen
zusehen, dass sie mit ihrer Lehre ein genügend gutes Angebot zu machen, um Personen
zu finden, die sich gern von ihnen lehren lassen.

## History und soziale Strukturen

Mein einer Hauptcharakter Flederschatten hat schon vor 15 Jahren mit einer
beta-Version angefangen. In der beta-Version wurden einige magische Tiere in
der Stadt gespawnt, die mit fortgeschrittenem Level eingesammelt hätten
werden können, das damals noch niemand hatte. Und niemand hat schnell genug
gelevelt, bevor die magischen Tiere alle aus der Stadt entschlüpft und
in die vorzeitliche, gefährliche und eher uninteressante Welt da draußen
spaziert sind. Sie wurden aber auch in späteren Versionen des Spiels dort
belassen. Wahrscheinlich haben viele nicht überlebt. Aber besonders
ambitionierte Leute können natürlich viel Zeit investieren und irgendwo auf diesem
Planeten nach welchen suchen.

Eine Generation nach Flederschatten hat eine Gruppe gemeinsam angefangen, Zeit
zu lernen, die schon immer so einen elitären Touch hatte, und den schlimm
ausgebaut hat.

Dann kam das Spielupdate, dass nur noch Leute ab Level 5 lehren dürfen. Der
Hintergrundgedanke war, dass das Gruppen mehr durchmischt, also neue Leute nicht
einfach als Gruppe anfangen und unter sich bleiben können. Ein Update, dass
auch mit diesem Hintergrund schon nicht allen gefallen hat, muss es aber
ja auch nicht.

Jedenfalls hatte das zur Folge, dass der größere Teil der Zeitgruppe immer
elitärer und gateKeepender werden konnte, weil ja Leute, die dazu kommen, sich
von ihnen ausbilden lassen müssen, und da können sie ja effektiv was
gegen tun.

Das wurde den meisten, die mit Flederschatten angefangen haben, zu bunt. Sie
sind zum großen Teil ausgestiegen, umgezogen oder haben von vorn, aber mit
einer anderen Fähigkeit angefangen.

### Schwerkraft-Gruppe

Sehr uneinheitlich, weil so viel Fluktuation.

### Optik-Gruppe

Eine Gruppe, in der viele Leute einen Community-Gedanken toll
finden, und etwa auch ausbilden, wenn es ihnen verhältnismäßig
wenig bringt, oder Heilungen verschenken.

### Reibung-Gruppe

Eine stets unterschätzte Gruppe. Reibung klingt ja für viele gar nicht so beeindruckend,
wie es ist. In der Gruppe sind vor allem Nerds, Personen, die sich selbst
oft nicht so ernst nehmen. Sie stellen manchmal Fallen, indem sie
den Ruf, den sie haben, dass sie eigentlich nicht so besonders sind, ausbauen,
und dann unerwartet Leute ausnocken. Ansonsten machen sie viel albernes
Zeug.

### Magnetismus-Gruppe

Die Gruppe hätte gern mehr Balancing, und versteht sich als zweitstärkste
Gruppe teils in der Verantwortung, die Zeitgruppe im Zaum zu halten. Einige sind
dafür, Flederschatten dabei zu haben, einige nicht.

## Charaktere

### Flederschatten und der Plan

Für Flederschatten jedoch ist die Welt ein Zuhause. Und einfach mal diese ganzen
geskillten Fähigkeiten loslassen fühlt sich auch nach Routinebruch und einfach nicht gut
an. Flederschatten ist durch die viele Spielzeit sehr overpoweret.

Eine Weile hat Flederschatten gecopet, indem Flederschatten ein eigenes Ding gedreht
hat. Zum Beispiel die unsterbliche Kröte gefunden hat.

Aber nun möchte Flederschatten die eigene Zeitgruppe, ausgenommen sich selbst,
besiegen. Besiegen, während es Respawn gibt, ist natürlich schwierig. Damit
ist gemeint, jede der Zeitpersonen unter Level 5 zu kriegen, sodass sie nicht
mehr selbst lehren können und entsprechend keine Fähigkeiten ausbauen.

Dann gäbe es die folgenden Möglichkeiten:

- Es spielen faktisch nur noch 4 Fraktionen gegeneinander.
- Flederschatten könnte als einzige Person neue ausbilden, die Gruppe wird neu.
- Die Spielentwickelnden müssen das 5-Level-Update zurücknehmen oder ändern.

Flederschatten findet vor allem daran aber auch reizvoll, Chaos auszulösen, Spielmechaniken
zu triggern, die noch nie getriggert worden sind.

Flederschatten kann zwar durchaus 20 dieser Zeitleute gleichzeitig aushebeln, aber
nicht in deren Basis, wo sie Wachttürme, Fallen und alles haben.

### Flammenfinger

Flammenfinger gehört keiner Gruppe an. Sey heißt Flammenfinger, weil sey schon ein
bisschen Lichtmagie erlernt hat, und gern mal Flammen auf den Fingern erscheinen
lässt. Das macht nur Show, sonst nichts.

Flammenfinger leitet und organisiert die Dramaturge, einen zentralen Kulturtreff. Hier
gilt Kampfverbot. Einige starke Charaktere haben sich für Flammenfinger bereit
erklärt, entsprechend ein bisschen zu wachen. Waffen müssen beim Betreten abgelegt
werden.

In der Dramaturge gibt es eine Bühne. Es werden regelmäßig Geschichten erzählt. Neue
Fähigkeiten im Theater vorgespielt. Tiere gepflegt. Es ist außerdem ein Treffpunkt
zum Diskutieren über Politik, zum Finden von Allianzen oder Lehrpersonen.

### Die Kröte (Name? Oder namenlos)

Die Kröte ist unsterblich, hat aber sonst keine magischen Eigenschaften. Sie ist
außerdem orange. Flederschatten hat sie gefunden, als Flederschatten aus Frust
wochenlang Spielzeit mit Erforschung der Welt außerhalb verbracht hat, bei
einem Lawa-See oder sowas. Nun hält sie sich bei Flederschatten auf, zum Beispiel
in serer Kapuze.

### Die Ziege, Gate

Die Ziege ist schwarz-weiß gefleckt und hart overpowert, eigentlich noch
viel mehr als Flederschatten, aber nutzt diese
Power sehr random. Sie wohnt in der Dramaturge und übernimmt aufspüren von Waffen,
schädlichen Splashpotions, sowas.

Andere Fähigkeiten umfassen:

- Ist häufig durchsichtig oder hat die Konsistenz von Rauch.
- Gelegentlich Portale schaffen und mit Leuten davonspringen.
- Hat manchmal Flügel.
- Sie weiß manchmal, was ganz woanders passiert.
- Sie kann Leute vorübergehend zum Erstarren bringen.
- Sie hat einmal mit Laseraugen einen Tisch oder so zerlegt, aber
das ist nur einmal aufgetreten.
- Telekinese.
- Sie spielt gern Schach und bewegt die Figuren mit ihrem Blick.
- Sie gibt kryptische Redensart-Ratschläge, die tatsächlich hilfreich
sein können.
- Manchmal ist sie ein Kuscheltier, und wenn sie dann bemalt wird, die
Muster hat sie noch, wenn sie sich zurückverwandelt.
- Sie ahmt anderer Wesen Geräusche nach. Aber obwohl es nie eine
Kuh gegeben hat, muht sie häufig.
- Sie nagt manchmal an abstrakten Dingen, wie der Wirklichkeit (noch
nicht sicher, wie das verständlich umgesetzt werden könnte.)

[tweet](https://twitter.com/karlabyrinth/status/1455610029411811330)

### Der Hecht

### Das Kind

Ist im Outergame nicht Flammenfingers Kind, aber Ingame tun sie so, als ob. Das
Elter vom Kind spielt das Spiel mit, also will das Kind auch. Aber Gemetzel ist
nicht so die Welt des Kindes. Also passt es in der Dramaturge auf die
wenigen Tiere auf, die dort manchmal abgeliefert werden.

### Die Schmiedperson

Schmiedet für alle Leute, ist vielleicht auch fraktionslos. Allerdings
schmiedet die Person in die Waffen für die Zeitleute Sollbruchstellen
ein.

### Haraldin, gehört zur Garde der Dramaturge, Magnetismus-Gruppe

Recht ernsthaft.

### Emeralone

Aus der Optik-Fraktion.

### Zeitkick

Aus der Zeit-Fraktion.

### Mirash

Mirash ist der Hauptcharakter. As kommt neu ins Spiel, und hat sich überhaupt
nicht vorbereitet. Mirash mag es, völlig überfordert zu werden, und genießt
zu einem gewissen Grad scheitern.

## Der Plot

Mirash kommt in die Dramaturge und teilt Flammenfinger mit, dass as
vom Spiel noch nichts weiß, und auch möglichst eher lieber immersiv
gespoilert werden will. Daraufhin freut sich Flammenfinger, weil
dann das Theater der Vorführung von neuen Fähigkeiten der fünf
Fraktionen dann einen Zweck hat.

Mirash entscheidet sich nach der Vorführung für Zeit, weil es cool ist, weil as gern
überfordert wird, -- und weil as eben auch keine Ahnung von den
toxischen Strukturen hat.

As wird daraufhin mitgenommen in die Zeitbasis, wo sie versuchen,
Mirash was beizubringen, aber schnell feststellen, dass sie nicht so
sehr zu einander passen. Mirash hingegen ist schlecht darin, Redflags
zu erkennen.

Der Plan der Zeitfraktion ist, sich gegen Flederschatten zur
Wehr zu setzen, denn sie merken schon, dass Flederschatten gegen
sie arbeitet. Sie schlagen vor, eine Damsel-In-Distress-Situation
für Mirash herzustellen, aus der Flederschatten as retten soll. Auf
diese Weise würde Mirash vielleicht Zugang zu Flederschattens
Basis erlangen. Sie erklären Mirash, dass Flederschatten Villain
ist. Aber auch Flederschatten braucht für das Ausbauen neuer
Fähigkeiten Personen zum Lehren, daher hoffen sie, dass
Flederschatten Mirash aufnimmt. Mirashs Auftrag ist dann, dort
in der Basis unauffällig zu sabotieren.

Die Damsel-In-Distress-Falle klappt. Aber Flederschatten durchschaut
den Plan, lässt Mirash zunächst machen, und erwischt as dann
dabei, zu versuchen zu sabotieren.

Anders als andere Superheld\*innen kommt Flederschatten aber
keineswegs an und behauptet, gut zu sein, oder drängt
Mirash zu einer Seite. Stattdessen klärt Flederschatten über
alles auf und macht alles so transparent, wie es eben geht, sodass
Mirash eine educated decision fällen kann. Flederschatten macht
auch transparent, Dinge zu wissen, die Flederschatten wegen
bestimmter Versprechen nicht teilt.

Und so beginnt Mirashs Karriere in der Doppelspionage.

Nach und nach ergeben sich mehr Dinge. Welche Gruppe vielleicht doch
gegen wen ist, warum, wer hinter Flederschatten steht, wer
eher weniger. Es wird vielleicht über Saviorism diskutiert. Es
werden Allianzen gesucht und Verträge ausgehandelt.

Es wird kein Endkampf ausgeschrieben. Die Frage ist, warum nicht?
Möglichkeiten:

- Das Update findet vorher statt, weil die Entwickelnden den Wink verstehen.
- Der Kampf wird gefochten, während Mirash und Flederschatten anderswo sind. Das
gefällt mir, weil dann die weniger guten Gruppen selbstermächtigter sind.
- Ob im letzten Szenario oder insgesamt könnte der Kampf auch einfach sehr
schnell gehen, weil Mirash nicht als einziges sabotiert hat. Etwa könnte
da die Sache mit der Schmiedperson die Wirkung bekommen.
- Die Ziege macht irgendein sehr seltsames Chaos.

Aber das Outcome wird das von Flederschatten erwünschte sein, nur, was
dann passiert, wird höchstens angerissen.
