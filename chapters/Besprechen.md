Besprechen
==========

Mirash war lange nicht mehr in einer Virtualität eingeschlafen, die
nicht speziell dafür gedacht war. Nach gut einer Woche intensiven
Spielens und der ganzen Aufregung war das wohl nur natürlich, dass
es passierte, wenn etwas unvermittelt zu Entspannung führte und Raum
dafür da war. Vielleicht hätte Mirash ein ungutes Gewissen gehabt, einfach
nach so etwas einzuschlafen, aber Flederschatten schlief
ruhig und entspannt fienerseits. Die Pronomen
hatten sich in der Nacht geändert und waren nun fer, fien(e), fiem,
fien. Sie hatten noch kurz darüber gealbert, was sie alles vorher
besprochen hatten und nicht getan hatten -- beißen zum Beispiel, oder
ausführlicheres Sadistischsein. Sie hatten dabei vorübergehend aus Spaß in dritter
Person von einander geredet und dabei hatte Flederschatten festgestellt, dass
nun andere Pronomen dran wären. Auch schöne Pronomen, fand Mirash.

As blickte sich im Raum um. Die Kerzen waren fast heruntergebrannt, aber
zwielichtiges Morgenlicht fiel durch die dünnen, von Motten angenagten
Vorhänge. Die Kröte schlief auf dem Tisch. Sie hatte die ganze bisherige Zeit
unter dem Bett verweilt.

Der Raum sah abgesehen von der Kröte so unspektakulär aus -- also, was eventuelle
Magie, Rüstungen, Waffen oder sonst etwas in der Richtung betraf. Es war ja an sich
Mirashs Aufgabe, für die Zeit-Fraktion bei Flederschatten zu spionieren. Der
Schrank war geschlossen, dort könnte Mirash vielleicht nun hineinschauen,
heimlich. Aber as legte sich stattdessen wieder hin. Das war nicht
richtig. Der Schlaf war noch Resultat vom romantisch-sexuellen
Miteinander. Dann hätte die Sache eine Auswirkung ins Spiel hinein, das fand
Mirash nicht in Ordnung. Also legte as sich wieder hin und blickte
in Flederschattens Gesicht. Ein Schmunzeln hatte sich dort
hineingestohlen. Flederschatten schlief nicht mehr. Wie lange war
das schon der Fall?

"Ich habe Fragen.", teilte fer mit.

Mirash hatte noch nicht wieder herausgefunden, wie Worte
funktionierten, also nickte as einfach.

"Ist für dich Anfassen, Kuscheln und sowas wie Wangen oder
Hände küssen immer sexuell oder romantisch?", fragte
Flederschatten.

Mirash dachte eine Weile nach und schüttelte schließlich den
Kopf. "Ich hatte ja erzählt, dass ich vor allem sexuelles, aber
auch romantisches Interesse nur an fremden Personen habe. Sobald
ich sie kennen lerne, verblasst das zügig. Aber ich mag dann
trotzdem noch kuscheln und ein wenig zärtlich sein, wenn die
andere Seite das auch möchte."

Flederschatten lächelte. "Darauf zielte die Frage ab. Ich
hätte Lust, dich noch ein wenig zärtlich anzufassen. Ich habe
bestimmte Vorstellungen.", sagte fer. "Aber noch kennen
wir uns nicht gut. Wäre das etwas, was bei dir im Moment
automatisch wieder sexuelle oder romantische Gefühle hervorruft?"

Mirash seufzte vorsichtig. "Zu spät.", sagte as
resigniert.

"Es ist nicht so schlimm.", sagte Flederschatten. "Solange
du damit zurechtkommst, dass ich gerade keine leidenschaftliche
Sache anfangen möchte."

"Komme ich.", sagte Mirash. Und fragte sich direkt im Anschluss, wie
sehr das der Wahrheit entsprach. "Es würde helfen, wenn wir
das Thema wechseln, oder zumindest zeitgleich ein weiteres
hätten. Es sei denn, das passt für dich nicht. Dann muss ich
mich einfach irgendwie beherrschen."

"Ich kenne dich überhaupt nicht.", sagte Flederschatten. "Ich
wüsste bisher drei Themen, über die ich mit dir reden
könnte: Sex und Romantik, was aber
gerade ausgeschlossen ist, es sei denn vielleicht. Es wäre albern oder
sachlich kommuniziert? Politik und Mechanik in diesem Spiel, worüber
ich sehr dringend mit dir reden möchte, aber wir wollten das
eigentlich nicht in romantisch aufgeladene Situationen
hineinmischen. Sollten wir die Regel vielleicht
aufweichen? Und Thermodynamik, aber ich habe den Eindruck, das
war ein Beispielthema, das du nicht deshalb genannt hast, weil
du, wären wir nicht gerade bei Sex gewesen, auf einen Info-Dump
gebrannt hättest."

"Was für Vorstellungen hast du überhaupt?", fragte Mirash.

Flederschatten hob die Brauen. "Ich hätte jetzt damit
gerechnet, dass erzählen kontraproduktiv ist, aber wenn
du willst?" Fer wartete Mirashs Nicken ab, bevor fer
auflistete. "Wenn du im Gegensatz zu mir Berührung an
den Brüsten magst, würde ich sie gern ein bisschen
streicheln, vielleicht vorsichtig drücken, ihre
Konsistenz erfühlen. Klingt das ausreichend unerotisch?"

Mirash kicherte und schüttelte den Kopf. "Das darfst
du machen. Aber erotisch ist es trotzdem."

"Anschließend würde ich dich gern, angezogen oder nicht, einfach
ein bisschen im Arm haben, dir vielleicht gelegentlich ein
Küsschen auf deine Schläfe oder deine Wange oder deine
Nasenspitze geben, während ich mit dir rede.", schloss
Flederschatten.

"Wenn ich bei ersterem rascher atmen und bei letzterem über
irgendetwas albern oder reden darf, klingt das sehr gut.", sagte
Mirash.

"Ich bin gespannt, worüber du alberst." Fer wartete allerdings
keine Antwort ab, bevor fer sich wieder näher an Mirash
heranrückte und mit den Händen saine Brüste untersuchte.

Mirash atmete gar nicht so viel schneller. Irgendwie tat Flederschatten
es sachlich, oder zumindest nicht sexualisiert. Durchaus zärtlich, aber
eher das Gewebe genießend und erforschend, als irgendwomit
aufgeladen. Trotzdem war es, wie sonst auch, ein Gefühl von Zartheit, von
Verletzlichkeit, das von den Berührungen ausgelöst wurde. Ein
intensives Gefühl, dass as gern hatte.

Flederschattens Finger beschäftigten sich vor allem mit dem
weichen Gewebe an den Außenseiten, fuhren nur einmal zart um
die dunkler gefärbte Haut in der Mitte. Als Flederschatten
genug hatte, fuhr fiene eine Hand über Mirashs Schulterpartie und fiene Arme
zogen as sanft in eine Umarmung, in der Mirash mit dem
Rücken an fien gelehnt endete. Flederschatten küsste
zärtlich Mirashs Schläfe. "So.", sagte fer. "Worüber
möchtest du albern."

Es fiel Mirash schwer, sehr schwer, die Gedanken zu sortieren. Flederschatten
hatte recht gehabt. Mirash war gerade nur voll von diesem Spiel und seiner
Politik, vielleicht noch von ihrer romantisch-sexuelle
Interaktion. Alles andere hatte gerade keinen Fokus, schien
eine Ewigkeit her zu sein. "Ich finde den Spagat in meinem Kopf gerade recht
albern", leitete Mirash vorsichtig aber mutig ein, "dass ich mich
in deinen Armen wohl fühle, während du gleichzeitig eine
Person bist, die ich hochgradig unsympathisch finden könnte oder sollte."

"Das ist Reden über das Spiel.", stellte Flederschatten vollkommen
korrekt fest. Aber fer klang nicht, als würde es fien stören. Vielleicht
klang fer leicht amüsiert, aber auch gleichzeitig interessant ernst.

"Stimmt." Mirash holte tief Luft und versuchte, nach etwas zu suchen, worüber
as zuletzt viel nachgedacht hatte, vor Lunascerade, aber als die Lungenflügel
gerade gefüllt waren, legte Flederschatten ein weiteres zartes Küsschen
auf Mirashs Schläfe ab. Mirash versuchte reflexartig mehr einzuatmen, was
kaum befriedigte, weil zuerst ausatmen dran war. "Shit, ey.", murmelte
as. "Sadistisches Wesen, du."

Flederschatten streichelte as vorsichtig, wo fiene Hand eben
gerade lag, selbige nicht viel bewegend. "Schlimm?"

Mirash schüttelte den Kopf, der sich dabei unter Flederschattens Kinn
entlang bewegte. "Ich mag es. Es flasht mich nur sehr."

"Ich wäre eigentlich übrigens dafür, dass wir das Thema mit dem
Spagat besprechen, wenn das für dich okay ist.", teilte
Flederschatten mit. "Ich empfinde das nämlich genau so. Also, mit
vertauschten Rollen."

"Ich könnte eine Person sein, die du hochgradig unsympathisch finden
könntest oder solltest?", fragte Mirash. As konnte Belustigung nicht verbergen. Es
war eine so absurde Situation.

"Ja.", sagte Flederschatten schlicht.

Das Amusement ließ sich Zeit, um sich verdrängen zu lassen, aber machte
dann einem sehr heißen, unangenehmen Gefühl Platz. Mirash schluckte.

"Ich meine, du hast eine Situation kreiert, aus der ich nicht
ohne heftigen Schaden rauskonnte.", hielt Flederschatten fest. "Ich
bin nicht sicher, wie bewusst du dir darüber bist, was für eine
Zwickmühle das war."

"Einigermaßen.", murmelte Mirash. Vorm Ausführen hatte sich das
noch besser angefühlt. As fühlte sich brauchbar in der Lage, sich
zu verteidigen, hatte aber nicht vor, dies ohne Aufforderung zu
tun.

"Ich meine, hätte ich nicht eingegriffen, dann hätte nicht nur ein
Teil der Spielenden geglaubt, dass ich
hinter der Explosion gesteckt hätte, sondern
hätten definitiv Recht damit, dass ich eine Mitschuld getragen hätte, weil
ich die Macht hatte, die Dramaturge zu retten.", leitete
Flederschatten die Analyse ein. "Ich musste mich
also einmischen. Ich war im Zugzwang."

"Ich finde es schwierig, von müssen zu reden.", widersprach
Mirash. "Andernfalls wäre ein Teil der Dramaturge explodiert und
du wärest teilbelastet gewesen. Inwiefern war dir das verboten."

Flederschatten lachte leise, aber mit wenig Freude darin. "Stimmt. Es
gibt kein Gesetz in diesem Spiel, dass mir verbietet, mich
entsetzlich zu verhalten." Völlig im Gegensatz zu dem Gesagten, strich
fer noch einmal sachte über Mirashs Arm.

"Warum hast du dich auf so eine Sache mit mir eingelassen und
bist zärtlich zu mir, wenn du meine Aktion so wenig vertretbar
findest?", fragte Mirash.

Flederschatten zog Mirash als Reaktion fester in die Umarmung, küsste
sehr sachte auf saine Schläfe, dann auf die Partie direkt vor Mirashs Ohr
und flüsterte anschließend hinein: "Chaos!"

Mirashs Körper bebte und atmete und es brauchte ein paar Momente, bis
as wieder halbwegs geradeaus denken konnte. "Das war auch Absicht, oder?"

"Du bist schön, Mirash.", erklärte Flederschatten, nun wieder recht
sachlich. Fer ließ auch die Umarmung wieder lockerer. "In Rosa
wie in Schwarz, aber vor allem in deinem ganzen Dasein, das ich
bis jetzt von dir kennenlernen durfte. Dunkelschön, würde ich
sagen. Du magst halt auch Chaos, oder nicht?" Und dann fügte
fer mit einräumendem Tonfall hinzu: "Und ja, es war Absicht. Sag
bitte jederzeit, wenn das zu viel wird."

Mirash nickte und bestätigte auch die vorherige Frage: "Ich mag Chaos sehr."

"Du hattest sicher interessante Motive dafür, zu versuchen, mir den
Boden unter den Füßen wegzureißen.", fuhr Flederschatten
fort. "Die wüsste ich gern." Fer strich abermals mit der Hand
über Mirashs Arm.

"Ich finde, ein einzelner Charakter sollte nicht so eine Macht
haben, wie du sie hast.", antwortete Mirash. Dann fiel ihm
erst ein, dass as hier nicht mit der Wahrheit hatte rausrücken
wollen. Mist. As fiel so schnell kein Argument ein, dass dieses
nun ausgesprochene halbwegs umdrehen oder entkräften würde. Wie das nun wohl mit
den Chancen aussah, Flederschattens Vertrauen zu gewinnen, um
Gelegenheit zu bekommen, fiene Basis auszuspionieren?
Aber vielleicht war Ehrlichkeit, und
anschließend sich von Flederschatten selbst von etwas anderem scheinbar
überzeugen zu lassen, gar kein so schlechter Weg.

"Valid.", sagte Flederschatten.

"Valid?", fragte Mirash irritiert. Das war etwa das Gegenteil
von hilfreich beim Versuch, sich von Flederschatten scheinbar überzeugen
zu lassen.

"Ja, finde ich.", sagte fer. "Du hast da eben Recht. Egal, wie
herum wir das betrachten: Entweder, ich existiere hier einfach neutral
und unbeteiligt herum. Warum bräuchte ich dann diese Macht? Oder ich bin
Villain, warum ich dann entmachtet werden sollte, ist wahrscheinlich
offensichtlich. Oder ich bin in Wirklichkeit ein gut gesinnter
Charakter. Wenn ich aber dann meine Macht benutze und nicht
abgebe, um Leute zu retten oder ihnen zu helfen, betreibe ich Saviorism. Und dann
gibt es sicher noch viele Wege irgendwo dazwischen. Aber du
hast halt schon recht. Es gibt wohl eher kein Szenario, in dem ich
Einwände dagegen haben sollte, wenigstens etwas entmachtet zu werden."

"Gibst du mir gegenüber gerade sozusagen zu, dass du evil
bist?", fragte Mirash belustigt.

"Schon.", sagte Flederschatten. "So ungefähr."

"Und davor hast du argumentiert, dass du mich unsympathisch
finden müsstest. Weil ich dich in eine Zwickmühlensituation
mit Zugzwang gebracht habe.", fasste Mirash zusammen. "Ist
das dann die Argumentation, dass du mit deiner evil Gesinnung
eben die Guten, aka mich, als feindlich ansehen müsstest?"

Flederschatten gluckste leise und gab Mirash noch
ein Küsschen auf die Schläfe. "Ich liebe deine logische
Argumentation, aber nein.", sagte fer. Und seufzte tief. "Und
nun wird es komplizierter zu erklären."

Mirash gab Flederschatten ein paar Augenblicke Zeit, noch
etwas hinzuzufügen, aber als fer das nicht tat, sagte
Mirash: "Und nun kommt der Part, in dem ich dir einfach
vertrauen soll, dass deine Pläne doch irgendwie einen Sinn
haben?"

"Niemals.", widersprach Flederschatten. "Wenn du die Geduld
und Zeit mitbringst, würde ich dich gern aufklären, vollständig
und transparent, mit Material zum Nachlesen. Ich würde dir
Anlaufstellen nennen, die mir gegenüber
ambivalent bis feindlich gesinnt sind, mit
denen du über mich reden kannst, um mich aus verschiedenen
Blickwinkeln sehen zu können. Ich würde dich motivieren, dir auch
deine eigenen Informationsquellen zu suchen, sodass du dir eine
eigene Meinung bilden und deine eigene Position finden kannst."

Dieses Mal drückte sich Mirash mehr in die Umarmung und legte
die eigene Hand auf die Flederschattens. Fiene Fingerspitzen
waren wieder kalt, also wärmte Mirash sie. "Das Angebot würde ich
gerne annehmen.", sagte as, und fügte dann nachdenklich hinzu: "Ich
erkenne RedFlags schlecht, ich gehe leicht ausversehen Beziehungen
ein, die toxisch sind, ich habe gerade Angst, dass mir das
passiert."

"Ich habe mir das fast gedacht. Alles davon.", murmelte
Flederschatten. "Ich verstehe zum Beispiel jetzt, dass du definitiv
eigene Motive hattest, mich anzugreifen. Aber ich frage mich
trotzdem, wie sehr du auch dadurch beeinflusst worden sein könntest, dass
du dich über die Lehre der Zeit-Fraktion beschwert hast, und
dir nun ein Angebot gemacht worden ist, eine Rolle zu spielen, bei
der dir scheinbar bei jedem Punkt zugehört wurde, sodass es
sich für dich angefühlt hat, als dürftest du dabei nun nicht wirklich
ablehnen."

Mirash fühlte ein Gefühl von Unbehagen wachsen, dass seit Tagen
vorsichtig irgendwo in ihm herumkeimte, aber keinen Raum zum
Wachsen bekommen hatte. As wusste nicht sofort, ob es dort jetzt nur
deshalb wuchs, weil Mirash Erfahrungen mit toxischen Beziehungen gemacht
hatte, und dieses scheinbar etwas tun müssen, weil ihm eingeredet
wurde, dass as es wäre, das jenes gewollt hätte, ihm so sehr
bekannt war, oder ob es hier wirklich auch der Fall war.

"Ich kann völlig falsch liegen.", beschwichtigte Flederschatten. "Ich
kenne die Zeit-Fraktion. Schon lange. Ich habe eine miese Meinung
über diesen Haufen. Ich weiß aber nicht, ob sie mies zu dir waren."

"Ich muss vielleicht gleich eine Spielpause machen.", murmelte
Mirash.

"Jederzeit.", sagte Flederschatten. "Puh, das wird kompliziert, wenn
du dann wieder online kommst. Ich würde nicht auf gut Glück hier
beliebig lange warten. Derzeit würdest du wahrscheinlich
ungefähr nirgends sicher sein. Vielleicht sollte ich dich doch in meine
Basis bringen, wenn du möchtest." Fer überlegte und machte
ein summendes Geräusch dabei.

"Dies ist nicht deine Basis?", fragte Mirash.

"Ist es nicht.", bestätigte Flederschatten und weihte Mirash
in fiene weiteren Überlegungen ein: "Vertrackte Situation. Alternativ bin
ich zu jeder vollen Stunde hier, und wenn dir das nicht zu viel
Eingriff in Inspiel-Dynamik ist und du gern mit mir anknüpfen
möchtest, meinen Schutz haben möchtest, wenn du wieder online
kommst, dann machst du es zu einer vollen Stunde. Andernfalls
kannst du natürlich auch ohne Schutz herumlaufen. Mehr als mehrfach
umgebracht werden, bis du wieder am Anfang bist, passiert dir auch
nicht. Klingt das manipulativ für dich?"

"Ich weiß es nicht.", sagte Mirash ehrlich. "Wäre ich nicht in der
Dramaturge sicher?"

"Flammenfinger würde dich sicher einlassen, wenn nichts los ist, und
dich verstecken, aber sey kann derzeit nicht vertreten, einer Person
Zugang zum Raum zu geben, während viel los ist, die sich zuletzt
hauptverantwortlich für eine geplante und nicht einmal gefakete
Sprengung bekannt hat.", erklärte Flederschatten. "Außerdem ist
dir wahrscheinlich nicht entgangen, dass Flammenfinger und ich, nun,
vielleicht nicht auf der ganzen Ebene zusammenarbeiten, aber
in manchen Punkten schon."

"Weshalb du mir erzählen möchtest, wenn ich nicht zu dir wollte, wenn
ich wieder jointe, weil ich dir nicht vertraute, dann wäre Flammenfinger
keine bessere Adresse?", fragte Mirash.

"Den Schluss möchte ich nicht für dich ziehen, und ich will dich
auch nicht dazu drängen, ihn selbst zu ziehen. Uffz.", machte
Flederschatten. "Mirash, ich bin voreingenommen, vor allem gegenüber der
Zeit-Fraktion. Ich habe auch sonst keine neutrale Sichtweise im Spiel, auch wenn ich
versuchen kann, meine Voreingenommenheit herauszufiltern. Ich gebe
dir gerade bruchstückhaft, weil ich unkoordiniert bin, die Information, die
mir gerade wichtig erscheinen für dich, damit du möglichst schnell
das Spiel verlassen kannst, und eine informierte Entscheidung fällen
kannst, wie du dich verhalten möchtest, wenn du wieder joinst. Ich
habe gerade sehr viel Angst, das nicht gut hinzubekommen."

Mirash nickte und verschränkte die eigenen Finger mit denen
Flederschattens. "Wenn du mich in deine Basis mitnehmen würdest, käme
ich da gerne mit hin.", sagte as schließlich. "Wie ist das? Bist du
dort im Prinzip die ganze Zeit? Oder hast du einen Mechanismus, der dir
sagt, dass ich wieder joine, sobald ich es tue?"

"Ich bin dort nicht die ganze Zeit, aber ich hätte keine Probleme
damit, dort in den nächsten Tagen überwiegend zu sein.", antwortete
Flederschatten. "Ich schlafe normalerweise nicht in Lunascerade, aber da ich
meine Basis nicht gern unbewacht lasse, würde ich dann eine Weile
dort auch In-Game schlafen. Du darfst mich dann gern wecken, wenn du joinst. Oder
dich zu mir kuscheln, wenn dir das lieber ist." In fienen letzten
Worten lag ein Schmunzeln und Mirash hätte mit einem weiteren
Küsschen gerechnet, aber jenes blieb aus. "Wenn in der Dramaturge
Hochbetrieb ist, bin ich dort und passe auf. Hm."

Mirash grinste vorsichtig, wartete ein weiteres Summen Flederschattens
ab, bevor as die unausgesprochene Schlussfolgerung selber
zog: "Wenn ich dann jointe, hast du keine so guten Möglichkeiten, deine
Basis vor mir zu schützen. Ich könnte sie mehr oder weniger ausversehen
in die Luft jagen. Ist es das, worüber du nachdenkst?"

"Ja, so ungefähr.", sagte Flederschatten. "Wobei es vielleicht wahrscheinlicher
wäre, dass du dich selber in die Luft jagst. Ich kann nicht alleine eine
Basis 24/7 durch meine Anwesenheit schützen. Natürlich habe ich einige interessante
Fallen. Am besten zeige ich dir, wie du heile rauskommst, und du entscheidest
dich eben, wenn du wieder joinst, ob du auf mich wartest, rausgehst -- und dann nicht mehr so
einfach wieder reinkommst --, oder irgendwelche gewagten Risiken eingehst. Klingt
das fair?"

Mirash nickte. "Das klingt okay, denke ich."

"Wollen wir dann direkt umziehen, damit du das Spiel verlassen kannst? Willst
du noch irgendwas tun oder fragen vorher?", fragte Flederschatten.

"Ich würde gern betonen, dass ich gesagt habe, dass ich vielleicht eine
Spielpause machen muss. Ich glaube, das 'vielleicht' ist bei dir nicht
angekommen.", erinnerte Mirash.

Flederschatten nickte, was Mirash im Nacken spürte. "Stimmt, das habe ich
zwar wahrgenommen, aber dann irgendwo auf dem Weg vergessen. Es tut mir
leid."

Mirash zog die Hand, in die saine Finger verschränkt waren, sachte zu
sainem Mund und küsste den Handrücken. Bei der Gelegenheit stellte
as fest, dass as die Hände noch nicht ausführlich geküsst hatte und
Lust dazu hatte. Aber nicht jetzt, jetzt war anderes dran. "Ich würde
am liebsten jetzt, wenn das mit deinen Plänen vereinbar ist, in
deine Basis umziehen, mich duschen, meinen EM-Anzug
waschen, etwas essen, einen Spaziergang machen und dann wiederkommen.", sagte
as. "Ich würde mir einen Tee machen und den trinken wollen. Ich
weiß, dass diese Virtualität keine Getränke und so vorsieht. Ich
vermute, um keine Leute auszuschließen, für die Nahrungsaufnahme ein
anstrengendes Thema ist?"

Flederschatten nickte abermals. "Genau. Und aus kulturellen Gründen. Dass
zu sozialen Treffs oder ähnlichem immer Essen oder Trinken gehörte, ist
ja nicht überall so, und dieses Spiel hat sich für eine Sitte
entschieden, bei der das nicht dazu gehört.", bestätigte und ergänzte fer. "Wenn
deine Frage ist, ob wir in meiner Basis zusammensitzen und unsichtbaren
Tee aus unsichtbaren Tassen trinken können, der eigentlich nur im Outernet
existiert, sodass das vielleicht etwas seltsam aussieht, als würden
wir imaginären Tee trinken: Sehr gern. Sowas habe ich schon
oft gemacht."

"Ja, so hatte ich mir das vorgestellt." Mirash küsste doch die Hand
ein weiteres Mal. Ein warmer Schauder durchlief sainen Körper. "Wenn
du gern ein Second Night Stand haben möchtest, sag an."

"Gern, wirklich gern, aber ich möchte das lieber erst, nachdem wir
unseren Spagat ausführlich abgeklärt haben.", sagte Flederschatten. "Wenn
wir uns danach zu gut kennen und du nicht mehr magst, dann ist das eben so. Damit kann ich
dann gut umgehen, aber ohne das vorherige Abklären möchte ich nicht."

"Valid.", sagte Mirash. "Das klingt auch gesünder irgendwie."

Dieses Mal gab Flederschatten Mirash doch ein weiteres, klitzekleines
Küsschen auf die Schläfe. "Du schönes Wesen.", sagte fer leise. "In
deinem Plan war von schlafen keine Rede. Hast du genug geschlafen? Wahrscheinlich
sollte ich mich überhaupt nicht einmischen. Lunascerade ist nur so ein
Spiel, bei dem Leute oft vergessen, dass sie müssen, weil es nie Tag
war und wegen der Spannung und allem."

Mirash seufzte. "Ich werde nicht schlafen können.", sagte as. "Und ja, auch
weil ich geschlafen habe. Sonst wäre ich vielleicht müde genug."

"Ich würde mich auch waschen und etwas essen, und mich dann in der
Basis hinlegen, bis du mich weckst. Ist das in Ordnung?", fragte
Flederschatten.

"Oder ich kuschele mich dann zu dir.", wiederholte Mirash Flederschattens
Worte von vorhin.

"Oder das.", sagte Flederschatten sehr leise und sanft. "Aber wahrscheinlich
wache ich davon auf und kuschele zurück."
