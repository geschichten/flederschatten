Flammenfinger
=============

Das leise Stimmengewirr, dass bei sainem Eintreten für ein paar
Momente fast verstummt war, wurde nun wieder lauter. Irgendwo
schwappte Wasser, als wäre hier ein See im Raum. Das Licht war
schummrig. Mirash trat auf gut Glück einen Schritt zur Seite, um nicht in
der Tür zu stehen. Meistens klappte Orientierung selbst ohne
viel Sehfähigkeit ganz brauchbar bei ihm.

Irgendetwas Kleines trabte auf as zu und berührte as am
Knie. As wich zurück an die Wand neben der Tür. Das mit
dem Schritt zur Seite hatte wohl ganz gut geklappt. Von der Berührung
ausgehend rann ein Kribbeln durch Mirashs Körper, und als es beim
Kopf angekommen war, konnte Mirash plötzlich wieder klar sehen. As leitete
die Adrenalin-Energie, die sonst in Erschrecken geflossen
wäre, direkt in Beobachten und Eindrücke Einsaugen um.

Der Raum war weitläufig und hoch. Am linken Rand erstreckte sich ein schmales
Wasserbecken, das sich zur hinteren, linken Ecke zu einem See vergrößerte. Die
Wasseroberfläche spiegelte das Fackellicht, aber leuchtete auch selbst sachte
cyan. Aus dem See ragte eine kleine Insel mit Bühne, auf die vom trockenen
Teil des Raums eine Brücke führte. Unterhalb der Bühne auf der Insel
lehnte eine Nixe an einem kleinen Hügel und schnitzte
an irgendetwas. Im Raum verteilt standen Tische mit Stühlen. Auf
manchen davon saßen Leute, aber viele Tische standen leer und noch mit
umgekehrt darauf platzierten Stühlen im Raum verteilt. Am anderen Ende des Raums befand sich
eine Art Tresen. Der Aufbau machte den Eindruck einer Bar außerhalb
des Hochbetriebs. Allerdings wirkte der Tresen
nicht wie einer, wo ausgeschenkt würde, eher wie ein Empfangstresen
für ein Theater oder so.

Mirash schrak zusammen, als das Wesen as erneut in der Kniekehle berührte. Die Ziege
war nicht besonders groß. War sie gerade unsichtbar gewesen? Mirashs Sehfähigkeit war auch
außerhalb von Virtualitäten ein wenig eingeschränkt, aber eigentlich glaubte
as, die Ziege andernfalls gesehen haben zu müssen. "Guten Tag,
Ziege.", begrüßte Mirash das Tier.

Die Ziege flackerte als Antwort kurz, und galoppierte, nach einem
intensiven Blick in Mirashs Gesicht, davon, hinter den Tresen zu
einer Person, die gerade aus einer unscheinbaren Tür dahinter
trat. Eine im Gegensatz zur Tür und eigentlich auch zu allen
anderen hier in keinster Weise unscheinbare Person. Sie
trug ein ausladendes, leuchtend rotes Kleid mit
orangenen Einsätzen, eine zarte, rote Maske, die selbst in dem wenigen
Fackellicht glitzerte, und orange-rotes Haupt- und Barthaar, das ihr in
glänzenden Locken bis auf die Schultern fiel. Mirash wagte der
Ziege zu folgen. Die hier Versammelten wurden dabei wieder leiser
und lüfteten ihre Hüte, wenn sie welche hatten. Mirash erwiderte
unsicher die Geste mit sainem Zylinder.

"Eine neue Person. Sei gegrüßt!", sprach die Person in rot, als Mirash
sie erreicht hatte. "Ich bin Flammenfinger. Ich leite die Dramaturge. Kann ich
dir weiterhelfen?"

"Vielleicht." As blickte sich noch einmal unsicher um. Die Leute an den Tischen
schienen neugierig, aber versuchten nun wieder, ihre Aufmerksamkeit auf Mirash
zu verbergen. Als as sich Flammenfinger zuwandte, wurde Flammenfingers Name für
as als Nametag über der hochgewachsenen Person eingeblendet, einschließlich
Pronomen. "sey/ser/sem/sem".

"Bist du neu?", fragte Flammenfinger.

"Sehr neu." Vielleicht hatte sey Mirashs wandernden Blick bemerkt, der
an Orten hängen geblieben war, die für andere schon gewohnt waren. Zum Nametag
etwa.

"Nametags werden angezeigt, sobald eine Person sich vorstellt. Du kannst
sie pro Person deaktivieren, wenn du glaubst, sie überall wiederzuerkennen, aber
das wiederum wird durch die wechselnde Maskerade natürlich erschwert.", erklärte
Flammenfinger. "Bei mir machen es viele. Das Haar bleibt gleich und ist bislang
einmalig."

Wenn es weiter nichts war, war es eine verhältnismäßig harmlose Form von
Machthaben über Personen, deren Namen eins kannte. As überlegte, transparent
zu machen, wie as an Spiele heranging, um nicht zu viel verraten zu bekommen.
Aber noch war es vielleicht nicht zu viel.

"Du wirkst ganz schön unversehrt für eine so ungerüstete Person, die durch
das Kampfgetümmel da draußen musste.", bemerkte Flammenfinger. Sere Stimme
war klangvoll, obwohl sey nicht besonders laut sprach.

Mirash hatte beim Pfeilziehen bemerkt, dass Verschleiß an der Kleidung durchaus
verblieb. Es war ein Loch darin gewesen, und zumindest bis
Mirash die fremde Person getroffen und nichts mehr gesehen hatte, hatte der Dreck der Wände, an
denen Mirash entlanggerutscht war, auf die Kleidung abgefärbt. Das gefiel
as ja durchaus, so von der Ästhetik her. Zerschlissenes Rosa mit
Dreck war eine fast so gute Ästhetik wie Schwarz. Aber als as nun wieder an sich
herunterblickte, wirkte die Kleidung so neu wie zum Spielstart. "Repariert
sich Kleidung mit der Zeit?"

"Oh!", rief Flammenfinger erstaunt aus. "Du bist dann wohl mit den ach so
seltenen und zufälligen Heilzaubern der Ziege zusammengeraten. Manchmal
tut Gate das. Selten, wenn es wirklich nötig wäre." Flammenfinger
zog sich einen Barhocker heran und setzte sich breitbeinig darauf.

Mirash kicherte. Und hielt sich dann auf. "Gibt es Sozialverhalten, dass
Gate verstimmt?" As wiederholte den Namen, wie as ihn gehört hatte. Nicht, wie
die niederelbische Vokabel für Törchen oder Gatter, sondern wie das Ende
von Agate.

"Vielleicht, aber wir wissen es nicht.", sagte Flammenfinger
schmunzelnd. "Es sieht mehr nach Chaos aus."

Mirash fühlte sich im Gespräch etwas verloren, stellte as fest. Es
hatte angefangen mit der Frage, ob as Hilfe gebrauchen könnte, die
halb unbeantwortet und planlos im Raum stand. Mirash überlegte, warum
das so war, und kam zu dem Schluss, dass sie mehr Zeit in Anspruch
nehmen konnte, aber Mirash noch nicht wusste, ob ihm diese Zeit
zugestanden würde. Das ließ sich ja im Vorfeld klären. "Magst du
mir eventuell einen Haufen Fragen beantworten?"

Flammenfingers ganzes Gesicht lächelte. Vielleicht schelmisch? "Dazu
habe ich mich hingesetzt. Ich habe Zeit, bis die Meute von der Straße
reinflutet. Ich schätze, das beginnt in einer halben Stunde."

Mit dem Zeitrahmen konnte Mirash etwas anfangen. As blickte sich
ebenfalls nach einem Hocker um, um sich Flammenfinger
gegenüber niederzulassen. As suchte sich eine eher grazile
Sitzhaltung aus und genoss den schönen Kontrast. Perfektes
Gothic-Bar-Feeling: vertrauter, als es sein sollte, und
doch mit einer guten Prise Verruchtheitsgefühl, in rot und
rosa. "Ich möchte diese Welt von innen kennen lernen, habe
mir also im Vorfeld nichts durchgelesen und keine Tutorials
angesehen."

"Wow!" Flammenfingers Reaktion kam schneller und emotionaler, als
Mirash im Spektrum der Erwartungen für wahrscheinlich gehalten
hatte. "Das wird ein Spaß!" Sey stützte die Ellenbogen auf
den Tisch und legte die Fingerspitzen beider Hände gegeneinander, von
denen dabei jeweils eine Flamme emporzüngelte. Über diese hinweg
betrachtete sey Mirash sehr aufmerksam und vielleicht etwas
belustigt.

"Das wirkt jetzt nicht sehr vertrauenserweckend.", murmelte
Mirash. "Durchaus ästhetisch und atmosphärisch, aber nicht
vertrauenserweckend."

"Was hast du erwartet?", fragte Flammenfinger mit einem Grinsen. Dann
aber verblasste es wieder. "Die Dramaturge ist ein kampffreier Raum. Im
Normalfall. Kein intrigenfreier Raum. Ich bin fraktionslos, mein
Interesse, an irgendwelchen Intrigen beteiligt zu sein, ist gering, was
nicht heißt, dass ich nie an einer beteiligt gewesen wäre.", erklärte
sey. "Das sind vielleicht Argumente, warum ich aus deiner Sicht eine
Person sein müsste, der du am Anfang so am ehesten vertrauen solltest. Aber
es ist nie verkehrt in diesem Spiel, eine gewisse Grundskepsis allen
gegenüber zu haben."

Mirash nickte bloß. Schob den Gedanken darüber, wie recht Flammenfinger
in diesem Punkt haben könnte oder nicht, zur Seite, und wechselte
das Thema zurück auf das Anliegen. "Meine Fragen wären:" As hielt einen Moment inne, um
sie in sainem Kopf zu sortieren. "Was ist die Dramaturge, was ist
ihre Funktion? Lerne ich irgendwann auch, meine Finger mit Flammen temporär
zu verzieren, oder ist es für dich besonders, weshalb du Flammenfinger
heißt? Ist Schwarz eine Farbe, die nur Personen tragen, die kämpfen? Woher
bekäme ich Schwarz? Was bedeutet fraktionslos? Und warum kämpfen die da
draußen und worum?"

Flammenfingers Gesicht grinste bei jeder Frage breiter. Sey ließ
eine weitere Flamme zwischen den Zeigefingern aufzüngeln, als Mirash
seren Namen erwähnte. "Ich grinse, weil ich mich sehr freue." Trotzdem
wirkte Flammenfingers Betonung sehr ernst, als sey dies sagte. "Es
ist einfach eine neue Situation mit sehr viel wunderschönem Potential. Und
zwar: Die Dramaturge ist ein Austausch- und Kulturtreff. Hier werden
Geschichten erzählt, -- Prosa, Balladen oder auch mit Musik --, Leute
lernen sich kennen, es gibt eine Bühne mit
Aufführungen. Die Dramaturge ist außerdem Tauschbörse sowohl für Waffen, Rüstung und
anderen Kram, als auch für soziale Ressourcen wie Lehre. Für den Usecase
'Ich habe keine Ahnung und wüsste gerne Dinge, ich würde gern reinkommen' ist
dies der optimale Ort für Antworten. Und ich kann mich kaum erinnern, wann
sie zuletzt wirklich diesem Zweck gedient hat. Ich danke dir!"

Flammenfinger überschlug die Beine, rückte sich näher an den Tresen
und zuppelte den Rock wieder zurecht. Mirash musste fast grinsen, als
as bemerkte, dass saine Beine dabei automatisch eine mehr gespreadete
Haltung einnahmen. Allerdings verhakte as die Zehen übergestreckt hinter
dem Barhockerstengel. Die rosa Hose spannte dabei angenehm
an den Oberschenkeln. "Die Person, die mich hier hergeleitet hat, war
da weniger ausführlich, aber sagte so etwas schon."

"Weißt du einen Namen?", fragte Flammenfinger.

Mirash schüttelte den Kopf und beobachtete dabei Flammenfingers Gesicht
genau. Keine besondere Regung. Und dann eine, die aber wahrscheinlich
nichts mit der Frage zu tun hatte.

Flammenfinger reckte sich, als
ein kalter Luftzug durch die Dramaturge fegte und die Fackeln zum
Auflodern brachte. Mirash drehte sich auf dem Barhocker herum und
erblickte eine sehr zerfledderte Person in einem Schwarzen Frack
mit dunkelgrünem Revers und gleichfarbigen Manschetten. Solche Begriffe
hatte Mirash im Vorfeld tatsächlich nachgeschlagen. Ein Revers war eine
nach außen geschlagene Kante eines jackenähnlichen Überkleidungsstücks. Die
Manschetten waren so etwas wie umgeschlagene Ärmelenden. Rosa Zeichen, die
Mirash auf die Entfernung nicht erkennen konnte, zierten die Manschetten.

Die Person hob die Arme. Für einen Frack machte das Kleidungsstück
die Bewegung überraschend galant mit, aber trotzdem wurde darunter
Kleidung sichtbar, die Rüstung sein mochte und nicht weniger
zerfleddert wirkte als der Frack. Wie sich Gate wieder
von Flammenfinger und ihm entfernt hatte, hatte Mirash nicht mitbekommen,
aber nun schnüffelte die Ziege an der Hose der Person. Und muhte.

Eine Person, die schon länger im Raum gesessen hatte, erhob sich und
warf einen Blick in ihre Richtung. Mirash vermutete eher, dass
Flammenfinger gemeint war, und sah sich gerade rechtzeitig um, um
sem nicken zu sehen. Sey wirkte, als wäre ein Umstand geklärt, und
richtete sere Aufmerksamkeit wieder auf Mirash, aber Mirash war zu neugierig, was
nun passierte. Immerhin brauchte es nicht lang: Die frisch
aufgestandene Person nahm der dazugekommenen allerlei Waffen ab, schleppte
sie durch den Raum zu ihnen und
verstaute sie in einem Fach hinter dem Tresen. Die nun weniger
gerüstete Person ließ sich irgendwo nieder.

Mirash wandte sich wieder zu Flammenfinger um und versuchte
sich nicht weiter ablenken zu lassen.

"Mein entwaffnendes
Lächeln.", sagte Flammenfinger. "Hier herrscht Waffenverbot außer für die
Garde. Wenn ich Gardierende anlächle, übernehmen sie
meist die Arbeit für mich."

Mirash grinste kurz, den Wortwitz anerkennend.

Die Person, die aus dem Gespräch zu schließen
zur Garde gehörte, schloss eine hölzerne Tür zu Füßen
Flammenfingers im Tresen ab und richtete
sich wieder auf. "Haraldin." Dieses Mal konnte Mirash
beobachten, wie mit der Vorstellung Name und Pronomen über
ihm erschienen. Haraldin lüpfte die Fliegerkappe. Mirash,
die Geste erwidernd, den Zylinder. "Freut mich. Schicker
Hut!" Haraldin sagte es, ohne lesbare Freude im Gesicht, aber
manchmal war das eben so, selbst wenn welche da wäre, wandte
sich wieder um und ging zurück an den Tisch nahe des Eingangs, wo
er zuvor auch gesessen hatte.

"Solche Ablenkung wird ab nun häufiger passieren." Flammenfinger
schmunzelte sanft.

"Ja, ich lasse mich leicht ablenken.", erwiderte Mirash. "Das
ist oft gar nicht mal so praktisch."

Flammenfinger hörte mit dem Schmunzeln nicht auf und beobachtete
as amüsiert. "Nicht so praktisch, in der Dramaturge, indeed. Soll
ich trotzdem noch Fragen beantworten?"

Mirash nickte. "Ich hatte den Eindruck, du merkst, wenn ich
abgelenkt bin. Wenn du damit zurechtkommst, gern."

Flammenfingers Finger gruben sich in das Fell der Ziege, die
plötzlich wieder neben sem aufgetaucht war. Sie flammten dabei
nicht. Mirash beugte sich kurz über den Tresen, denn Flammenfingers Barhocker
war hoch und die Ziege eigentlich zu klein, als dass as
den Rücken hätte sehen dürfen. Aber sie stand mit ihren
Hufen auf einer vielleicht dafür bereit gestellten Kiste. Ihr
Fell war schwarz und weiß gefleckt. Ein Fleck sah allerdings
übertrieben detailliert wie ein Gespenst aus.

"Mit dem Begriff Fraktionen bezeichnen wir die fünf Gruppen, die
sich hier battlen. Du kannst dich zu jeder Zeit im Spiel
für eins von fünf Elementen entscheiden und dann gehörst
du für den Rest des Spiels zu der zugehörigen Fraktion.", erklärte
Flammenfinger.

"Elemente.", sagte Mirash, gespielt skeptisch. "Wie in
Feuer", Mirash blickte betont auf Flammenfingers aneinander
gelegte Finger, "Wasser", Mirash nickte in Richtung
Nixe, "und drei weitere?"

Flammenfinger tat unbeeindruckt. "Kräfte, Optik, Reibung,
Transformation und Zeit.", listete sey trocken auf.

Mirash nahm sich Zeit, zu verinnerlichen, was das für Elemente
waren. Wie cool war das denn? Und welches Element verursachte, dass
eine Person Flammen werfen könnte? "Du gehörst zu Optik?"

"Ich bin fraktionslos." Flammenfinger schürzte die Lippen. Hoffentlich
nur gespielte Verärgerung. Diese Herzlichkeit von vorhin, die gefühlt
den ganzen Raum eingenommen hatte, fehlte nun.

"Entschuldige, du sagtest das schon." Mirash bemühte sich, etwas
bedröppelt zu klingen.

Flammenfinger schüttelte beruhigend lächelnd den Kopf. "Ich spiele
mit dir. Ich foppe dich. Mir fällt es schwer, das abzustellen. Kommst
du damit zurecht."

Mirash spürte ein Ziehen in der Brust. Ein Gefühl, das Mirash
mit Romantik in Zusammenhang brachte, das sich aber eigentlich viel mehr
auf Situationen als auf Personen bezog. Kompliziert zu beschreiben. "Ich
mag das."

Flammenfinger atmete erleichtert aus. "Aber du hast Recht, die Flammen
kommen von der Optik-Magie." Sey nahm die Spitzen der Finger etwas
auseinander und erzeugte neue Flammen, als sey sie wieder aufeinander
klappen ließ. "Prinzipiell können alle jede Magie lernen. Aber mit einem
heftigen Malus oder einer starken Benachteiligung, wenn du so
willst, auf alle Magien, die nicht ihre sind."

"Also, dieses Herumgeflamme, das du da machst, könntest du viel
leichter, hättest du dich der Optik-Fraktion zugeordnet?", rückversicherte
sich Mirash.

"Genau.", bestätigte Flammenfinger. "Insgesamt hast du sowas wie Energiebars
pro Fähigkeit. Wenn du eine Fähigkeit anwendest, saugt sich diese Bar leer. Das
geht sehr fix, wenn du nicht skillst. Und eben besonders fix, wenn die
Fähigkeit einem Element zugeordnet ist, das nicht deines ist. Du kannst
aber die Länge der Bars ausbauen, aka skillen, indem du die Fähigkeit lehrst." Flammenfinger
hob das letzte Wort hervor.

"Oh.", machte Mirash. Grübelte ein paar Momente, und fügte dann hinzu: "Das
ist eine sehr interessante Spielmechanik."

"Indeed." Flammenfinger machte eine dramatische Kunstpause. "Das
ist quasi die Spielmechanik, für die Lunascerade bekannt ist."

Aus Mirash unerfindlichen Gründen brach diese Erklärung Flammenfingers
diese Immersionssache für as nicht. Und as schloss noch etwas. "Du
hast dann wohl Optik viel gelehrt, obwohl du überhaupt nicht
zum Element Optik gehörst?"

Flammenfinger nickte gewichtig. "Oh, ja."

Mirash musste unweigerlich grinsen, weil diese Person vor ihm aus
ihrer Interaktion gefühlt ein Theaterstück machte. "Warum bist
du fraktionslos?"

Diese Frage stimmte Flammenfinger überraschend nachdenklich. Sey
strich sich durch den Bart. Irgendwo in der kaum sichtbaren
Haut zwischen Maske und Haaransatz bildeten sich ein paar
Gedankenfalten. "Also, ich weiß es.", sagte sey. "Ich wurde
das nur lange nicht mehr gefragt und muss mich erinnern. Vielleicht
ist es auch eine lange komplexe Geschichte."

"Geht sie mich etwas an, möchtest du sie erzählen?", fragte Mirash.

"Es berührt mich, dass du das so respektvoll nachfragst." Und
Flammenfinger schaffte es dabei, so viel Gefühl und Erleichterung
in diese Worte zu legen, dass Mirash beinahe geweint hätte, oder
aufgeatmet. "Tatsächlich ist es ein bisschen persönlich, aber nicht
geheim. Ich würde aber gern andere deiner Fragen vorziehen."

"In Ordnung.", stellte Mirash klar. As konnte sich kaum mehr
an die Fragen erinnern und hoffte, dass Flammenfingers
Gedächtnis da besser wäre.

"Ich heiße in der Tat Flammenfinger, weil ich diesen Trick sehr mag
und dafür bekannt bin.", sagte sey.

"Und es handelt sich dabei nicht um Feuer, sondern nur um die
Illusion von Feuer?", riet Mirash.

Flammenfinger schürzte wieder die Lippen, aber grinste und
nickte gleichzeitig. "In einer früheren Version des Spiels hieß
das Element mal Strahlengang. Prinzipiell ist das auch die
passendere Bezeichnung, weil sich mit dem Element auch andere
Dinge aus diesem Welle-Teilchen-Dualismus-Gedöns beeinflussen
lassen, wie Elektro-Magnetismus. Aber das ist sehr fortgeschritten und
folgt auch Regeln aus der Optik. Und wir haben hier in diesem
Zeitalter nicht den Vorteil elektrischer Leitungen, die für
eine ausgefeilte Nutzung von Elektromagnetismus als Element
von Nöten wären, daher wurde das Element Optik genannt, weil
es intuitiver eine Vorstellung vermittelt. Die Idee dahinter ist, dass wir
uns Licht von woanders herholen und so umleiten, dass die
optischen Bilder von etwas entstehen."

Mirash erinnerte sich an die Person, die von der Brücke geregnet
war, geflackert hatte, und deren Erscheinungsbild dann eine
halbe Personenbreite versetzt wieder aufgetaucht war. Und die
deshalb das Geschoss nicht versehrt hatte, das sie durchdrungen
hatte. Eigentlich nur das Abbild von sich selbst. As
nickte. "Ich habe eine grobe Vorstellung."

"Prinzipiell wird das Fackellicht im Raum dunkler, wenn ich
das hier mache." Flammenfinger ließ wieder eine Flamme
auf den zusammengelegten Zeigefingern entstehen, die dann von Fingerpaar zu
Fingerpaar hüpften und beim äußeren wieder verlosch. "Aber
es handelt sich um so kleine Lichtmengen, dass es kaum
merkbar ist."

"Und du hast den Trick erlernt, weil du gern performst? Eindruck
machst, so auf dramaturgische Art?", riet Mirash.

Flammenfinger lachte leise und nickte dabei. "Wenigstens diese
Eigenart von mir ist leicht durchschaubar."

Mirash freute sich innerlich, etwas korrekt durchschaut zu haben. Das
passierte nicht so häufig. Und gerade das fühlte sich plötzlich
für dieses Spiel etwas unbehaglich an. Mirash schätzte es als ein
Spiel ein, in dem diese Fähigkeit sehr hilfreich sein würde, von
der sich as bewusst war, dass as sie nicht so sehr beherrschte. Aber
nun, dann war das eben so. Vielleicht machte es Dinge auch einfach
aufregender.

"Jedenfalls:", setzte Flammenfinger wieder ein. "Zu den Farben. Fraktionslose
Personen dürfen alle Farben tragen außer Schwarz."

"Oh, das ergibt Sinn. So etwas habe ich mir fast gedacht.", fiel
Mirash wieder ein.

"Zu jeder der fünf Fraktionen gehört eine Grundfarbe, ein Wappen
und eine Farbe für dieses Wappen. Es darf ein bisschen in Schattierungen
schwanken, aber ansonsten müssen Zugehörige eines Elements vor allem
Schwarz tragen, erkennbar große Flächen ihrer Kleidung in ihrer
Grundfarbe und das Wappen nur in ihrer Wappenfarbe. Wobei
es verschiedene ähnliche Varianten der Wappensymbole selbst
gibt. Hängt mit Generationen zusammen." Flammenfinger hob
eine Hand mit ausgestreckten Fingern, und entflammte je aufgezähltes
Element einen davon. »Die Farben für die Elemente:

- Kräfte hat Dunkelgrün und als Wappenfarbe Rosa.
- Optik hat Violett und als Wappenfarbe Gelb.
- Reibung hat Cyan und als Wappenfarbe Dunkelblau.
- Transformation hat Orange und als Wappenfarbe Schwarz.
- Und Zeit hat Schwarz und als Wappenfarbe Hellgrau.«

"Das habe ich mir so schnell nicht merken können." Aber natürlich
hatte Mirash bei der Aufzählung versucht, herauszufiltern, zu welcher
Gruppe die Person gehören mochte, die as als erste kennen gelernt
hatte. Schwarz und eine orange Sache an der Kapuze oder im Nacken. Das
klang nach Transformation. Mirash erinnerte sich an die Person, die ihm
ganz am Anfang unter der Brücke Raum verschaffen hatte, indem sie
den Boden in eine Welle verwandelt hatte. Das
mochte auch ein Trick mit Transformation gewesen sein. As erinnerte sich
vage an Orange, fragte sich aber auch, ob sain Gehirn das nun ergänzend
hinzuerfand. Das passierte ihm manchmal.

Sain Geleitschutz zur Dramaturge hatte etwas in der Richtung gesagt, dass
er informiert worden wäre. Das passte womöglich auch dazu. Vielleicht
sprachen sich wahrscheinlicher Personen aus der gleichen Gruppe ab.

"Wahrscheinlich ist es sinnlos, wenn ich frage, welches das coolste Element
ist?", fragte Mirash provokant.

Der Gesichtsausdruck Flammenfingers zeigte, dass sem saine Art wohl gefiel. Sey
gluckste ein wenig. "Jein.", sagte sey. "Heute Abend nach den Straßenkämpfen
gibt es wieder Aufführungen der verschiedenen Fraktionen. Jede Fraktion
stellt ein paar coole neue Tricks auf der Bühne vor." Flammenfinger
deutete auf die Bühne im See, wobei sere ganze Faust und Finger einen
kurzen Moment in Flammen aufging.

"Solche wie mit den Flammenfingern?", fragte Mirash. "Oder welche, die
für Kampf relevant sind?" Und fügte dann noch hinzu: "Das klingt
bewertend, vielleicht, ich meine das aber neutral."

"Eher Show-Tricks. Hier herrscht immerhin Kampfverbot, außer auf
der Bühne selbst.", antwortete Flammenfinger. "Wobei häufig die
Anwendung der Show-Tricks in Kampfsituationen nur etwas ergänzender
Vorstellungskraft bedarf."

Mirash grinste. "Sowas habe ich wohl." As liebte es, Konzepte
zu übertragen.

"Dann fehlt, wenn ich das richtig im Kopf habe, nur noch die Frage, warum
und worum da draußen gekämpft wird." Flammenfinger holte tief Luft
und setzte sich gerader hin. Sere Finger versanken abermals im Fell der
Ziege, die gerade neben sem auftauchte. Dieses Mal hatte
Mirash beobachten können, dass sich die Ziege dort leise, ein
bisschen verschwommen hinbewegt hatte, in einer Art, die bei Mirash
zum Anzweifeln der eigenen Wahrnehmung führte. Es waren bei der Ziege
einfach unauffällig Dinge off, oder etwas zu schnell, oder
verschwommen, wie in Erinnerungen, bloß schon im Geschehen
selbst, so etwas.

"Die Frage war noch unbeantwortet, ja. Aber ich kann mich offengestanden
nicht mehr daran erinnern, ob das alle waren.", gab Mirash zu.

"Sie ist jedenfalls einfach und kompliziert zugleich.", sagte
Flammenfinger. "Es handelt sich hier um ein Kampfspiel, das vorwiegend
Leute spielen, die sich gern battlen."

"Aber es gibt Ausnahmen, wie du?", warf Mirash mutmaßend ein.

Flammenfinger lächelte, aber es wirkte nicht überzeugt. "So
ungefähr. Es haben sich viele soziale Gefüge außerhalb des
klassischen Spielziels entwickelt."

Mirash nickte. Die Ziege war schon wieder weg. Ein Luftzug sagte
ihm, dass weitere Personen die Dramaturge betreten haben mochten, aber
Mirash blickte sich dieses Mal nicht um.

Dafür wirkte Flammenfinger allmählich um so ungeduldiger. "Ich
muss mich gleich um etwas kümmern.", kündigte sey an. "Im Prinzip
wird hier gekämpft um des Kämpfens willen. Die fünf Fraktionen
gegeneinander. Manchmal gibt es Elementeile, die irgendwo spawnen, um
die gekämpft wird. Und es gibt Kombat-Abende, wo in ganz Fork außerhalb
der Dramaturge jede Person gegen jede kämpft. Oder auch kleine Gruppen
gegen andere kleine Gruppen. Dann gibt es bestimmte, verabredete
Trainingskämpfe. Ich würde wirklich gern weitererzählen, aber
die Pflicht ruft."

Mirash blieb kaum Gelegenheit, zu nicken, geschweige denn, sich
in Gedanken die vielen Fragen zu notieren, weil Flammenfingers
letzte Auflistung doch sehr schnell gegangen war.
Hatte as richtig gehört? Elementeile? Sain Verständnis des
Kampfgeschehens in diesem Spiel war nicht unbedingt besonders
klar geworden.

Flammenfinger richtete
sich auf und schritt durch den Raum in Richtung Tür. Und wie sey
schritt. Als gehörte Flammenfinger der Laden, und vielleicht
tat er es auch.

Mirash blieb an der Bar hocken und sog die Atmosphäre in sich
ein. Flammenfinger sprach mit gelassener und einnehmender
Stimme mit verschiedenen Personen, die auftauchten. Wie angekündigt,
strömten nun zunehmend mehr in die Damaturge. Flammenfinger beteiligte
sich eine Zeitlang beim Abtasten und Verstauen von Waffen, manchmal irgendwelchen
Flaschen, vielleicht mit Zaubertränken oder so etwas darin. Sey
wirkte nicht unhöflich oder abgehackt, war nur eben nun
sehr beschäftigt. Personen näherten sich dem Tresen, machten
irgendwelche Angebote oder hatten Nachfragen und Flammenfinger notierte
sich vieles in ein riesiges, altes Buch mit schwerem Umschlag. In einer Geheimschrift, die
zwar wunderschön war, aber die Mirash nicht entziffern konnte.

Mirash fragte sich, ob das eine Geheimschrift war, die Flammenfinger
so zügig zu schreiben gelernt hatte, oder ob es in serer Darstellung
der Virtualität gut lesbar wäre. Ob Geheimschriften lesen und schreiben
auf diese Art vielleicht ein erlernbarer Skill war.

Die Bestellungen waren etwa welche nach einem neuen Degen mit
Antiform-Legierung der Stufe 3. Die gewünschte Länge
wurde mit Variationsspielraum angegeben. Eine Person fragte nach einer
neuen Lern-Person für Bogenschießen, weil sie darauf skillen
wollte. Flammenfinger warf dabei einen Blick auf Mirash, aber
brachte sie nicht direkt in Kontakt. "Ich finde dir wen." Eine
Person wollte irgendetwas mit einem Ausbildungs-Duo, eine
Gruppe aus drei Transformation-Leuten wollte die Bühne für Mandostag
Abend buchen.

Dann endlich gab es eine kurze Lücke im Ansturm. "Längere
Erklärungen werden heute Abend nichts mehr.", stellte Flammenfinger
Mirash gegenüber klar. "Ich würde gern, aber ich habe hier halt
alle Finger voll zu tun." Sey hob die Hände und erneut wanderten
stimmungsvoll Flammen darüber. "Bist du okay damit, morgen
früh wiederzukommen? Ich kenne deine Spielzeiten natürlich nicht."

"Morgen früh ist fein.", sagte Mirash. "Ich habe mir nichts
anderes für die nächsten Wochen vorgenommen."

"Wochen!", betonte Flammenfinger und grinste. "Wunderschön!" Sey
unterstrich es mit einer einladenden Geste und einer halben
Verbeugung. "Ich freue mich sehr, deine Bekanntschaft
machen zu dürfen. Es ist mir eine Ehre." Und fügte hinzu: "Natürlich
ist dies kein Rauswurf. Schau dir gern das Schauspiel an, um die
Elemente kennenzulernen. Meine Empfehlung: Lehn dich erst einmal
zurück und genieß das Spektakel."
