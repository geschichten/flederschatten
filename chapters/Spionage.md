Spionage
========

"Dein Vorschlag.", erinnerte Mirash.

Es war lange her, dass Flederschatten gesagt hatte, er
hätte einen. Seine Pronomen hatten sich wieder
geändert, dieses Mal zu er, sein, ihm, ihn. Inzwischen
lagen sie sich im Bett gegenüber, ein bisschen näher
als sinnvoll für eine halbwegs sachliche Konversation, aber
auch nur ein bisschen. Flederschatten war inzwischen
wieder angezogen, mit dieser weichen Kleidung vom
Vortag, die Mirashs Finger mit ihrer Haptik so sehr anzog.

"Du meinst, ich solle dir vorschlagen, was ich mir gedacht
habe, wie wir es hinkriegen, dass du nicht so abhängig
von mir bist?", fragte Flederschatten.

Mirash nickte. "Genau."

"Ich schlage dir Doppelspionage vor.", sagte
Flederschatten. "Für mich wäre es von Vorteil, eine Person
in der Zeit-Basis zu haben, die sich dort auskennt. Das war, was
ich meinte, wie ich sehr von dir profitieren könnte. Ich will dich da aber
auch nicht reindrängen. Ich würde dir einfach vorschlagen, dass
du hier tatsächlich spionierst. Ich helfe dir dabei nicht, ich
schmeiße dich nur für nichts raus. Und mit den Erkenntnissen, die
du findest, gehst du in die Zeit-Basis und teilst, was du
teilen möchtest. Umgekehrt erzählst du mir alles, was
du über den Schutz der Zeit-Basis weißt, wenn du es mit
mir teilen willst."

Mirash runzelte die Stirn. "Das könnte unter Umständen, je
nach Entscheidung von mir, mit wem ich was teile, sehr
ungünstig für dich werden, oder nicht?"

"Es ist im Rahmen.", widersprach Flederschatten. "Ich kann bei
der Gelegenheit den Schutz meiner eigenen Basis prüfen und
verbessern. Du wirst eine ganz schöne Weile mit allem beschäftigt
sein. Ich meine, du musst es unter meiner Nase tun, während
ich weiß, dass du es vielleicht tun wirst. Du willst gleichzeitig
von mir gelehrt werden. Oder du musst eben irgendwie so
wirken, als würdest du gelehrt, wenn du den Zeit-Leuten nicht
offen legen möchtest, dass das nicht zwischen uns passiert. Und
du musst gelegentlich in der Zeit-Basis
auftauchen. Damit allein bist du schon ziemlich gebremst. Während der Zeit
wird es voraussichtlich viele Gespräche zwischen uns beiden geben. Oder
zwischen dir und anderen hier im Spiel. Irgendwann wirst du
eine Entscheidung fällen, mit der ich dann lebe. Ich bin einverstanden
mit dem Risiko, dass das zu meinen Ungunsten passieren kann, während
ich gleichzeitig die Chance habe, dass es mir stattdessen Vorteile
verschafft. Tut es eben auch so: Deine Gesellschaft, viel noch
einmal frisch selber reflektieren, meine Schutzmaßnahmen
überprüfen und diesem Gerechtigkeitsempfinden nachgeben, von
dem ich immer noch nicht weiß, ob das der richtige Begriff dafür
ist."

Mirash nickte, immer noch mit leicht gerunzelter Stirn. "Ich
denke drüber nach. Aber lass mich das noch einmal in plakative
Worte fassen: Du möchtest als eine Seite von zwei Fronten
eine Person im Haushalt haben, von der du weißt, dass sie
Doppel-Spionage betreibt und noch keine Seite gewählt hat?"

"Fast." Flederschatten hob schmunzelnd einen Finger. Diese
leicht arrogant gespielte Geste, um etwas zu präzisieren, Mirash
mochte sie. "Vier Fronten. Etwa. Sie sind teils ein wenig schwammig."
