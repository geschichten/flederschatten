Planung
=======

"Du bist die Person, die mich in meinen ersten Spielstunden zur
Dramaturge begleitet hat.", stellte Mirash fest. As erkannte
die Person nicht wieder, weil as ja nichts hatte sehen können, aber
Zopf und Farbgebung passte. "Ich dachte, du gehörst zu
Magnetismus, weil ich die Kröte für einen Teil deiner Kleidung
gehalten habe."

Flederschatten lächelte milde. "Ja, das war ich." Dann schwieg
sha erst einmal und betrachtete Mirash eingehend.

Flederschattens Nametag erschien mit den Pronomen sha, shan(e), sham,
shan, sowie einer kleingedruckten Notitz in Mirashs Sichtfeld,
dass sie wechselten, und in Abwesenheit keine verwendet
werden sollten. Shane Kleidung war nicht so sehr verspitzt oder
beglitzert wie die vieler anderer, aber sog Mirashs Blick nicht
weniger auf sich: Flederschatten trug einen weichen Hoodie, an dem
ein Cape befestigt war. Es erinnerte womöglich entfernt an
Fledermausflügel und war vielleicht namengebend. Der Hoodie
oder vielleicht doch das Cape, Mirash war sich nicht sicher, mündete
im Nacken in einer gemütliche Kapuze. Die Ärmel wirkten ein
bisschen zu lang und verdeckten die weichen, eng anliegenden,
fingerlosen Handschuhe zum Teil. Das ganze weiche Auftreten
fühlte sich für Mirash auffangend an, als dürfe as sich
nun ausruhen.

Das war albern. Oder nicht?

"Was sollte das Ganze?", fragte Flederschatten schließlich
sachlich.

Mirash hatte sich Antworten zurecht gelegt, und hätte sie zu
jedem Zeitpunkt bisher abrufen können, aber nun plötzlich war
sain Kopf leergefegt. As senkte den Blick und holte tief
Luft. "Der Zweck der Aktion war natürlich Rufmord." As konnte
ein Grinsen nicht unterdrücken.

Flederschatten grinste nicht, aber wirkte auch nicht besonders
wütend oder so etwas. Eher neugierig. "Und warum?"

Was war das für eine sanfte Stimme. Sie rann weich Mirashs
Rücken hinab. As bezeichnete sich selbst manchmal
als Romantic Slut, und dieser Moment verriet ihm Mal
wieder, warum. Um die Gefühle in den Griff zu bekommen,
atmete as noch einmal tief durch. As blickte auf diese ruhige,
neugierige Mundpartie. "Die Zeit-Fraktion hat mich davon
überzeugt, dass du zu viel Macht hast und ein böser Charakter
bist.", erklärte Mirash und deutete auf das allgegenwärtige
Chaos. Aber das Gesagte fühlte sich fremd an, eher
aufgesetzt. "Hast du vor, Loot einzusammeln?"

Flederschatten blickte sich so rasch um, dass es höchstens
eine Geste sein konnte. "Ich habe genug. Bedien dich, wenn
du willst."

Mirash schüttelte den Kopf. "Sollten hier nicht wertvolle
Elementeile getauscht werden, die nun herumliegen sollten?"

"Glaubst du, die Personen mit den Elementeilen sind hier
geblieben?", fragte Flederschatten. "Ich weiß es natürlich nicht
sicher, aber ich vermute eher, alle, die etwas zu verlieren hatten, haben
die Dramaturge verlassen, bevor ich die Anwesenden zerlegt habe."

"Wo sie dann an deiner statt von Teilen der Zeit-Fraktion
zerlegt wurden?", fragte Mirash.

"Worden wären, wären von jener noch Personen dort gewesen.", korrigierte
Flederschatten. "Gegen Holgem und Rosemahr zu kämpfen,
während ich für die Dramaturge noch genug Reserven übrig
behalten musste, war schon trickreich."

Mirash runzelte die Stirn. "Du hast hier drinnen von den Bomben
mitbekommen, als ich sie erwähnt habe, bist dann zuerst
raus, um Holgem und die anderen
zu bekämpfen, dann wieder rein, um die Bomben zu entschärfen und hast
anschließend hier alles zerlegt?"

Flederschatten schüttelte den Kopf. "Ich habe Holgem schon
beim Verstecken der Sprengsätze beobachtet, was ich kann, weil
ich zu Zeit gehöre. Ich habe sie direkt entschärft, bis auf
einen, dessen Zeitmechanismus ich manipuliert und ihn
anschließend an Holgem befestigt habe. Den Rest kannst du dir ungefähr
vorstellen, nehme ich an?"

"Oh.", machte Mirash.

Flederschatten grinste das erste Mal. Es hielt nicht lange. Mirash
gefiel das Gesicht ohne Lächeln durchaus nicht weniger. Sha
sagte: "Wir haben nicht mehr so viel Zeit, bis die ersten wiederkehren
werden. Bis dahin wüsste ich gern, was wir machen. Was möchtest
du?"

Mit so einer Frage hatte Mirash nicht gerechnet. As
konnte doch schlecht antworten, dass as mit in Flederschattens
Basis wollte. "Ich weiß es nicht.", log as. "Ich glaube, nach
dieser Sache möchte mich die Zeit-Fraktion nicht mehr so gern
um sich haben. Sie haben ziemlich deutlich gemacht, was sie
eigentlich von mir erwarten." Wenigstens war mehr schlecht
gelaufen, als geplant. Flederschatten hatte tatsächlich halbwegs
überzeugend versichert, nicht die Dramaturge in die Luft gejagt
haben zu wollen. "Und ich schätze, ich bin nirgends mehr
willkommen. Vielleicht sollte ich mir einen neuen Account
zulegen."

Flederschatten schmunzelte. "Dabei bist du nun so ein
interessanter Charakter."

Die neue Welle an Glut, die durch Mirashs Körper rauschte, traf
as überraschend unvorbereitet. As schluckte. "Würdest du
mich mitnehmen?" As schloss die Augen. As hatte es
also doch gefragt.

"Ja.", antwortete Flederschatten schlicht. Leise und einfühlsam, als
gäbe es viel zum Einfühlen.

Mirash atmete zitternd ein. "Dann ist es das, was ich möchte."

Flederschatten stuppste die Kröte mit einem Finger sachte
an, die darauf in shane Kapuze hoppste. So klebrig galant, wie
schöne Kröten eben hoppsen. Dann streckte sha einen Arm über den
Tisch aus, die Hand nach oben geöffnet, Mirash entgegen. "Wenn du
Probleme mit schnellen Bildern, Licht- oder Szenenwechsel
hast, schließ die Augen, bevor du die Hand in deine nimmst."

"Zeitreisen wir zu dir?" As blickte auf die zart-weiche, ausgestreckte
Hand. Die Finger waren auf ihrer derzeit oberen Seite rötlich-hellbraun, etwas
dunkler in den Falten bei den Gelenken. Mirash hätte sie gern angefasst, einfach
um die Handschuhe und die Haut zu erfühlen, aber zögerte. As wusste
nicht genau, ob der Grund für das Zögern der Zeit-Effekt wäre, den as gleich kennen
lernen würde, oder die warmen Gefühle, die as nicht mit dem Anfassen verbinden
wollte. Darüber wollte as eigentlich wenigstens ehrlich sein.

Die Hintertür öffnete sich und Flammenfinger betrat die Dramaturge. Das
Kleid wirkte etwas ramponiert aber nicht beschädigt. "Ihr seid im Begriff
zu gehen?", fragte sey. Hinter sem trat nun auch Antagone wieder in den
Raum. Das Gesicht war ein einziges Staunen.

Flederschatten nickte. "Oder hast du noch etwas zu besprechen?"

"Seid lieb zu einander." Flammenfinger klopfte das Kleid ab und hielt
anschließend die Tür auf.

"Ich bin immer lieb, du kennst mich doch." Ein weiteres, schmales
Grinsen stahl sich auf shan Gesicht, das sich nun wieder Mirash
zuwandte. Sha bewegte auffordernd zwei Finger.

Mirash atmete tief durch und ergriff die Hand.

As hatte nicht geplant, die Augen zu schließen, aber die Bilder schossen
zu schnell an ihm vorbei. Vielleicht war Zeit wirklich keine so
gute Elemente-Wahl gewesen, wenn das dazugehörte. Als as den Sog auf
der Hand, und die von der Virtualität angetriebenen, angedeuteten
Laufbewegungen nicht mehr spürte, öffnete as die Augen wieder und
erkannte wenig überraschend die Gegend nicht mehr. Sie waren auf einem unscheinbaren,
überdachten Hinterhof. Flederschatten führte as zwei Stufen
hinab durch eine Tür in eine Halbkellerwohnung. Es war, wie überall
in Lunascerade, dunkel, aber Flederschatten entflammte ein langes
Streichholz und anschließend mit diesem ein paar Kerzen, die in Wandkerzenhaltern
steckten. Im Raum stand ein altes, abgewetztes Sofa, ein Regal mit
ein paar wenigen Büchern darin, ein Schrank, von dem hellblaue
Farbe abplatzte, ein Tisch und ein breites Doppelbett, -- letzte beiden
Möbelstücke waren in einwandfreiem Zustand. Dekadent war das im Vergleich
zur Ausstattung bei der Zeit-Fraktion nicht. Aber sehr gemütlich.
Romantisch vielleicht. Mirash sollte das ansprechen. Oder eine
Spielpause machen und den Bedarf an Romantik oder auch Sex
anderswo decken.

"Setz dich, wenn du magst.", ludt Flederschatten ein, als auch
die Kerzen auf dem Tisch in der Mitte brannten.

"Ich hätte was zu besprechen, was nicht so sehr mit dem Spiel
Lunascerade zusammenhängt.", sagte Mirash. "Wenn das okay ist."

Flederschatten legte das abgebrannte Streichholz in eine
Schale auf dem Tisch, lehnte sich selbst daran, sodass sie
einigermaßen auf Augenhöhe reden können würden. "Worum geht
es?" Sha räusperte sich. "Das klang vielleicht wenig
einladend von meiner Seite. Natürlich
ist das okay."

"Du darfst gern, sehr gern direkt abblocken.", stellte Mirash
klar. "Wie gut kommst du mit dem Gesprächsthema Sex klar?"

Flederschatten runzelte für den Bruchteil eines Moments die
Stirn, dann war das Gesicht wieder glatt und sortiert. "Du
hättest jetzt gern welchen? Mit mir?"

Nun, das war vielleicht nicht schwer zu schließen. Trotzdem
wurde Mirash heiß, nun vor allem wegen der Angst, doch
irgendwie aufdringlich zu sein. "Natürlich
nicht, wenn du nicht möchtest, dann brechen wir das Thema sofort
ab. Ich möchte nichts, wozu du nicht klar consentest, und habe
auch kein Interesse an sexueller oder romantischer Interaktion, wenn sich
innerhalb eines Gesprächs über Grenzen und Bedürfnisse herausstellt, dass
unsere nicht matchen. Harmonieren, zusammenpassen, sowas."

Flederschattens Gesicht umspielte nun ein leichtes Lächeln. "Ich
hatte noch nie Sex mit einer Person, die ich so schlecht kannte wie
dich. Und ich hatte noch nie Internet-Sex. Aber ich kann mir vorstellen, zu
möchten, wenn wir alles gut absprechen."

Wow, dachte Mirash bloß. Und daran, dass in sainem reizüberladenen,
romantisierten Gehirn auch noch irgendwo Platz für besagten Consent-Talk
bleiben musste. As hatte eine Stichpunkt-Liste für sich auswendig gelernt, um
sie in solchen Situationen durchzugehen, aber diese ergänzte as gerade zügig
durch ein paar Punkte, die Mirash zusätzlich in diesem Kontext
klarstellen wollte. "Wow.", sagte as trotzdem erst einmal.

"Selber.", sagte Flederschatten.

"Ich würde gern für dieses Spiel, Lunascerade meine ich, klarstellen, dass
mein romantisches und sexuelles Interesse
von selbigem losgelöst ist. Dass es vor allem physisch, körperlich und
eben auf gewisse Art romantisch, also chemisch ist.", sagte as. "Ich möchte
dadurch keine spieltechnische Beziehung vertiefen. Pläne und Gesinnungen
bezüglich Lunascerade, die ich habe, hatte ich schon vorher, und ändern
sich dabei nicht."

"Du willst damit sagen, wenn du mich heimlich umbringen wolltest, dann
wirst du es nicht währenddessen versuchen, sondern erst, wenn wir die Situation
wieder aufgelöst haben?", fragte Flederschatten. "Aber du wirst den Plan
keinesfalls abhängig vom Sex ändern?"

Mirash grinste. "Ja, sowas."

"Einverstanden." Flederschatten lächelte sanft und nickte. "Wir könnten
dazu natürlich, um es vom Spiel abzugrenzen, in eine andere Virtualität
umziehen."

Mirash grübelte einen Moment stirnrunzelnd. Das war eigentlich keine schlechte
Idee, aber warum fühlte sie sich dann so enttäuschend an?

"Dieser Raum gefällt dir?", fragte Flederschatten.

"Schon sehr. Und du und dein weiches Outfit.", gab Mirash zu. "Aber zum
einen ließe sich viel davon mitnehmen und zum anderen ist vor allem wichtig, dass
du dich vollkommen sicher fühlst. Ich werde gleich noch ein paar weitere
Dinge über mich offen legen, damit du mehr Klarheit hast. Aber lass uns gern
erst besprechen, ob du lieber in eine andere Virtualität möchtest."

Flederschatten stieß sich vom Tisch ab und kam einen Schritt auf
Mirash zu. "Ein Kellerloch in Mitten einer düsteren Stadt, wo Gefahren
lauern, finde ich durchaus sehr atmosphärisch.", sagte sha. "Von mir aus
reicht, wenn wir uns mental von der Spiel-Politik lösen und hier bleiben."

Das war fast zu viel für Mirash. As wich einen Schritt nach hinten
zurück. Bald würde die Wand kommen.

Flederschatten reagierte sofort und lehnte sich zurück an den Tisch. "Das
war zu früh.", sagte sha. "Ich habe keine Erfahrung. Und du bist anziehend, seit
du gesagt hast, was du willst."

"Deine Anziehung reagiert auf Konsens?", fragte Mirash.

Flederschatten nickte.

"Wie cool.", sagte Mirash leise. "Ich wünschte, ich wäre da auch so. Ich
hätte, wenn du nun 'nein' gesagt hättest, wohl eine Spielpause
gemacht, um anderswo zu versuchen, die Bedürfnisse
zu decken. Immerhin funktioniert durch konkret geäußertem Nicht-Konsens einigermaßen, dass
ich Gefühle, die für bestimmte Personen entstehen wollen, in den Griff kriege. Es
ist kompliziert und ich hasse jedes bisschen davon, was nicht leicht kontrollierbar
ist, aber Leute stört."

"Meinst du so etwas wie Fantasien mit konkreten Leuten?", fragte Flederschatten. "Hast
du dir schon Sex oder so etwas mit mir ausgemalt? Ist die Frage zu
persönlich oder offensiv?"

"Sie ist krass persönlich.", gab Mirash zu und beantwortete sie trotzdem. "Ich
habe mir vorgestellt, wie sich deine Haut an der Hand und
deine Kleidung anfühlen mag. Es ist mir unangenehm
darüber zu reden, aber ich habe den Eindruck, Ehrlichkeit ist gerade wichtig."

Flederschatten senkte den Blick. "Es geht hier mehr um Offenheit als
um Ehrlichkeit, oder nicht?" Sha strich sich mit der einen Hand über
die Innenfläche der anderen. "Ich habe den Drang, zu sagen: wenn du willst, komm
her und fühl nach."

Es provozierte. Dass die Erlaubnis nur indirekt formuliert war, dämpfte es etwas, aber
Mirash konnte nicht anders, als in Gedanken hinüberzugehen, bremste den Gedanken,
lokalisierte sich dort, wo as stand und ging in Gedanken schon wieder hinüber. As
schloss die Augen und atmete. Was sollte diese Unkontrolle, dieser Sog? "Ich
habe ausschließlich sexuelles Interesse an mir fremden Personen.", sagte
Mirash stattdessen. Es fühlte sich etwas Roboter-artig an. "Bei romantischem
Interesse ist es ähnlich, das verblasst auch rasch, wenn ich eine Person besser
kennen lerne. Nicht ganz so schnell, wie sexuelles Interesse. Letzteres baut
vor allem auf Neugierde auf, darauf, dass ich noch nicht weiß, wie es sich
mit einer Person anfühlt."

"Also wird das hier ein One-Night-Stand?", fragte Flederschatten.

"Oder es gibt noch ein Second-Night-Stand. Außer bei meinen ersten Dates, bei
denen ich noch nicht wusste, wie das bei mir ist, hatte ich aber nur ein
einziges Mal noch ein Third-Night-Stand.", fasste Mirash zusammen.

"Verblasst dein Interesse nur, wenn du sexuelle Dinge mit einer Person hattest, oder
auch, wenn du sie näher kennen lernst, ohne dass Sex passiert ist?", erkundigte
sich Flederschatten.

"Auch in letzterem Fall.", antwortete Mirash. "Sex fühlt sich für mich mit
Leuten nicht gut an, die ich besser kenne. Dann ist da kein Interesse bei mir."

Flederschatten lächelte sanft und hob den Kopf wieder. Mirash konnte nicht
anders, als dieses Lächeln anzusehen. "Das hört sich für mich nach
einer stressigen sexuellen Orientierung an. Ist Orientierung hier das
richtige Wort? Ist es eine Form von demisexuell?"

"Das perfekte Label habe ich noch nicht gefunden.", antwortete Mirash. "Einfach
ist was anderes. Aber mit doch noch gerade so brauchbaren Mikrolabeln und
guten Profil-Matching-Algorithmen finden sich gelegentlich Leute für so etwas. Ansonsten
habe ich nicht selten Sex mit dafür kreierten, virtuellen, imaginären Personen. Trotzdem
ist so eine Situation wie diese hier sehr besonders und eher selten für mich. Wenn
du immer noch Interesse hast."

"Habe ich.", sagte Flederschatten. "Ich bin auch unsicher, und ich sage noch
nicht zu allem zu. Je nachdem, was du willst und so. Aber wenn dich im
Zweifel schon glücklicher macht, nur meine Hände und meine Kleidung
anzufassen, klingt das schon einmal nach etwas, wozu ich gern konsenten würde."

Mirash sog hastig die Luft ein. "Es wäre", as zögerte und setzte
neu an. "Auch das wäre schon sehr besonders und würde mich glücklich machen, wenn
du es magst."

"Lasst uns über das mehr reden, das, was du noch willst, oder besser, was
wir beide noch wollen.", sagte Flederschatten. "Wie gehst du normalerweise
bei so etwas vor? Du hast mehr Erfahrung."

Mirash lächelte. "Der erste Punkt war, dich deutlich darüber aufzuklären, was für
eine Schlampe ich bin.", sagte as. "Wie oberflächlich. Nicht böse oder
abwertend gemeint. Es ist eine bewusst gewählte Selbstbezeichnung. Es ging
mir darum, dass du weißt, worauf du dich einließest."

"Ich denke, das habe ich jetzt verstanden.", sagte Flederschatten. "Es
hat nichts mit Freundschaft oder partnerschaftlicher Beziehung zu tun. Wir
machen das losgelöst von Spiel-Intrigen, wie auch immer die aussehen. Es
geht darum, körperliche Bedürfnisse zu decken und einen Haufen Hormongedöns
in eine ein- bis zweimalige, gegebenenfalls leidenschaftliche Sexsache
umzusetzen."

Mirash nickte. "Ich arbeite außerdem mit Safe Words."

"Nicht unüblich." Flederschatten nickte. "Ich habe zuletzt
mit der Ampel gearbeitet. Rot für sofortiges, komplettes
Auflösen. Gelb für Innehalten, und wenn das nicht reicht,
langsam Stück für Stück auflösen, bis geredet werden kann. Und Grün, wenn
eine Person mitteilen will, dass etwas sehr okay ist, auch
wenn es nicht gut anderswodurch erkennbar ist."

"Ja, damit habe ich auch viel gearbeitet. Das können
wir so halten.", stimmte Mirash zu.

"Wie häufig hast du Sex?", fragte Flederschatten. "Ist das eine
zu persönliche Frage? Ich weiß nicht einmal genau, warum
ich das wissen will."

"Kommt drauf an, was zählt.", antwortete Mirash. "Mit NPCs
sozusagen etwa einmal in der Woche. Mit Avatars, hinter denen
echte Personen stecken, eher so drei Mal im Jahr, aber dann meist
je zwei Mal."

Flederschatten grinste. "Daraus ergibt sich die nächste viel
zu persönliche Frage: Und wie oft mit Menschen außerhalb des
Internets?"

"Noch nie." Davon gingen die meisten nicht aus und Mirash
gab sich oft nicht die Mühe, deren Vorstellungen zu
korrigieren. "Deine Fragen sind in Ordnung. Du darfst
alles fragen. Wir sind fast alle unangenehmen durch, und
wenn ich irgendetwas nicht beantworten will, sage ich das
einfach."

"Dir war unangenehm, dir vorzustellen, meine Hände
anzufassen.", erinnerte sich Flederschatten. "Ist das so, weil
du eigentlich gern keine Vorstellungen hättest, solange Leute nicht Einverständnis
äußern, aber deine Gedanken schneller sind, als deine Beherrschung?"

Mirash nickte. "Besser hätte ich das nicht auf den Punkt
bringen können."

"Solange du mir nur auf Nachfrage davon erzählst, was du dir
vorstellst, kannst du dir gern mit mir alles vorstellen, was
du willst.", erlaubte Flederschatten.

Ein weiteres Lächeln machte sich auf Mirashs Gesicht breit. "Das
entspannt auf der einen Seite, aber auf der anderen ist vorhersehbar, dass
mein Fantastik-Apparat im Gehirn gleich frei dreht."

Flederschatten kicherte. "Ich bin, wenn ich mich nicht zurückhalte, etwas
sadistisch. Ist das okay für dich."

"Wow, ja!" Mirash fiel erst zu spät ein, dass das nicht uneingeschränkt
stimmte. "Also, kommt auf den Sadismus an. Du hast so ein paar sadistische
Vibes, die ich sehr mag."

Die so lässig halb lehnende Gestalt, verschränkte die Arme und setzte
ein bestimmtes, sachtes Lächeln auf. "Was sind die anderen unangenehmen
Fragen?" Sha hob kurz einen Finger aus der Armbeuge hervor. "Das
war eine Reflex-Frage, als du sagtest, wir wären fast alle unangenehmen
durch. Die ich unterdrückt habe. Diesen Sadismus meine ich. Immer
in die wunden Punkte spielen."

Mirash kicherte. "Nehme ich. Gefällt mir.", stimmte as zu. "Es
wären welche nach meinen Kinks."

Sie blickten sich einige Momente erwartungsvoll an. Mirash vermutete, dass
Flederschatten darauf hoffte, dass as gleich darüber reden würde -- ohne
noch einmal extra danach gefragt zu werden. Aber das Spiel war zu schön,
sha diesen Gefallen nicht zu tun.

"Die da wären?" Flederschatten schürzte gespielt die Lippen.

"Vorweg: für mich ist wichtig, dass so etwas im Spielkontext bleibt und
nicht plötzlich unabgesprochen in einem anderen Rahmen Anwendung findet, auch wenn
eine Situation aufkommen sollte, die sich doch wieder romantisch anfühlt.", stellte
as klar.

Flederschatten nickte. "Verstanden." Sha klang dabei überhaupt nicht
verspielt, was Mirash ein angenehmes Gefühl von Sicherheit gab.

"Ich stehe darauf, wenn im Spiel Leute Dinge nachahmen, die in der
Welt da draußen übergriffig wären.", erklärte Mirash. Ihm wurde
sehr heiß dabei und as senkte den Blick, bevor as fortfuhr. "Ich
stehe auf Gier, ich stehe darauf, wenn Leute mir klar machen, wie
sehr sie etwas von mir wollen." As stockte der Atem. Da war noch
mehr, aber as war verheddert.

"Was interessant ist, denn du hast angefangen, weil du willst.", sagte
Flederschatten leise, mit dieser Weiche in der Stimme.

Mirash blickte kurz auf in dieses Gesicht, aber senkte den
Blick direkt wieder. Eins nach dem anderen. "Ich stehe darauf, etwas
in der Bewegungsfreiheit eingeschränkt zu werden. Nicht gefesselt, aber
sachte gegen Wände gedrückt, festgehalten oder vorsichtig umklammert. Ich
mag es, wenn Leute fordernd sind. Gierig, und", as stockte einen
Moment, "Knie zwischen Beine schieben vielleicht." Endlich wagte as es, wieder
aufzusehen.

Flederschatten stand unverändert an den Tisch gelehnt und blickte
zurück, reagierte nicht sofort. Und dann sagte sha leise: "Ich
mag das auch."

"Das war so ein bisschen meine Hoffnung, als du meintest, du wärest
sadistisch, muss ich zugeben." Mirash versuchte ein unsicheres
Lächeln.

Aber Flederschatten schüttelte den Kopf. "Missverständnis. Mist.", sagte
sha. "Beim Sadismus geht es nicht unbedingt um so etwas. Eher um Teasen
und um fies sein. Weniger darum, zu fixieren oder um die Form von
Dominanz, die Gegenstück zu deinen Kinks ist. Kink war das Wort, das
du für alles davon benutzt hast, richtig?" Flederschatten wartete Mirashs Nicken ab, bevor
sha fortfuhr. "Ich meinte, ich habe eher ungefähr die selben Kinks. Ich
habe sehr, wirklich sehr darauf reagiert, als du mir mitgeteilt hast, dass
du mich anfassen willst. Und wo."

Mirash atmete heftig ein. Und kicherte dann. "Ja, ich würde dich
gern anfassen, wenn du möchtest und mich lässt."

"Und vielleicht lasse ich dich nicht." Flederschatten grinste. "Also, wenn
dich der Entzug quält, und du nicht stattdessen nicht mehr willst, weil
du nur willst, was ich zulasse." Sha zögerte plötzlich kurz und korrigierte:
"Und wenn die Qual, nun, süß für dich ist. Wenn du dich gern quälen
lässt."

Mirash senkte den Blick. "Es kann funktionieren. Tease And Denial funktioniert
bei mir nicht immer. Mein Inneres reagiert da manchmal mit Trotz drauf. Aber
du darfst gern probieren."

"Und wie sieht das mit Wechseln aus, was etwa das gegen Wände oder auf
Unterlagen drücken angeht?", fragte Flederschatten.

Ihrer beider Blicke wanderten bei dem Wort 'Unterlagen' zum Bett. Dann
trafen sie sich wieder und sie grinsten beide.

"Ja, ich drücke auch gern Leute gegen Wände, wenn sie dabei schmelzen
und ich das sehe oder sowas.", sagte Mirash flapsig. "Ich experimentiere
überhaupt gern. Wenn du mit mir irgendetwas bestimmtes ausprobieren
möchtest, dann ist die Wahrscheinlichkeit hoch, dass ich das möchte. Es
ging gerade spezifisch um meine Kinks. Die versetzen mich noch einmal
in ein etwas unkontrollierteres Mindset."

"Für Kontrollverlust bin ich wiederum sehr gern verantwortlich." Flederschatten
rieb sich die Hände.

Mirash kicherte. "Ich glaube, wir werden Spaß haben."

Flederschatten nickte, wirkte dann aber plötzlich wieder ernster. "Ich wollte
schon immer Mal etwas bestimmtes ausprobieren.", sagte sha. "Und zwar mag
ich erste Male und habe mich gefragt, woran das liegt. Ich mochte besonders
die ersten Male, bei denen ich mit einer anderen Person sehr unsicher war, was
wir wollen. Dagegen mochte ich die ersten Male weniger gern, wo wir von
Anfang an wussten, wir wollen küssen oder dies oder jenes und das einfach
gemacht haben. Du wirkst jetzt schon skeptisch.", Flederschatten unterbrach
den Redefluss.

"Ein wenig." Mirash hatte tatsächlich die Stirn gerunzelt. "Ich bin eigentlich
lieber eine Person, die vorher gern relativ genau abspricht, wozu es kommen
darf und wozu es ganz sicher nicht kommen soll. Ich bin gerade sehr unsicher, wie
das mit dem, was du da beschreibst, zusammen gehen kann."

"Indem du mich zum Punkt kommen lässt." Flederschatten grinste wieder einen
kurzen Moment. "Ich würde gern ausprobieren, vorher abzusprechen, was wir
machen, aber die Dinge trotzdem nicht sofort umsetzen. Ich glaube, ich
habe starke romantische Gefühle, bevor gewollte physische Interaktionen
tatsächlich ausgeführt werden. Und ich wollte immer Mal ausprobieren, wie
sich mit romantischen Gefühlen in dieser Art spielen lässt."

"Also du magst quasi auch gern geteaset werden?", schloss Mirash vorsichtig, dieses
Mal erst, als as sicher war, dass Flederschatten ausgesprochen hatte.

Sha wirkte nachdenklich und gab ein untermalendes "Hm" von sich. "Irgendwo
hast du recht, aber es fühlt sich nicht wie das gleiche an. Es ist weniger
ein Entziehen, als eine Langsamkeit, ein Fallenlassen in Erwartungen, bevor
etwas passiert. Ein Auskosten eines Moments davor." Flederschatten runzelte
die Stirn. "Vielleicht ergibt das alles keinen Sinn. Und es ist einfach
Teasen."

"Vielleicht ein bestimmtes Teasen.", überlegte Mirash. "Ich finde, es ergibt
schon Sinn. Und mir macht die Unterhaltung gerade sehr viel Spaß."

Flederschattens Haltung wurde weicher. "Mir auch.", sagte sha sanft. "Was
sind denn Dinge, die du gern tun würdest, und Dinge, die eher nicht
passieren sollten?" Wieder wirkte sha kurz nachdenklich und fügte
hinzu: "Was für technische Möglichkeiten brauche ich?"

"Oh.", machte Mirash. "Daran habe ich noch nicht gedacht. Natürlich
bieten nicht alle EM-Anzüge Einsätze und sind auch grundsätzlich
verschieden ausgestattet, wegen verschiedener Bedürfnisse an Sensorik. Klassisch
natürlich die sensorisch sehr sensiblen Stellen und erogenen
Zonen betreffend. Mein EM-Anzug schließt sauber um Brüste ab, sitzt
sehr gut im Schritt, und ich hätte da auch entsprechende Einsätze für
filigrane penetrative Zwecke. Er ist auch in der Lippentechnik
ziemlich neu und gut. Weil ich eben ausschließlich
virtuell Leute treffe für so etwas. Ich hatte noch nie ein Date mit
einer Person, die da nicht so ausgestattet ist, wie sie das jeweils
will."

"Ich habe keine Einsätze. Ich habe Lippenhaut-Gedöns, das ist nicht
brandneu, aber taugt, um mit Lippen Dinge recht präzise zu ertasten. Ich
habe das vorwiegend, weil ich mit Lippen gern stimme. Sagt dir das was?" Sha
hatte das 's' von 'stimmen' stimmlos ausgesprochen. Es war ein
vom Niederelbischen Stimming^[Es ist eigentlich Englisch und eingedeutscht, und
beschreibt Emotionsregulierung durch bewusst zugeführte Reize. Dazu können
zum Beispiel Haare drehen, auf Beißringen rumkauen und so etwas gehören. Es
gibt auch eine Reihe Stimm-Toys.] abgeleitetes Wort.

Mirash nickte. "Ich habe einige liebgehabte Wesen im Umfeld, die stimmen.", sagte
as. "Ich habe gerade das Gefühl, irgendwie Druck gemacht zu haben. Du
wirkst bei dem Thema unsicher. Ich möchte gerne klarstellen, dass EM-Anzüge
alle in einer Weise ausgestattet sind, dass damit viele Dinge möglich
sind, die ich mag, und ich nicht mehr für mich erhoffe oder so etwas. Es macht mir
nur gelegentlich Spaß. Es geht hier nicht darum, was du bräuchtest, sondern
was mit dem, was du hast, möglich ist."

Flederschatten nickte. "Ich bin nur nervös, das ist alles.", sagte
sha. "Ich muss den Anzug wahrscheinlich ohnehin hiernach erst einmal
waschen."

Mirash grinste. "Jetzt schon?"

"Ja, jetzt schon." Flederschatten fiel in das Schmunzeln ein. "Du
bist halt heiß." Den Blick sehr genau auf Mirashs Körperhaltung
gerichtet, legte sha den Kopf schief und hob wieder einen Finger
aus der Verschränkung. "Deshalb habe ich das gesagt. Damit
das passiert."

Mirash versuchte aus dem Knäuel aus Belustigung, Feuer und stockendem Atem, das
as war, wieder aufzutauchen. "Du bist gut! So gut!"

"Eben nicht nur im Spiel overpowert.", sagte Flederschatten.

Mirash stockte der Atem, als as auffiel, was mit dem Element Zeit noch
machbar wäre. "Du könntest binnen Augenblicken unerwartet
vor mir stehen."

"Das klingt nach einem Plan." Flederschattens Stimme hatte einen
sachlichen Ton wiedergefunden und erinnerte: "Du hast nur die Anzugfrage
halbwegs beantwortet."

"Ich werde Mal sehr konkret, in Ordnung?", fragte Mirash.

Flederschatten nickte. "Ich bitte darum."

"Ich habe eine Vagina und ich mag nicht, wenn da etwas eingeführt
wird, was größer als ein Finger ist. Ich denke, ich werde mich vor
der Session nicht noch einmal umziehen, weshalb Einführen eine
Sache wäre, von der nur du etwas mitbekämst. Deshalb würde ich das
für heute gern ganz lassen.", beschloss Mirash.

Flederschatten nickte wieder. "Ich habe auch eine Vulvina. Ich bin, nach
manchen Definitionen zumindest, inter, was sich bei mir vor allem
in einem etwas selteneren Hormonspiegel auswirkt, oder auch in
anderer Körperfett- und Muskulaturverteilung. Ich weiß nicht, ob
das Grund dafür ist, oder dass ich manchmal so etwas wie Dysphorie
habe, was meine Brüste betrifft, aber ich mag nicht gern, wenn
Fokus auf ihnen liegt, und sie fühlen irgendwie vergleichsweise
wenig. Ich mag, wenn Mal eine Hand außen davon entlangstreicht, aber
alles andere finden irgendwie viele andere toll, ich aber nicht."

"Das klingt nervig.", sagte Mirash, aber konkretisierte dann
zügig: "Also nicht für mich, sondern dass du auf eine wohl
unfreiwillig experimentelle Art herausfinden musstest, dass
du etwas nicht magst, wovon viele denken, es müsste toll
für dich sein. Bei mir hat so etwas zumindest viel Zweifel
ausgelöst, ob an meiner Art oder meinem Körper vielleicht
etwas nicht stimmte."

"Wow." Flederschatten wirkte plötzlich traurig, dass es Mirash
fast erschreckte. "Ja! Ja, das trifft genau meine Erfahrung. Und
ja, es war nervig und anstrengend und ich fühle mich gerade
verstanden."

Mirash hätte Flederschatten ja gern eine Umarmung angeboten. Aber
war das im Moment hilfreich? Oder würden sie daraus vor fertiger
Absprache irgendetwas anfangen. Mirash nickte einfach und
kam sich dabei seltsam fehl am Platz vor.

"Ich hatte deine Listung unterbrochen und mit meinen Brüsten
angefangen. Wie geht deine weiter? Oder soll ich mit meiner
fortfahren?", fragte Flederschatten.

"Fahr gern mit deiner fort.", beschloss Mirash.

"Ich mag küssen, aber ich mag keine Zungenküsse.", sagte
Flederschatten. "Beziehungsweise, es kommt darauf an, was schon
als einer gilt. Ich mag, wenn meine Lippen kurz mal von einer
Zungenspitze berührt werden, schon, aber nicht, wenn Zungen
zu sehr hineingedrängt werden oder sehr aktiv sind." Sha
runzelte schon wieder die Stirn und die Ergänzung ließ
nicht lange auf sich warten: "Das spielt keine Rolle, weil ich
keinen EM-Fortsatz für Zungen habe, oder?"

"Du würdest meine Zunge dort spüren können, wo du EM-Anzug
hast. Und ich würde deine verhältnismäßig brauchbar dort
spüren können, wo ich EM-Anzug habe, weil Virtualitäten
auch von Bildern aus Berührung ergänzen können, nicht nur
über die Übertragung von EM-Anzug-Daten.", erklärte Mirash. "Aber
wenn du kein Mundinnenstück hast, oder Alternativ ein modulares
EM-Tuch, das eben Dinge auch mit etwas Physik nachbildet, dann
spürst du an der Zunge nichts und das finden zumindest viele etwas
seltsam."

"Ich habe nichts dergleichen. Hast du eins davon?", fragte Flederschatten.

"Letzteres.", antwortete Mirash. "Die EM-Tuch-Variante ist aber
weniger perfekt. Die Haptik bleibt für die Zunge halbwegs Tuch. Ich habe
Mal mit einem Mundinnenstück experimentiert, aber das fühlte
sich für mich nicht so gut an. Da muss noch dran gefeilt werden. Fazit
jedenfalls, ich könnte dich, außer an den Stellen, wo du nicht
mit EM-Anzug bedeckt bist, mit der Zunge liebkosen oder anlecken, und
wir beide würden das brauchbar spüren. Umgekehrt schwieriger."

"Würde dir was fehlen, wenn ich dich einfach nicht anlecke?" Flederschatten
übernahm ohne Zögern die Wortwahl. Sie grinsten wieder beide.

Mirash schüttelte den Kopf. "Gar nicht. Wenn ich dich richtig
verstanden habe, möchtest du vor allem im Mund nicht ausführlich
angeleckt werden, aber woanders schon?"

"Eigentlich auch anderswo nicht ausführlich.", widersprach Flederschatten. "Kannst
du mir zeigen, wenn wir soweit sind, wie du das machen würdest, und
ich sage, ob ich es mag?"

Mirash nickte. "Schon.", sagte as, aber runzelte zögerlich
die Stirn. "Ich stelle mir also gerade
diese Sache mit dem Abwarten vor. Zum Beispiel, einfach hypothetisch
erstmal, wir liegen sehr lange dicht nebeneinander
auf dieser Unterlage, die wir aus welchen Gründen auch immer bisher nicht
einfach Bett genannt haben, und nähern uns sehr langsam an, bis
ich dir vielleicht vorsichtig die Nasenspitze küsse. Und dann
sagst du: Hey, das war mir zu wenig Zunge, da kannst du ruhig eine
Nuance mehr."

Flederschatten lachte, und Mirash, das das ja beabsichtigt hatte,
stieg mit ein. "Für später, denke ich.", sagte sha. "Ich denke, wenn
du mir die Nasenspitze küsst, wird das ohne Zunge erstmal wundervoll
sein. Ich mag deine Vorstellung. Auch, wenn ich der Szenerie ein
Intro vorweg verpassen würde, in dem wir diese Sache umsetzen, in der
ich mit Zeitmagie zu schnell und sehr dicht vor dir lande. Und du
meine Hände anfasst. Und vielleicht unsicher meine weiche Kleidung. So
stelle ich mir das gerade vor." Und in Mirashs schnelleren Atem
hinein fügte sha hinzu: "Dein Körper wirkt schonmal einverstanden."

"Ich auch." Mirash merkte, wie die eigene Stimme etwas rau klang.

"Und dann machen wir da so lange vorsichtige Anfass-Dinge in dicht, ohne, dass
die Gesichter etwas berühren, bis das ausgekostet ist, und ziehen
aufs Bett um, wo du irgendwann meine
Nasenspitze küsst. Wir fahren sehr langsam fort, bis wir irgendwann
vielleicht doch einer unbarmherzigen Leidenschaft nachgeben -- oh, ich
glaube die Lyrik geht mit mir durch --, die uns aneinander
schweißt und uns küssen und anfassen lässt. Ein Knäuel, in dem wir
abwechselnd die jeweils andere Person mal nach unten drücken, den
Kopf festhalten, Finger in Haare verkrallen, in den Hals beißen. Moment, bin
ich zu schnell? Mag dein Körper auch Halsbisse?"

"Sehr, aber die funktionieren wieder nur mit EM-Schiene." Mirash wusste
nicht, wie as nun überhaupt noch im Stande war, zu sprechen.

"So eine habe ich. Für Stimming. Sollte aber auch dafür taugen, ein
bisschen an dir herumzunagen." Flederschatten grinste, und Mirash
war sich ziemlich sicher, dass es eine befriedigte Reaktion auf
Mirashs Körperreaktionen war.

"Wow.", hauchte as. "Unter den technischen Voraussetzungen gäbe es
für mich nichts mehr zu besprechen. Wie sieht das mit dir aus? Gibt
es noch Tabus oder so etwas?"

Flederschatten legte kurz die Finger an die Lippen, als Geste
fürs Nachdenken, die aber auch eine gewisse Erotik innewohnen
hatte. "Nichts, was mir gerade einfiele und im Zweifel gibt
es noch Safe Words."

Mirash nickte. Ein paar Momente sahen sie sich schweigend an. Mirash
nickte noch einmal. Dann stieß sich Flederschatten von der Tischkante
ab und eröffnete das Spiel. In Mirash verabschiedete sich einiges
an Kontrolle. As tauchte in ein Mindset aus Verlangen, Gier und
Genuss, das jetzt auch voll und ganz erlaubt und erwünscht war.
