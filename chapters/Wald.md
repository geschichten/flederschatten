Wald
====

In Mirashs Spielraum -- dem Raum, in dem die EM-Felder zusammen mit dem
EM-Anzug Lunascerade und andere Virtualitäten spürbar machten -- lag
eine Matratze mit ein paar weichen Decken und Kissen und einer
Kuschelmöwe. Direkt darüber befand sich ein Fenster, das nicht
völlig durchsichtig war, weil auch in jenes das Drahtgeflecht für
die EM-Felder eingearbeitet war. Aber es kam dadurch Licht hinein. Mirash
hatte ein Problem mit Räumen, in die kein natürliches Licht fiel, -- was
vielleicht interessant war angesichts der Tatsache, dass as sich irgendwas
bei ein bis drei Dittel des Tages in Virtualitäten befand. Phsychologische
Effekte waren manchmal stark und seltsam.

Fenster in Spielräumen jedenfalls waren eher selten. Dafür hatte
die Hütte, in der Mirash wohnte, überhaupt nur drei Räume: dieses
Spielzimmer, eine Küche mit gerade ausreichend Raum für einen Tisch für zwei und
ein Bad. Es gab nicht einmal einen Flur, die Funktion übernahm die
Küche ebenfalls.

Es war also nicht viel, aber Mirash brauchte auch nicht viel. As
brauchte vor allem Abgeschiedenheit. Jederzeit die Möglichkeit, die
ganze soziale Welt für eine Weile auszuschalten und sich in der
Realität in einem Wald irgendwo weit draußen auf dem Land zu
befinden. Die Hütte stand nicht allein in diesem Wald, das wäre vielleicht
schwierig geworden, wenn Mirash gleichzeitig einen Anschluss ans
Spinstromnetzwerk und vor allem für den Lebensmitteldrucker
haben wollte. Es war ein kleines Waldhüttendorf aus ungefähr 20 etwas
verstreuten Hütten, und das nächste Dorf war eine gute halbe Stunde
zu Fuß entfernt. Mirash kannte die meisten der
anderen Leute, die hier lebten, vom
Sehen. Manchmal gab es Unterhaltungen darüber, wie sie gemeinsam
das Gebiet pflegen wollten, wer sich wie um welches Gestrüpp
kümmerte. Manchmal wurde as gefragt, ob as eine Person im Falle
eines Notfalls in das nächste Dorf fahren würde, oder ab und an
Mal vorbeischauen würde, ob das alte Mau drei Hütten weiter
es noch hinbekam, sich gut um sich zu kümmern. Inniger wurde es
nicht und das passte Mirash gut in den Kram.

Mirash setzte sich auf die Matratze und lehnte sich mit einem
Kissen im Rücken an die Wand, die Möwe auf dem Schoß
streichelnd. "Wie geht es dir?", fragte Mirash.

Die Kuscheltiermöwe antwortete nicht. Das war Mirash gewohnt.

Sie störte sich allerdings auch nicht daran, wenn Mirash sainerseits
davon erzählte, wie es ihm ergangen war. Und es war sehr
entspannt, ihr davon zu erzählen. Technisch gesehen Selbstgespräche, aber
das ließ die Möwe auch unkommentiert. Es sprach ja auch nichts dagegen.

"Kurzzusammenfassung mit Übertreibungsmodus.", leitete Mirash ein. "Ich
habe mich derbst in eine Person verliebt, die potenziell Super-Villain
in diesem Spiel ist. Ich habe Mal wieder irgendwelche RedFlags nicht
gesehen, also von irgendwelcher Seite nicht mitbekommen, wann ich in
eine Ecke gedrängt oder manipuliert worden bin, und ich weiß nicht
einmal von welcher." Mirash machte ein kurzes nachdenkliches Geräusch. "Eine
bisher recht gute Methode, das herauszufinden, war immer Gefühle
ernst zu nehmen."

Wenn Mirash nicht aussprach, worüber as nachdachte, ergoss sich das
ganze Universum an Gedanken einfach synchron als Netz in sainem
Gehirn. das war nicht immer eine abwegige, schlechte Denkstruktur, aber
gerade war sie zu groß, um sie gut zu erfassen. Zum Sortieren half
es Mirash, sie laut abzuarbeiten, selbst wenn es dabei immer noch
chaotisch zuging. Auf diese Art war as wenigstens gezwungen, sich
Verzweigungen in der Struktur konkret anzusehen.

Manche der Gedanken überprang as, weil as sie bereits oft durchdacht
hatte, sie aber als Verknüpfung vom gerade
besprochenen zu neuem Geäst gebraucht wurden. In
diesem Fall erinnerte sich as daran, dass as bisher in jeder toxischen
Beziehung ein unangenehmes Gefühl gehabt hatte, das as lediglich
unterbewusst versucht hatte, zu unterdrücken. Das half leider in die
eine Richtung wenig. As fühlte sich auch in vielen Beziehungen mit
Leuten im Alltag nicht wohl, die nicht toxisch waren, einfach weil
as sie nicht so gut verstand, oder weil Bedürfnisse kollidierten, oder
weil die Kommunikationsart oft zu Missverständnissen führte. Aber
in die andere Richtung half es: Eine Beziehung, in der as nie
Angst oder Beklemmung spürte, nie dachte, dass as vielleicht ein
Störfaktor sein könnte, hatte sich noch nie als toxisch herausgestellt.

Mirash atmete Erleichterung ein und aus, entspannte sich. Flederschatten
war eine Person, bei der Mirash keine dieser Ängste hatte. Oder doch? Da
war kurz dieses heiße, miese Gefühl gewesen, als Flederschatten geäußert
hatte, dass Mirash Flederschatten unsympathisch sein müsste. Aber das
war irgendwie etwas anderes gewesen. Es war eine moralisch fragwürdige
Sache gewesen, die Mirash da gemacht hatte. Das heiße Gefühl war schon
vorher manchmal leise da gewesen, bevor Mirash das ganze ausgeführt
hatte.

Flederschatten hatte mit der Vermutung unrecht, dass Mirash da allzu sehr
hineinüberredet worden wäre. Es war nie darum gegangen, der Zeit-Fraktion
etwas zu zeigen. Mirash hatte sich nicht gezwungen gefühlt, weil as
ja zu allem nein gesagt hätte, und nun ja hätte sagen müssen, weil as
nichts argumentativ hätte dagegen sagen können. Mirash fielen spontan
viele Gründe ein, die as hätte anbringen können. Aber as hatte sich
von Anfang an dazu bereit erklärt, etwas in der Richtung zu tun.

"Warum habe ich denn gefühlt das ganze Spiel gegen mich aufgebracht?", fragte
as die Möwe.

Sie antwortete wieder nicht.

Mirash grub die Finger in ihr weiches Fellgefieder. Es war sehr
flaumig. "Flederschatten hat sich so gut angefühlt. Ich möchte so gern wieder, ich
habe so einen Drang dazu. Und ja, vielleicht würde er weniger werden, wenn
Flederschatten nicht wollte, aber wahrscheinlich will Flederschatten auch, nur
müssen wir eben vorher Dinge klären. Das wird hart."

As fühlte etwas in sich resignieren und ermatten bei dem Gedanken, dass
Masturbieren in der Spielpause wohl eine sinnvolle Sache wäre. Es war nicht
so, dass Mirash nicht gern masturbierte, aber as hatte es auch eilig, wieder
ins Spiel zu kommen. Masturbieren kostete Zeit und brauchte außerdem
ein Mindset, in das Mirash vorher hinein- und hinterher wieder heraustauchen
musste.

"Eins nach dem anderen. Ich liste dir Mal die Gründe auf, okay?" Die Möwe
antwortete nicht. "Ich glaube, mein stärkster Grund war gar nicht der, dass
ich finde, dass keine einzelne Person so eine Macht haben sollte. Das ist
nach wie vor ein Grund und auch ein guter Grund. Der, den ich am besten
nach außen vertreten kann.", erklärte Mirash. "Mein persönlicherer und
stärkerer Grund ergibt sich aus der Art, wie ich dieses Spiel spiele, aber
andere eben nicht. Dieses Spiel ist voll uraltem, sozialem Gedöns, das
sich bestimmt irgendwo in Foren nachlesen lässt. Aber ich bin nicht
Teil davon, noch nicht. Und wenn ich davon erfahren will, dann müsste ich
mich von irgendwelchen Personen aufklären lassen. Also, dachte ich, schreibe
ich selbst Geschichte, verändere was Großes, stehe im Mittelpunkt."

In diesem Moment spaltete sich Mirashs Gedankengang in zwei Richtungen
auf und as konnte sich nicht davon abhalten, beide gleichzeitig
zu versuchen zu denken und dabei zu scheitern. "Ich stehe gern im
Mittelpunkt.", murmelte as. Aber die anderen Argumente für die
Aktion suchte as erst einmal vergeblich. Vielleicht, weil eine kurze
Welle von Scham as dafür überrollte, gern im Mittelpunkt zu stehen. Mirash
hatte nie so richtig verstanden, was die Scham da eigentlich sollte. Aber
das war auch ein Problem für einen anderen Zeitpunkt.

Mirash würde ein paar Tage Abstand vom Spiel brauchen, um dieses Gedankengewusel
sortiert zu bekommen. Dazu war as gerade noch nicht bereit. Es wäre für
Psycho-Hygiene wahrscheinlich gesünder gewesen, aber Mirash forderte
die eigene Gesundheit in gewissem Rahmen durchaus bewusst und
kontrolliert heraus. As kannte das. Die Folgen waren es ihm wert.

Mirash machte ein paar erdende, mit Atmung verbundene
Entspannungsübungen und legte anschließend die eigene Bettdecke
zwischen die Beine. Es gab sicher Werkzeuge, die zum Masturbieren
eher gedacht waren, aber Mirash hatte als Kind Bettdecken dafür
genutzt, weil as sie eben zugänglich gehabt hatte, ohne sich weiter
informieren zu müssen, und so war es eine altvertraute Gewohnheit. Mirash
versuchte sich zunächst davon abzuhalten, beim Masturbieren an
Flederschatten zu denken, auch wenn die Bilder alle so frisch
waren. Dann erinnerte as sich daran, dass Flederschatten dem
zugestimmt hatte, aber schließlich verdrängte Mirash das Erinnerungsgefühl
an Flederschattens Körper trotzdem wieder aus sainen Vorstellungen. As
war erschöpft und hatte Bedenken, das Traumabbild später mit in die
Realität der Virtualität zu vermischen, da nicht sauber trennen
zu können. Also stellte sich Mirash dabei eine sehr fiktive, frisch
erfundene Person vor. Ohne Vorstellungen konnte Mirash nicht.

---

Anschließend lag Mirash ein paar Momente auf dem Rücken, um die
Pläne zu sortieren. Essen, Trinken, Spazieren, Duschen. Duschen
war vor Spazieren dran. Und vor Essen, aber das Essen konnte schon
drucken, bevor as duschte. Und als erstes käme der EM-Anzug in
die Wäsche. Aber in sainer Hütte hatte as keine Waschmaschine, dazu
musste as über den Platz zum Raum mit den Maschinen gehen. Und
das war mehr als ärgerlich. Vor dem Duschen wollte as nicht in
saubere Kleidung steigen. Aber der Anzug brauchte am längsten, um
ausreichend zu waschen und zu trocknen, daher sollte der
Schritt schon als erstes passieren. Aber nackt mit Schleim
im Schritt im Winter über den Platz spazieren, war auch nicht
das Highlight, das Mirash sich für diese Pause erträumt hätte.

Mirash seufzte und entschied sich, sich mit weniger benutzten
Stellen des EM-Anzugs grob abzuwischen, einen Bademantel zu
benutzen und den Plan ansonsten trotzdem umzusetzen. Anzug
wegbringen, während der Matsch des durchgeweichten Waldbodens
in die Badelatschen schwappte, Druckauftrag starten, duschen,
anziehen, essen und dann ein Spaziergang im Wald. Für diesen
trug Mirash geeigneteres Schuhwerk als Badelatschen, aber
leider keine Kröte in der Kapuze. Natürlich waren Mirashs
Gedanken voll mit Flederschatten beschäftigt, mit diesem ruhigen Gesicht, das
gleichzeitig so neugierig und so chaotisch war. Aber auch mit
Flammenfinger, ein wenig mit Zeitkick und Holgem, und mit dem Spiel
im Allgemeinen.

Es war weit nach Mitternacht und Mirash sah den schmalen Waldweg
unter sainen Füßen nicht, insgesamt fast gar nichts. Aber as
kannte die Windungen genau. Außerdem wurde dort, wo der Weg am Rande
aufhörte, der Boden weicher und bewachsener. Mirash
fühlte, wenn as vom Weg abkam. Es roch kalt und feucht. Und
endlich hatte Mirash wieder Bewusstsein dafür, für sich selbst
zu atmen.

As fokussierte sich auf die Pläne, die as nun für sich entwerfen
wollte. As wollte kein Abhängigkeitsverhältnis mit Flederschatten
haben. Und es war nun ein wenig trickreich, das einzufädeln, oder
wieder auszufädeln. Auf der anderen Seite fühlte sich Mirash schließlich
mit Flederschatten wohl, das hatte as festgestellt.

Vor ihrer Sex-Sache hatten sie ausgemacht, dass diese ihre Haltung
zueinander nicht beeinflussen sollte. Zu dem Zeitpunkt vorher war
der Plan gewesen, in Flederschattens Basis zu gelangen, um dort
für die Zeit-Fraktion zu spionieren. Und gegebenenfalls zu
sabotieren.

Spionage hielt Mirash immer noch für eine gute Idee, stellte as
fest. As war dort nicht im Zusammenhang mit der Sex-Sache hingelangt,
sondern im Gespräch, in dem es bereits wieder um Politik gegangen
war. Und heimlich Dinge über Flederschatten zu wissen, war eine
Absicherung. Mirash konnte dann immer noch entscheiden, was as
mit Informationen machte, -- falls as welche fand. As war sich noch
nicher, was as von der Zeit-Fraktion abschließend halten sollte. Es
gab sicher eingefahrene Strukturen, von denen as schon mitbekommen
hatte und die ihm missfielen. Aber sie war vielleicht trotzdem die
einzige Fraktion, die Flederschatten etwas entgegensetzen könnte und
würde. Mirash war immer noch recht überzeugt davon, dass es nicht
gut wäre, wenn ein einzelner Charakter im Spiel so viel Macht
hätte. Vielleicht würde es sich als sinnvoller Kompromiss
herausstellen, mit der Zeit-Fraktion in ein paar Punkten
zusammen zu arbeiten. Einen Moment lang überlegte Mirash, auch
wenn ihm so etwas gar nicht behagte, die beiden Parteien gegeneinander
auszuspielen, um den anderen Fraktionen mehr Raum zu verschaffen. Dazu
hätte Mirash aber vielleicht doch mehr von Intrigen verstehen
müssen. Und bräuchte insgesamt mehr Hintergrundwissen.

Informationen würden ihm in jedem Falle helfen, um
abgleichen zu können, ob Flederschatten oder beliebige andere Parteien
die Wahrheit sprachen. Oder sie konnten einfach ein Stück Macht sein, das
as dann Flederschatten gegenüber hatte, um das besagte
Abhängigkeitsverhältnis anzukratzen.

Risiko bestand natürlich, dass Mirash sich dabei ausversehen
in die Luft jagen würde. Aber auch das wäre interessant: Würde
Flederschatten Mirash dann abschreiben? Oder nicht? Wieviel war
Flederschatten die Sache wert? Was war Flederschattens Motiv, sich
sainer anzunehmen?

Einen Augenblick überlegte Mirash, doch Forendiskussionen oder so
etwas nachzulesen. Natürlich nicht selber. As las nicht so gern
lange Texte. Aber dafür gab es KIs, die für as das aus Texten
herauslasen, was as wissen wollte, und die Inhalte für as
zusammenfassten. Es wäre bei der Komplexität des Spiels, die
sich weniger aus der Spielmechanik als viel mehr jahrelanger
sozialer Entwicklung ergeben hatte, vielleicht zur Abwechslung
nicht verkehrt gewesen. Hier wurde mit Psyche gespielt, das
war immer etwas haariger, als wenn es bloß um Spielmechanik
ging. Aber noch sträubte sich alles in Mirash dagegen, mit
dem Prinzip zu brechen. Vielleicht wenn sich saine eigentlich recht
stabile Psyche irgendwann doch zu labil anfühlte. Oder, wenn
sich die Lage in einer Woche nicht gebessert hätte, zusammen mit einem
Account-Neustart.

Dann wiederum hatte Flederschatten auch einfach recht damit: Mirash
war nun ein sehr interessanter Charakter in diesem Spiel.
