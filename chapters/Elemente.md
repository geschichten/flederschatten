Elemente
========

As hätte nun einfach hier am Tresen verweilen und Flammenfinger
beobachten können, wie sey geschäftig durch die Dramaturge
wuselte, Leute an den Tresen mitnahm, sie zum Beispiel motivierte,
vielleicht auch einmal etwas aufzuführen. Sey machte dabei
keinen Druck. Meistens. Bis auf bei einer Person, der Flammenfinger
sehr streng gegenüber war, aber vielleicht war das so ein
Spiel wie zwischen ihnen vorhin. Mirash konnte nicht leugnen,
Flammenfinger sehr sympathisch zu finden, aber auch ein wenig
unheimlich. Vielleicht manipulativ. Vielleicht nicht in einer
schlimmen Weise. Flammenfinger war gut darin, mit Stimmung
anzustecken. Vielleicht beschränkte sich das Manipulieren
auf eben dies.

Aber Mirash überlegte sich stattdessen, eine weitere freiwillige
Person für ein Gespräch zu finden. Es schien schließlich ein soziales Spiel zu
sein. Mirash machte soziale Interaktion durchaus Spaß, auch wenn sie
as sehr anstrengte. Aber in Spielen mit vielen Leuten war es meist
möglich, sich aus Gruppen zurückzuziehen, sobald es unangenehm
wurde. Das reduzierte die Angst und machte es zu einem Feld, in
dem as sich ausprobieren konnte.

Mirash blickte sich im Raum um, der inzwischen brechend voll
war. Mirash nutzte eine Filterfunktion in Spielen für die
Akustik, die automatisch Stimmen, die weiter weg
waren, herunterregelte. Ohne so etwas hätte Mirash die
Situation vermutlich zu stressig gefunden und wäre innerhalb
kürzester Zeit zu müde gewesen, um weiter teilzuhaben. Leute standen und saßen
überall, manche umarmten sich, tauschten Gegenstände oder
unterhielten sich angeregt.

"Hat jemand von euch diesen Schild gedroppt? Ich habe auch das
Schwert dazu gesammelt, aber beim Tresen abgegeben!", schrie eine Person
durch den Raum, die sich dazu auf einen Tisch stellte. Sie rief
es ein zweites Mal.

"Hier, ich! Oh!", antwortete eine andere Person von der anderen
Seite des Raums überrascht und schlängelte sich erfreut
durch die Menge zu der Person auf dem Tisch. Mirash beobachtete, dass
sie beide die gleiche Farbe trugen. Nicht Transformation. An die weitere
Farbkodierung erinnerte sich Mirash nicht mehr.

Wo schon einmal jemand auf einem Tisch gestanden hatte, beschloss
Mirash dasselbe zu tun, um eine Übersicht zu bekommen. As kletterte
auf den Tresen, was gar nicht mal einfach war, weil er relativ glatt und
hoch war. Von dort aus blickte as in die Menge, ob ihm irgendeine
Person bekannt vorkäme. Vage hoffte as, sainen Geleitschutz
wiederzuerkennen. Aber das war wegen des vorherigen Blendens ziemlich
unwahrscheinlich.

Das war der Moment, in dem Mirash das erste Mal die Idee kam, dass das
Blenden Absicht gewesen sein könnte. Blenden ordnete Mirash am
ehesten Optik zu. Aber der Begleitschutz hatte zu Transformation
gehört. Der Begleitschutz hatte as in die Dramaturge gebracht und
gesagt, dass dies ein sicherer Raum wäre. Flammenfinger gehörte
nicht zu Optik, aber konnte Dinge mit Optik. Vielleicht gehörten
sie zusammen.

Vielleicht erfand Mirashs Vorstellungskraft hier aber auch abstruse
Intrigen, weil dies ein intrigenreiches Spiel war. As könnte eigentlich
überhaupt nicht gut welche erkennen, und sah deshalb manchmal
überall welche. Eigentlich war das bisher nur so stark in Romanen, Filmen
oder Spielen mit Story gewesen, wenn Mirash versucht hatte, zu
raten, wer die Charaktere mit üblen Motiven wären. Hier handelte
es sich um reale Personen, die einfach herumspielten, nicht
Teil des Spiels waren. Das war neu.

Mirashs Blick blieb schließlich das vierte Mal bei der Person hängen, die
sich vorhin erkundigt hatte, ob jemand von ihr Bogenschießen lernen
wollen würde. Mirash sprang vom Tresen und näherte sich ihr. Die
Person trug ein schwarzes, schlichtes Oberhemd, einen schwarzen, kurzen
Spitzenrock und dunkelgraue Strumfphosen und Hosenträger -- oder viel
mehr Spitzenrockträger. Auf den Manschetten erkannte Mirash ein
aufgesticktes, hellgraues Symbol, das entfernt an eine verzierte
Uhr erinnerte. Zeit, riet Mirash.

"Du darfst dich ruhig dazusetzen, wenn das deine Frage ist.", sagte
die Person.

"Ich schätze, ich habe gestarrt. Das tut mir leid." Mirash mochte, dass
das Begrüßungsgefloskele ausblieb. "Zeit?"

"Zeit." Die Person lächelte kurz. Sie trug keinen Hut. Die schwarze
Spitzenmaske überdeckte das Gesicht bis zur Nasenspitze. Die Schnüre, mit
denen sie an den Kopf gebunden war, brachten die Frisur etwas
durcheinander. Im Vergleich zu den anderen hier wirkte sie allerdings weniger
zerfleddert.

"Hast du draußen nicht mitgekämpft?", fragte Mirash. "Und überhaupt: Ist
es in Ordnung, wenn ich dich mit Fragen bewerfe?" As wollte eine Antwort
auf die Frage abwarten, bis as sich setzte.

"Ich finde Fragen in Ordnung, aber ich bin auch nicht so der
Smalltalk-Typ. Vielleicht bin ich kurz angebunden. Das ist
nicht böse gemeint." Die Person machte eine einladende Geste mit
der Hand.

Mirash setzte sich. Der Holzstuhl wirkte etwas desolat. Mirash
rutschte absichtlich darauf herum, um ihn zu stresstesten. Besser
einkalkuliert am Anfang damit zusammenbrechen, als irgendwann
mitten im Gespräch überraschend. Zwischen ihnen auf dem Tisch
stand eine unentbrannte Kerze. "Du suchst nach einer Person,
der du Bogenschießen beibringen kannst, richtig?"

Die Person verzog den Mund zu einem etwas dauerhafteren
Lächeln. "Ja. Möchtest du Bogenschießen lernen?"

"Ich bin noch nicht sicher, aber warum nicht.", antwortete
Mirash. "Ich bin neu im Spiel und lerne es von innen kennen. Ich
habe mich im Vorfeld nicht über Spielmechaniken informiert und
weiß nun ein paar von Flammenfinger. So viel, wie da draußen
geschossen wurde, vermute ich, dass Bogenschießen ein guter Skill ist."

"Definitiv.", antwortete die Person. "Wobei Bogenschießen auch
nicht Bogenschießen ist. Ich würde gern Schießen mit Langbogen
skillen. Das ist so eine klassische Fernkampftechnik. Ich bin
auch noch ziemlich am Anfang."

Mirash nickte lediglich. As fiel auf, dass die Person die erste Frage
noch nicht beantwortet hatte. Weil as nicht wusste, was as zum
Thema Schießen konkret fragen sollte, überlegte as, die Frage zu wiederholen, aber
bog dann spontan gedanklich woanders ab, weil as das Bedürfnis verspürte, sich
vorzustellen. "Wie ist das in diesem Spiel mit Namen? Mir haben
sich zwei Personen vorgestellt und eine nicht. Gibt es dazu
eine Art Regelset? Was ist üblich?"

"Flammenfinger hat sich bestimmt vorgestellt. Sey stellt sich allen
vor.", sagte die Person. Das war zwar keine Antwort, aber sie
fügte hilfreicher Weise hinzu: "Ich bin Zeitkick. Pronomen sie, ihr,
ihr, sie."

"Flammenfinger hat sich vorgestellt, das stimmt.", bestätigte
Mirash. "Sollte ich mich jetzt vorstellen?"

"Du kannst das halten, wie du willst.", antwortete Zeitkick, vielleicht
eine Spur harsch. "Ich fände das eine nette Geste." Zeitkick wand sich
auf ihrem Stuhl ein wenig herum und fügte leiser hinzu: "Offen
gestanden hatte ich deine Nachfrage so aufgefasst, dass du gern
wolltest, dass wir uns einander vorstellen und würde mich unbehaglich
fühlen, wenn du es nicht tust. Du hast das natürlich nicht
direkt gesagt, aber du musst schon zugeben, dass es klar ist, dass
das so rüberkommen ist, oder?"

Mirash fand das nicht so klar. Aber as war auch gewöhnt daran, dass
diese Probleme bei Kommunikation zwischen ihm und anderen auftraten
und beschloss, in Zukunft mit Zeitkick möglichst klarer zu kommunizieren. "Ich
bin Mirash."

Zeitkick atmete auf. "Wenn wir ein Lern-/Lehrverhältnis anfangen
möchten, dann ist es ohnehin gut, wenn wir unsere Namen kennen." Sie
sortierte die lockigen, dunkelrotbraunen Haare in den Nacken
und rückte näher an den Tisch. "Ich hatte mir jedenfalls schon
fast gedacht, dass Flammenfinger uns nicht sofort zusammenbringen
würde."

Mirash lächelte vorsichtig. "Warum?"

"Sagen wir: Flammenfinger hat gern Dinge im Griff.", erklärte
Zeitkick. Sie lächelte dabei überhaupt nicht. "Ich möchte
nicht schlecht über Leute hinter deren Rücken reden."

"Das verstehe ich." Mirashs Blick wanderte abermals zur
Kerze. As stellte sich vor, wie Flammenfinger vorbeikommen
und sie anzünden würde. Dann fiel ihm ein, dass Flammenfinger
mit Illusionen von Feuer keine Kerzen anzünden können würde. As
blickte wieder auf in Zeitkicks halb verdecktes Gesicht. "Es
war vom Anfang noch eine Frage offen."

"Welche?", fragte sie.

"Ob du draußen nicht mitgekämpft hast.", erinnerte Mirash.

"Ah!" Zeitkick nickte. "Nicht viel. Nur ein bisschen
Selbstverteidigung, um von der Zeitbasis hierher zu kommen. Ich
bin zu spät gejoint, sonst wäre ich schon vor der Kampfzeit
in die Dramaturge gekommen."

"Kämpfst du nicht gern oder geht es um diesen Kampf?", fragte
Mirash.

"Es geht um diesen Kampf." Zeitkicks Blick wanderte ein paar
Momente abgelenkt Richtung Bühne.

Mirash wandte sich ebenso um. Inzwischen brannten dort mehr
Fackeln und einige Personen rückten dort Bühneneinrichtung
zurecht.

"Das sinnlose Gekloppe ist nicht so meins.", fügte Zeitkick
hinzu. "Ich mag meine Ressourcen gern für die Eroberungen
schonen. Ich weiß nicht, wieviel du schon weißt, worum
es hier eigentlich geht."

"Nicht so viel.", gab Mirash zu.

"Gleich fangen die Vorstellungen an. Ich trete im letzten
der fünf Slots auf. Daher bin ich ab nun ein bisschen
nervös.", erklärte Zeitkick. "Aber wenn du magst, kann
ich dich hinterher ein bisschen einführen. Ich bin eher
nachtaktiv, ich weiß nicht, wie das bei dir ist."

"Chaotisch." Wie die Ziege, dachte Mirash. Außerdem bemerkte
as innerlich spielerisch genervt, dass as
schon wieder an eine Person geraten war, die nun zu
abgelenkt für komplexere Gespräche war. Auf der anderen Seite war es eben ein
Ort, an dem es schon Pläne gab. Da war klar, dass
Mirash sich da in Vorhandenes einfügen müsste.

"Willst du dir das Schauspiel ansehen?", fragte Zeitkick.

Mirash nickte. "Gern! Es hieß, dass alle fünf Elemente
vorgestellt werden und ich für mich entscheiden kann, welches
das coolste ist."

Zeitkick lachte kurz auf. "Zeit, wenn du mich fragst." Sie
schmunzelte. "Ich bin wohl voreingenommen."

"Das ist ja nicht schlimm." As machte sich einen Moment Gedanken, ob
Flammenfinger ihm voreingenommen vorgekommen war. Eigentlich
gar nicht. Trotz der vielen Flammen. Das war allerdings kein Hype auf
Optik gewesen, sondern einfach ein bisschen Show.

"Rück doch zu mir herum, dann siehst du besser und muss dich
nicht so verrenken.", lud Zeitkick ein.

Mirash folgte dem Vorschlag. Der Tisch war auf der anderen
Seite nicht sehr breit, sodass es sich zunächst seltsam anfühlte, dass
Zeitkick am Tisch saß und Mirash nun daneben. Aber das Gefühl
löste sich bald auf. Mirash hatte ein Stühlerücken
im ganzen Raum eingeleitet. Innerhalb kürzester Zeit saßen
die meisten Leute der Bühne zugewandt. Vorn setzten sich Personen
auf den Boden. Hinten standen ein paar zwischen Sitzgruppen
von Leuten, die das Theater weniger zu interessieren schien, die
in ihren Sitzgruppen sitzen blieben und sich weiter leise
unterhielten.

Dann begann die Vorstellung. Als erstes betraten vier Personen
die Bühne, in Schwarz mit dunkelgrünen Farbeinsätzen.

"Kräfte.", informierte Zeitkick leise neben Mirash.

"Danke!", flüsterte Mirash zurück.

"Die schwächste und größte Gruppe.", fügte Zeitkick hinzu.

Mirash wandte ihr abrupt den Blick zu. "Was?"

"Nicht böse gemeint.", erklärte Zeitkick beruhigend. "Kräfte
skillt sich einfach vergleichsweise schnell, aber das Element
ist nicht sehr mächtig. Entsprechend lernen es Leute, die nur
gelegentlich mal spielen. Es bildet sich eine große
Fraktion mit viel Fluktuation, die sich vor allem durch ihre Menge anderen
Fraktionen entgegensetzen kann. Das ist ja nichts per se Schlimmes."

Mirash blickte zurück zur Bühne. Was sie dort allerdings vorführten, kam
ihm gar nicht so wenig mächtig vor. Drei der Personen machten
interessante, choreographische Bewegungen mit den Armen -- keine
komplexe Choreographie, aber eine schöne --, und die vierte
stieg eine wohl dadurch entstehende, unsichtbare Wendeltreppe
hinauf. Sie trug einen langen, ausladenden schwarzen Rock mit
grünen Längsstreifen, den sie dabei galant etwas emporliftete, um
nicht auf den Saum zu treten. Als sie zu den Köpfen der anderen
drei Personen angelangt war, hörten diese mit einem schönen
Abschluss-Move auf und blieben mit angelegten Armen still
stehen. Die unsichtbare Treppe verschwand, aber nun führte die
fallende Person selbst eine Bewegung aus, die Mirash aus Entspannungsgymnastik
kannte. Mit dieser wurde am Ende häufig noch einmal ein Ausatmen
mit einem Bedanken verbunden. Die Person fiel langsamer, als
es ihr die Schwerkraft normalerweise abverlangt hätte, und die
Geste passte intuitiv gut zum Bremsweg.

Das wirkte elegant, fand Mirash.

"Wie gefällt es dir?", flüsterte Zeitkick, als die Gruppe der vier
Kräfte-Leute die Bühne verließ und stattdessen nun drei andere
Personen derselben Gruppe die Bühne betraten.

Mirash lächelte. "Elegant."

"Oh, das war noch gar nichts in Sachen Eleganz!", versprach
Zeitkick.

Nur Momente, nachdem sie das gesagt hatte, stolperte
eine der Personen auf der Bühne beim
Aufbau recht unelegant. Sobald eine der anderen
eine Hand frei hatte, kümmerte sie sich sehr lieb um
die eine Person, fand Mirash. Ein Schauspiel, dass Mirash
sich wegen der Hilfsbereitschaft auch gern ansah. Die
zwei Nixen fügten sich in die Gruppe so unscheinbar
ein, dass Mirash sie zunächst gar nicht
bewusst wahrgenommen hatte.

"Ah, die machen die Planeten.", murmelte Zeitkick Mirash zu. Es
wirkte fast ein bisschen verschwörerisch. "Ich könnte das ja nicht. Mich
in einer so unkoordinierten Gruppe wohl fühlen, meine ich. Ist
was Persönliches. Dadurch, dass die sich ständig neu finden, wissen
die kleinen Grüppchen teils nicht von einander, was so in letzter
Zeit schon vorgeführt worden ist, und es kommt viel zu Wiederholungen. Ich
bin deshalb lieber in einer Fraktion, in der ich eine Chance haben
kann, die anderen wirklich kennen zu lernen und mich mit ihnen abzusprechen."

Mirash hatte den Eindruck, dass Zeitkick allmählich auftaute. Zumindest
wurden die Monologe länger und persönlicher. Mirash kam das entgegen, aber
as nickte ihr bloß lächelnd zu, bevor as wieder das
Bühnengeschehen beobachtete.

"Du kennst natürlich noch nicht so viel.", fügte Zeitkick hinzu, mit
einem Lächeln in der Stimme. "Hast du vor, viel zu spielen?"

"Ja.", bestätigte Mirash schlicht.

"Dann würde ich dir eher eines der höheren Elemente nahelegen. Zeit
oder Transformation."

As bemerkte die kleine Lücke, die Zeitkick vor der Nennung
letzteren Elements gemacht hatte. War es Unbehagen? Mirash blickte
auf ihr Profil. Ein schönes Profil. Zeitkick trug schwarzen
Lippenstift, hatte weiche Wangen und einen Ansatz eines schönen
Doppelkinns.

Als sie bemerkte, dass Mirash sie beobachtete, wandte sie sich
wieder zu ihm um. "Du hast mein Zögern bemerkt, du
bist aufmerksam.", sagte sie.

Mirash war anderer Meinung, aber gut. Schön, dass sie das so
wahrnahm. As lächelte.

"Es ist echt nichts Schlimmes. Die Transformation-Fraktion und die
Zeit-Fraktion haben sich in den Haaren. Also etwas mehr, als
mit den anderen Fraktionen jeweils.", erklärte Zeitkick leise
flüsternd. "Das ergibt sich vielleicht einfach als etwas Natürliches
zwischen den beiden mächtigsten Fraktionen. Aber deshalb hätte
ich natürlich ein Eigeninteresse daran, wenn du dir eben eher
nicht Transformation aussuchst."

"Sondern Zeit.", ergänzte Mirash.

"Kommt drauf an.", entgegnete Zeitkick. "Du sagtest, du möchtest
viel spielen. Und du wirkst recht sozial. Aber natürlich kenne
ich dich kaum. Wir haben ein paar soziale Regeln für ein
gutes Miteinander. Uns ist wichtig, dass sich Personen, die
dazukommen, daran halten."

"Wirkt verständlich." Mirash kannte so etwas von allerlei
Communitys. Sie hatten ihre Codexe für etwa ein möglichst
diskriminierungsfreies Miteinander. Es klappte verschieden gut.

"Wir sind ein ziemlicher Nerd-Haufen. Wenn du dich bei uns
nicht wohlfühlen solltest, ist das voll verständlich.", fügte
Zeitkick hinzu. "Aber dann wäre halt mein Eigeninteresse, dass
du bei einer anderen Fraktion landest. Vielleicht bei den
Optik-Leuten oder so."

War es Zufall, dass as mit Zeitkick gerade über die zwei
Fraktionen -- Transformation und Optik -- sprach, zu
denen as sich Gedanken gemacht hatte, sie eher nicht zu wählen?

Mirash nickte. "Verstehe." Und blickte wieder nach vorn. As
hatte einiges verpasst. Aktuell kreiste eine kleinere
Kugel um eine große. Planeten. Wie Zeitkick angekündigt
hatte. Es war durchaus schön.

---

Drei Aufführungen später betraten endlich zwei Personen die
Bühne, die nicht Dunkelgrün als Ergänzungsfarbe trugen, stattdessen
Violett. Mirash erkannte sain Zylindermodell in
der Farbe bei beiden Vorstellenden. Sie trugen Partnerlook, relativ
schlichte, schwere, schwarze Kleider mit einer violetten Weste
darüber.

"Wie stehst du zu Partnerlook?", fragte Zeitkick leise.

"Indifferent." Mirash hatte sich doch eigentlich vorgenommen, weniger
mit Fachwörtern um sich zu werfen, und fügte hinzu: "Weder negativ noch
positiv."

"Ich weiß, was indifferent heißt.", flüsterte Zeitkick. "Partnerlook ist
nicht so meins, offengestanden."

Beide der Vorführenden zückten jeweils ein Streichholz. Die rechte
Person ratschte ihres über den Rand der eigenen Schuhsohle, gut für
alle sichtbar, aber das andere Streichholz flammte auf. Die zweite Person
näherte sich damit einer Kerze auf einem Barhocker zwischen ihnen und zündete
sie an. Die Flamme am Streichholz erlosch. Die Kerze flackerte nur ein paar
Momente, als ein die andere Person laut Luft zwischen den Zähnen einsog und
so die Aufmerksamkeit wieder
auf sich lenkte. Sie ließ das unversehrte Streichholz fallen, stampfte
darauf und pustete sich über die Finger. Im selben Augenblick erlosch die
Kerze. Und als sie sich bückte, um das betrampelte Streichholz aufzuheben, war
es schwarz und gekrümmt, wie Streichhölzer für gewöhnlich beim Abfackeln
enden. Das Streichholz der anderen Person war unversehrt.

Sie räumten die Bühne. Mirash blickte sich zu Zeitkick um, ob sie
wieder einen Kommentar von sich geben würde. Aber vielleicht war
auch einfach Mirash mal dran. Nur, as fiel nichts zu sagen ein. Zeitkick
jedenfalls blickte schließlich zurück und schwieg ebenfalls vor sich hin.

Als nächstes trat eine einzelne Person auf die Bühne, die ein Charisma oder
so etwas hatte, das es mit Flammenfingers aufzunehmen vermochte. Sie trug
einen schwarzen Frack mit sehr langen Schwalbenschwänzen und grell violettem
Revers. Das Hemd darunter war dunkelblasslila. Dazu trug die Person
eine schwarze Lackhose mit violetten Einsätzen, wie sie
zum Reiten sinnvoll sein mochten. Die Haare waren glatt, schwarz und dick und
fielen schwer bis in die Hüften. Sie waren durchsetzt von Strähnen, die nur bei bestimmtem
Lichteinfall violett schillerten. Die Person war langsam und mit kerzengeradem
Rücken auf die Bühne geschritten, hatte sich zum Schluss mit einem passenden
Schwung zum Publikum umgedreht, dass Frack und Haar perfekt um sie herumflogen
und sich wieder legten.

"Emeralone.", hauchte Zeitkick Mirash zu. "Mächtig. Leg dich besser
nicht leichtfertig mit ihs an."

Mirash nickte, aber wandt den Blick nicht von Emeralone ab. Das Nametag
erschien mit Zeitkicks Erwähnen über ihs, die Pronomen waren rie, ihs, ihs,
rie. Das funktionierte also auch, indem Leute Namen
weiterverrieten. Mirash war sich nicht sicher, ob as diese Spielmechanik
mochte.

"Meistens macht rie reine Machtperformance.", murmelte Zeitkick. "Nichts
allzu Interessantes, aber deshalb nicht weniger beeindruckend."

Emeralone -- was war das für ein komplexer Name? -- zückte einen Taktstock
oder Zauberstab aus dem Frack und hob ihn wie zum Andirigieren, -- oder
zaubern.

"Niemand braucht einen Stab.", erklärte Zeitkick noch viel leiser. "Falls
du Angst haben solltest, weil das nicht dein Stil ist: Das brauchst du
nicht."

Mirash hatte keine Angst davor gehabt. Was aber kurz Angst auslöste, war
der Moment, als die Dramaturge plötzlich in absolute Dunkelheit
versank. "Wow", entwich es Mirash unwillkührlich und as war damit nicht
allein. Gemurmel erhob sich in die Schwärze ohne den geringsten
Lichtrückstand, aber nicht das übliche, eher Bewunderndes
oder teils Beunruhigtes. Irgendwo in der unsichtbaren Menge erschreckte sich eine Person mit einer
tiefen Stimme ziemlich beeindruckend waberig aufschreiend, die
Mirash dadurch direkt sympathisch war, auch wenn as den Eindruck hatte, besser
Mitleid als Sympathie verspüren zu sollen.

"Sag ich ja.", murmelte Zeitkick in das leise Stimmengewirr hinein. "Mächtig und
viel Showeffekt. Rie ist sehr fortgeschritten."

Langsam wurden Emeralones Umrisse wieder sichtbar, sehr klar, verglichen
mit der Art, wie Mirash nach dem Blenden hatte sehen können. Es
wirkte, als würde hinter ihs eine weiße Sonne scheinen, und Emeralones
Umriss hatte den Effekt eines Mondes bei einer Sonnenfinsternis. Die Corona glomm
schick in der Dunkelheit, wanderte um rie herum, bis ihs Abbild selbst wieder
in klaren Farben erschien, nur so flimmernd, als wäre Emeralone heiß wie
die Sonne. Oder etwas nur sachte weniger beeindruckend heißes.

Dann kehrte das Licht in die Dramaturge zurück. Die Menge atmete auf. Mirash
fragte sich, ob viele hier Angst gehabt hatten, oder es eben einfach die
Anspannung eines sehr beeindruckenden Effekts gewesen war, der nun nicht
mehr alle Aufmerksamkeit sog.

Emeralone verbeugte sich und verließ die Bühne.

"Die Reibungsleute.", informierte Zeitkick Mirash, noch bevor
weitere Personen die Bühne betraten. "Emeralone macht immer
den Abschluss für Optik."

"Ist es immer die gleiche Reihenfolge?", fragte Mirash.

"Ja, wenn nicht irgendwelche wichtigen Sachen dagegensprechen, dann
schon.", antwortete Zeitkick. "In der Rangfolge der Elemente. Je
stärker das Element, desto weiter am Ende."

"Irgendwie hat das so einen Touch Elitärismus oder so.", überlegte
Mirash leise. Und bereute es gleich. Das klang so ungerechtfertigt wertend.

Zeitkick blickte Mirash grinsend an. "Ja, manchmal wird uns sowas
nachgesagt. Aber der Hintergrund ist eigentlich praktischer Natur.", setzte
sie zu einer Erklärung an. "Ich sagte ja schon, die
Kräfte-Gruppe besteht vor allem
aus Leuten, die nicht so viel Zeit in diesem Spiel verbringen. Sie
gehen teils früher ins Bett und so. Es kommen entsprechend
die Fraktionen mit der meisten Spielzeit spät dran
und jene hängt eben nunmal mit der Stärke der
Elemente zusammen."

Mirash nickte nachdenklich. Das klang logisch. As störte sich
am Wort 'zusammenhängen' in dem Kontext, aber das mochte spitzfindig
sein. Sicher konnten auch Leute mit wenig Spielzeit sich das Element Zeit
aussuchen und es würde dann vermutlich nur sehr, sehr lange dauern, bis
sie die zugehörigen Fähigkeiten sinnvoll anwenden könnten. Oder irrte
sich Mirash da? As nahm sich vor, später nachzufragen.

Die Gruppe, die nun auf die Bühne trat, wirkte auf Mirash eigentlich
unkoordinierter als alles, was as bisher von der Kräftefraktion
mitbekommen hätte. Zeitkick kommentierte allerdings nichts dazu.

Die Grundfarbe der Gruppe war Cyan, und Mirash konnte sich durchaus
daran erinnern, bisher noch nicht viele von der Fraktion gesehen
zu haben. Dieser hervorstechende, grelle Farbton wäre ihm aufgefallen.

Es war ein interessantes Schauspiel. Die Gruppe baute ein schräges
Gerüst auf und schleppte dann eine große Holzplatte heran. Eine äußerst
dicke, schwere Holzplatte. Sie lehnten sie ans Gerüst und -- stellten
die Reibung darunter aus. Sie konnten auf diese Art Gerüst und das
fette Holzbrett relativ leicht auf dem Boden drehen, sodass die Gerüstseite
zum Publikum blickte. Wegen des Gewichts war für den Anschwung immer
noch die Kraft der fünfköpfigen Gruppe notwendig, um die Drehung in
Gang zu setzen. Aber es war ein ulkiges Gefühl einer so schweren und
eigentlich so wenig zusammenhängenden Konstruktion dabei zuzusehen, sich
zu drehen, als stünde sie auf Glatteis. Als sie sich erst einmal drehte, bremste
sie von selbst nicht mehr ab.

Die drei der Gruppe, die mit entsprechenden tanzartigen Bewegungen
zuvor die Reibung ausgestellt hatten, machten mit den Armen nun
Bewegungen, wie um etwas abzuschließen, und die Drehung hörte unsanft
auf.

Anschließend knüpften sie einen Flaschenzug an das obere Ende des
Bretts. Einen Flaschenzug aus Seil, das sich selbst gebremst hätte, weil
es so rau war und nicht mal auf Rollen um Knicke geführt wurde. Mirash
staunte als as die Bedeutung von Reibung begriff. Der
Flaschenzug machte einen sehr unrealistischen Eindruck, als eine einzelne
Person das schwere Holzbrett damit das Gerüst hinaufzuppelte, während die
anderen lediglich damit beschäftigt waren, Reibung an den richtigen
Stellen wegzuzaubern.

Anschließend verkeilten sie das schwere Brett über ihnen einfach zwischen zwei sehr
hohen aufgestellten Böcken links und rechts. Die wohl ausschließlich
durch stark erhöhte Reibung an Ort und Stelle blieben.

"Ganz schön lange Aufführung. Aber witzig, finde ich.", murmelte Zeitkick.

Mirash nickte. Hier waren sie sich einig.

Zum Schluss schraubten die Aufführenden einen Haken von unten ins Brett. Es
war ein großer Haken. Sie steckten einen Stab durch die Öse, um einen
längeren Hebel anwenden und zu fünft gleichzeitig schrauben
zu können. Als sie fertig waren, stellten sie sich in eine Reihe
vorn an den Bühnenrand, um sich zu verbeugen. Mirash verstand nicht, was
das mit dem Haken sollte, bis sie ihre Oberkörper geschlossen neigten und sich
der Haken hinter ihnen einfach aus der Decke drehte. Mit einem Scheppern
kam er auf dem Boden auf. Mirash musste kichern, als As realisierte, dass
Schrauben und Decken nur durch Reibung hielten, teils riesige Gewichte
nur durch Reibung tragen konnten, und ohne die selbe von der
Schwerkraft beschleunigt sich eben selbst herausschrauben würden.

Applaus brandete durch den Raum. Mirash klatschte nicht gern aber
ahmte die Bewegung vorsichtig nach. Bei den Gruppen zuvor war
der Applaus nur lokal an manchen Stellen im Raum aufgeflackert. Da
hatte as sich weniger unter Druck gesetzt gefühlt, mitzumachen. Zeitkick
klatschte dankbarer Weise nur leise mit.

Es gab eine Funktion, mit der
Virtualitäten so umgestellt werden konnten, dass immer, wenn irgendwo
Applaus passierte, stattdessen für eine Person persönlich stiller
Applaus dargestellt wurde, das Winken mit beiden Händen neben den
Schultern. Eine Applaus-Form, die außerhalb von Virtualitäten eigentlich
heutzutage Standard war. Aber in alt-angehauchten Virtualitäten
klatschten häufiger noch Leute, indem sie die Hände aneinanderschlugen, -- es
konnte ja ausgeblendet werden. Mirash hatte schon manches Mal darüber nachgedacht,
die Einstellungen dahingehend anzupassen.

Als der Applaus abflachte und die Bühne umgeräumt wurde, was dieses Mal
länger dauerte, fragte Zeitkick as darüber aus, wie ihm die Vorstellung
gefallen hatte. Mirash versuchte Worte dafür zu finden: "Besonders? Ich
mochte sie sehr. Es wirkte angenehm albern."

"Das beschreibt die Fraktion sehr gut." Zeitkick grinste. "Wenn also
Zeit nichts für dich sein sollte, was hieltest du von Reibung?"

"Viel." Mirash meinte es, aber merkte selbst, dass as skeptisch
klang. "Darf ich meine Entscheidungen selber fällen?"

"Ach du je, natürlich." Zeitkick wirkte angemessen geknickt. "Es
tut mir leid. Du hattest vorhin die Frage nach dem coolsten
Element gestellt. Ich dachte, da würde dich so ein bisschen
Lenkvorschlag erfreuen. Aber ich halte mich dann zurück!" Zeitkick
warf die Hände in die Höhe.

Die Transformationgruppe bastelte ein Flaschenschiff, aber auf
eine eher praktische Weise. Sie vergrößerten die Flasche, zunächst
so sehr, dass zwei der Gruppe gemütlich durch den Flaschenhals spazieren
konnten. Zwei andere Personen, die wohl für die Größe der Flasche
verantwortlich waren, schrumpften sie anschließend wieder so zusammen,
dass die Personen in der Flasche nun unpraktischer Weise darin
gefangen waren, dafür aber gemütlich alle Flaschenwände erreichen
konnten. Das filigrane Material, das ihnen für den Schiffsbau hineingereicht
wurde, wurde zum Basteln vergrößert, und dadurch zu lange
Masten, die nicht anders um die Kurve gepasst hätten, auch
noch vorübergehend verbogen. Zum Schluss
schrumpften die beiden Personen in der Flasche, um durch den Hals derselben
wieder hinauszukriechen. Zeitkick informierte as darüber, dass Körpertransformation
mit zu den fortgeschrittenen Fähigkeiten gehörte.

Anschließend wurde das gebastelte Schiff und die Flasche zeitgleich vorsichtig
verkleinert. Die zeitliche Abpassung wirkte kompliziert, aber Mirash
wurde vom Spektakel dadurch abgelenkt, dass Zeitkick neben ihm aufstand und
sich verabschiedete. "Wenn du hier wartest, nehme ich dich gern
hinterher mit in die Zeitbasis, um dir ein paar Dinge zu zeigen."

Mirash nickte. "Gern." Mirash konnte nicht leugnen, dass,
einfach mit einer Person mitzugehen, sich etwas unheimlich anfühlte. As
genoss es. Das war, was as an Spielen liebte. Wie sie innerhalb
weniger Stunden eine Situation schaffen konnten, in der Gruseln eine
Sache war. Es berauschte Mirash, ließ as sich interessant selbstbewusst
fühlen, wie ein Widerstand in einem Strom. Um sich mit Metaphern dem
Physik-Universum anzupassen.

Zeitkick war die erste Person, die auf der Bühne Waffen verwendete, die
auch wie Waffen aussahen: Ein kleiner Bogen mit zwei Pfeilen. Sie wurden ihr
auf der Bühne ausgehändigt.

Zeitkick bewegte sich sicher, aber nicht so beeindruckend wie etwa
Flammenfinger oder Emeralone. Sie zielte mit einem kurzen blauen Pfeil
von der linken Seite der Bühne auf einen Punkt an der Decke, sodass
die Flugbahn gegenüber einer vertikalen leicht nach rechts geneigt wäre. Sie
verharrte in der Position eine Weile konzentriert, bis sie
schoss, aber führte beim Schießen bereits eine recht komplexe Bewegung
mit dem Körper aus. Der Pfeil hielt, kaum hatte er die Sehne verlassen, einfach
in der Luft an.

Zeitkick wanderte an ihm vorbei auf die andere Seite der Bühne und schoss
von dort, das Manöver spiegelnd, einen roten Pfeil in die Luft. Auch
diesen hielt sie an. Dann bewegte sie sich in die Mitte der Bühne, um sich
zu verbeugen. Die Pfeile verharrten auf wunderschöne Art startbereit in
der Luft, bis sie breit grinsend den Körper endlich neigte und sie sich aus ihrer Starre
lösten. Sie trafen sich, wie geplant, in der Luft, aber sie hatten nicht
die gleiche Geschwindigkeit gehabt. Während der eine herabtrudelte, schoss
der andere in einer nur leicht verschobenen Bahn ins Dachgebälkt. Mirash
fühlte sich, als hätte as etwas ahnen müssen. Die Sehnenspannung war
as nicht auf beiden Seiten gleich vorgekommen, aber Mirash hatte dieses Detail
nur halb bewusst wahrgenommen und verdrängt.

Sicher war es sehr kompliziert, diesen Trick präzise auszuführen. Mirash
juckte es in den Fingern, es präziser zu erlernen. Aber wahrscheinlich
würde so etwas nicht für die nächste Zeit schon auf sainem Zeitplan landen.

Während Flammenfinger zusammen mit Haraldin die Bühne betrat und sie zunächst leise
mit Zeitkick sprachen, wanderte Mirashs Blick dorthin, wo der zweite Pfeil
nun in einem Deckenbalken steckte. Etwas Orangenes zog für einen kurzen Augenblick
Mirashs Aufmerksamkeit auf sich. Es hopste im Deckengebälk. Dort war eine
Person, sehr dunkel gekleidet. Das Fackellicht erreichte sie kaum, als sie über
die Balken sprang und irgendwo im Unergründlichen verschwand. War es
Mirashs Geleitschutz vom Anfang? Dieses bestimmte Orange!

"Es ist nur nicht so gelaufen wie geplant!" Zeitkicks etwas ungehaltene und
nun laute Stimme riss Mirash aus den Gedanken und saine Aufmerksamkeit
zurück auf die Bühne. "Das wisst ihr genau!"

"Das wissen wir nicht genau.", widersprach Flammenfinger in gleicher
Lautstärke aber sehr gelassen. "Wir haben hier nicht ohne Grund Regeln. Wenn
du nicht sicher genug mit Waffen auf Bühnen bist, bring keine mit."

"Der Pfeil hat nicht einmal den Bühnenbereich verlassen!", beschwerte
sich Zeitkick. "Ich bin echt nicht so der Typ, der im Normalfall bei Regelverstößen
diskutieren würde, aber das ist doch echt Unsinn in diesem Fall!"

"Tatsächlich haben wir eine offizielle Maximalhöhe, die noch als Bühne
gilt.", widersprach Flammenfinger.

"Ihr wollt ernsthaft meine billigen Waffen konfiszieren und
mich rausschmeißen?" Zeitkick klang sowas von unamüsiert.

"Für eine Woche.", konkretisierte Flammenfinger sachlich.

"Kann ich mich wenigstens von meiner Begleitung verabschieden?" Zeitkicks
Körperhaltung erschlaffte.

Mirash interpretierte erstens, dass as die Begleitung sein mochte, und
zweitens, dass Zeitkick nun aufgegeben hatte, sich gegen die Konsequenzen
für ihr Verhaltens zu sträuben. As hatte noch keine Meinung, ob sie
gerecht wären oder as eher Zeitkicks Wahrnehmung teilen sollte.

"Du hast eine Viertelstunde. Ich lege dir nahe, sie nicht
auszureizen." Flammenfingers Stimme und Auftritt etwas
entgegenzusetzen wirkte wirklich nicht in irgendeiner
Weise erfolgversprechend.
