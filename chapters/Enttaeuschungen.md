Enttäuschungen
==============

Eine Woche später -- Mirash hatte vom Spiel kaum mehr
Pause als für Körperversorgung gemacht -- saß as früh
morgens nachdenklich in der
Dramaturge und beobachtete die Ziege. Antagone kümmerte
sich um sie. Antagone war ein Kind, das bei diesem
Spiel mitspielen wollte, aber eher nicht bei den Straßenschlachten
beteiligt werden mochte. Oder nicht sollte, da war sich
Mirash nicht sicher. Flammenfinger betreute es hier. Sey
gab sich als sein Elter aus. Ob sey das nun im
Outernet auch war, oder nur im Spiel, wusste Mirash auch nicht.

Was Mirash nicht gewusst hatte, als as sich für Zeit entschieden
hatte, war, dass die Zeitfraktion als Motivation zum Lernen die Türen auch von
innen mit Zeittricks verschlossen hatte. Vorgesehen wäre
gewesen, dass Mirash eben so lange die entsprechende Magie
trainierte, bis as die Türen selbst öffnen und sich dadurch
freier bewegen könnte. Zeitkick war eine geduldige Lehrkraft. Nicht
alle in der Zeitbasis waren das. Mirash hatte Unterricht bei verschiedenen
Personen, die versuchten, guten Unterricht zu machen, aber für
Mirash funktionierte ihre Form von Motivation nicht. Vor
allem der künstliche Freiheitsentzug, damit das Öffnen der
Türen am Ende als Belohung funktionieren könnte, löste eher
Trotz in Mirash aus. "Du spielst noch lange genug, wenn du mit
Zeit überhaupt etwas anfangen willst. Du wirst die Welt hinterher
besser kennen und verstehen als die meisten der Spielenden in
den anderen Fraktionen. Aber für Zeit brauchst du halt
Geduld.", hatte Holgem motiviert. Wenn sie mal selber da war. Sie
war viel unterwegs. Ein anderen Mal hatte sie zu Mirash
gesagt: "Das ist eben das, was passiert, wenn du einer Gruppe
joinst, die ein Gefüge hat, das nicht so gut zu dir passt. Aber
mach dir keine Sorgen. Irgendwie raufen wir uns schon zusammen."

Zeitkick hatte Mirashs Unbehagen mit der Situation früh erkannt, aber
zunächst gemeint, dass es ja bald besser würde, wenn Mirash einen
bestimmten Satz an Fähigkeiten hätte, und ob as sich nicht einfach
darauf für diesen endlichen Zeitraum einlassen könne, aber als Mirash
sich bei ihr erkundigt hatte, wo as respawnen würde, wenn as von der
Festung herabspränge, hatte sie Mirash wortlos die Tür geöffnet, durch
die sie gekommen waren. Mirash hatte sie zögernd betrachtet, versucht, den
Gesichtsausdruck zu entschlüsseln, und dabei wohl irgendeinen
Verteidigungsmechanismus bei ihr ausgelöst. Ihre letzten
Worte waren gewesen: "Du wolltest doch unbedingt raus. Geh doch, wenn
du glaubst, dass das besser ist und du dich so sehr über
den Dingen fühlst."

Das jedenfalls ließ Mirash nicht kalt. Nun saß as hier und
grübelte. Es war früh morgens im Outernet. In Lunascerade war
nie so richtig Tag, aber dünnes Licht fiel durch eine unrealistisch
dunkle Wolkenschicht. Der Bodennebel waberte durch mehrere
Stockwerke der Stadt und war deshalb vielleicht nicht
mehr nur Bodennebel. Auf der anderen Seite war in dieser
stockwerkrigen Stadt überall Boden. Das Wasser glimmte grünlich.

Mirashs Blick wurde von der Ziege auf sich gezogen, die sich
in ein Kuscheltier verwandelte. Antagone hob sie auf und kam zu
Mirash an den Tisch. "Hast du einen Stift?", fragte es.

Mirash schüttelte den Kopf. Aber nun fiel ihm auf, dass der
Fleck auf der Ziege, der wie ein Geist aussah, in Kuscheltierform
anders wirkte als vor der Verwandlung. "Bleiben die
aufgemalten Muster, wenn sie sich zurückverwandelt?", riet as.

Das Kind nickte und drückte ihm das Kuscheltier in die Arme. "Ich
frage Flammenfinger nach Stiften. Passt du auf?"

Mirash versprach es. Aber bevor das Kind mit Flammenfinger zurückgekehrt
war, plöppte Gate wieder in ziegenartigere Ziegenform zurück, muhte und
stieg von Mirashs Schoß auf dem Tisch. "Du bist wirklich
eine schöne Ziege.", murmelte Mirash.

"Deine Fresse kommt mir vage bekannt vor.", begrüßte Flammenfinger
mit klangvoller Stimme. Und fügte dann hinzu: "Entschuldige, ich
drücke mich mal wieder vulgär aus. Soll ich mehr auf meine
Wortwahl achten?"

"Ist es abfällig gemeint?", fragte Mirash statt einer klaren Antwort.

"Hast du Sorge?", fragte Flammenfinger. Sey komplimentierte die Ziege
vom Tisch und setzte sich Mirash gegenüber. "Hast du was dagegen, dass
ich mich zu dir geselle?"

Mirash schüttelte den Kopf. "Also, dagegen, dass du dich setzt, habe
ich nichts. Wir sind nicht im Grünen auseinandergegangen, glaube
ich. Wir waren für letzte Woche morgens verabredet, weil du mich
ein bisschen einführen wolltest. Ich könnte verstehen, wenn du
sauer bist, wäre aber über direktere Kommunikation dankbar. Also, ja
ich habe Sorge, dass das abfällig gemeint war."

"Ah, da warst du ganz in Rosa, richtig?", fragte Flammenfinger.

"Das ist korrekt." Mirash setzte den Zylinder ab und platzierte
ihn am Rand des Tisches.

"Und dann bist du in die Zeitfraktion eingetreten und sie haben dich
ein bisschen festgenagelt.", mutmaßte Flammenfinger.

Mirash nickte. "Du weißt wohl besser als ich, wie das da
zugeht.", mutmaßte as.

"Davon gehe ich auch aus." Flammenfinger seufzte theatralisch. "Ich
nehme dir nichts übel. Als wäre ich als Leitung dieses Ladens nicht
gewohnt, mit verschiedenen Formen von Zuverlässigkeiten oder
Unzuverlässigkeiten zurecht zu kommen und sie einzuplanen. Am
Morgen nach unserem Kennenlernen ging ohnehin einiges drunter und
drüber."

"Ist etwas Spannendes passiert?", fragte Mirash neugierig.

"Emeralone ist verschwunden.", sagte Flammenfinger. "Das war die
Person mit Korona auf der Bühne."

Mirash erinnerte sich an den beeindruckenden Auftritt und
nickte. "Verschwunden heißt dann zwingend was anderes, als
dass rie gerade einfach nicht online ist?"

Flammenfinger wiegte den Kopf. "Es kann sein." Sey betonte
das Verb sehr skeptisch. "Das wäre äußerst untypisch."

"Gibt es Mutmaßungen?", fragte Mirash.

"Einige.", sagte Flammenfinger. "Emeralone trägt nicht ohne
Grund diesen Namen. Rie liegt mit einigen so ein wenig
im Clinch."

Mirash runzelte die Stirn. "Sollte mir der Name etwas
sagen?"

"Sagt dir Rubin Hood etwas?" Flammenfinger legte die
Flammenfinger aneinander. Der Trick verfehlte einfach
nie eine gewisse unterstreichende und ästhetische
Wirkung.

"Rubin Hood.", wiederholte Mirash nachdenklich für
sich. "Emeralone. Uffz, das ist schwierig. Emerald, ist
der niederelbische Name für Smaragd, richtig?"

"Ja, du bist da was auf der Spur." Flammenfingers Lächeln
nahm fast ser ganzes Gesicht ein.

"Aber Melone wiederum ist Kadulan für Bowler Hat. Das
Gemisch aus Kadulan und Niederelbisch ist also genau
umgekehrt als bei Rubin Hood. Vielleicht
wäre das Rätsel einfacher gewesen, hätte Emeralone auch
eine Melone getragen." Mirash war trotzdem gerade etwas von
sich selbst angetan, das Rätsel geknackt zu haben.

"So einen hatte ihs früher. Der Hut wurde
schon vor gut einem Monat gestohlen. Es
war ein besonderer Hut, der das gleiche violette Schillern
hatte wie ihs Haar." Flammenfinger senkte den einen Unterarm
auf die Tischplatte, um sich dort abzustützen und den anderen
in das Fell der Ziege, die neben Flammenfinger aufgetaucht
war. "Rie wollte ihn nicht durch eine einfachere Melone
ersetzen." Die Ziege muhte leise und genussvoll. "Jedenfalls
ist rie bekannt für romantisierte Verbrechen: Rie bricht
mit einer Gruppe von Leuten in
Basen ein, stielt Reichtum zu und verteilt ihn unter den
sozusagen Armen. Das macht verständnisvoller
Weise einige hier nicht unbedingt glücklich. Andere schon."

Mirash konnte sich gut vorstellen, dass Emeralones erstes
Ziel die Zeitbasis sein könnte. Das Thema war spannend, aber
saine Aufmerksamkeit wanderte vorübergehend zu Antagone. Es
hatte sich durch den Raum bewegt und hockte
nun am Rand des Wassers. Etwas
Großes lugte aus dem Wasser und schmiss sich platschend wieder
hinein.

Flammenfinger drehte sich nicht einmal um. "Henne, der
Hecht."

"Wer hat den Hecht Henne benannt?", fragte Mirash und
lachte auf.

"Antagone. Hast du Einwände gegen den Namen?" Flammenfingers
Sprachmelodie ließ keinen Zweifel übrig, dass die Frage
zwar spielerisch gemeint war, aber 'ja' eine unzulässige
Antwort sein würde.

"Kein Stück. Ich mag Gewagtes." Was ja auch stimmte.

"Das gefällt mir an dir.", sagte Flammenfinger leiser. Ohne
das ganze aufgesetzte Theater in der Stimme, mit dem sey
sonst absichtlich sachte übertrieb. Oder war
das Entfernen dieser Theatralik auch ein dramaturgischer
Trick?

In Mirash jedenfalls floss ein heißes Gefühl die
Luft- oder Speiseröhre hinab und bildete einen plötzlichen
Sog in sainem Brustkorb. Flirtete Flammenfinger mit
ihm? Und die viel wesentlichere Frage, sollte as
zurückflirten? "Du bist zu gut zu mir." As bemühte
sich dabei, einen spielerischen Ton in die
Stimme zu legen. Die Antwort auf die eigene Frage war
wohl ein vorsichtiges 'ja'.

Flammenfinger betrachtete Mirash ausführlich. "Du siehst
so aus, als hättest du nur diesen einen Satz Kleidung, in
dem du ziemlich drangsaliert worden bist."

Mirash schürzte die Lippen und nickte. "Bin ich wohl, und
ja, ich habe nicht mehr."

"Steht dir. Ob du willst oder nicht.", sagte Flammenfinger
mit einem Grinsen. "Ich habe nicht sehr viel schwarze Kleidung
da. Ich könnte dir einen Samtumhang bieten. Umhänge sind nicht
sehr praktisch, aber immerhin hat er einen leichten Rüstwert
gegen Magnetismus. Eisenwaffen, solange du sie darunter
trägst, werden weniger stark von Feldern beeinflusst."

"Das ist ein außerordentlich liebenswürdiges Angebot.", sagte
Mirash. "Ich habe Bedenken, wenn ich in die Zeitbasis
zurückkehre -- sollte ich mich dazu entscheiden --, dass
daran dann für alle erkennbar sein würde, dass ich weg war. Was
womöglich auch nicht so schlimm ist. Aber."

Flammenfinger hörte zu lächeln auf. Und nickte gemächlich. So
verständnisvoll hatte sey vielleicht noch nie gewirkt. "Da
steckt so viel drin, in dem, was du sagst.", murmelte sey. "Hier, nimm
die Ziege, wenn du willst." Sey reichte Mirash die Ziege, die nun
wieder Kuscheltierform hatte.

"Ich glaube, Antagone wollte sie bemalen.", murmelte Mirash, als
as sie entgegennahm. Sie war noch weicher als vorhin. Mirash
erinnerte sich daran, dass die Ziege as am Anfang geheilt
hatte. "Bist du sicher, dass die Kräfte so zufällig verteilt
werden? Oder könnte ich Gate etwa sympathisch sein?"

Flammenfinger kicherte leise und warm. "Irgendwann hat sie
sich mal überraschend mit Laserblick umgeschaut und dabei
so einige Leute zerlegt, die dachten, sie würden von ihr
gemocht. Nun, sie sind einfach respawnt, aber das Grammophon
hat nun nur noch einen halben Trichter. Einiges vom Dachgebälk
kam herunter und musste ersetzt werden. Und wir hatten den
Genuss einer Wasserheizung. Die hat es auch zerlegt."

"Wie sieht das so mit Angst vor der Ziege aus?", fragte Mirash.

Flammenfinger zuckte mit den Schultern. "Zweierlei hält uns
vom Fürchten ab." Flammenfinger hielt zwei Finger hoch und
entflammte sie nacheinander. "Sie ist niedlich. Und wir können
uns ohnehin nicht schützen. Sie wohnt hier und macht Chaos. It
is, how it is."

Der Hecht sprang ein weiteres Mal aus dem Wasser und planschte. Mirash
konnte beobachten, dass das Kind den Hecht schminkte. Henne schien das
zu freuen. Und Mirash beschloss, dass Antagone dann wohl im Moment
genug damit beschäftigt war, Tiere anzumalen, und nicht zur
Ziege gerufen werden musste. Irgendwie löste das aus, dass Mirash
sich erlaubte, die Kuschelziege für sich zu kuscheln und das tat
gut. Die Ziege nutzte diesen Moment, um sich zurückzuverwandeln, blieb
aber mit dem Bauch über Mirashs Schoß liegen wie eine Katze. Um
der Sache die Krone aufzusetzen miaute sie. Eine sehr schwere
Katze allerdings.

"Okay, es könnte wirklich wirken, als würde sie dich mögen. Sie
hat da wohl einen ähnlichen Geschmack wie ich.", räumte
Flammenfinger ein. "Verlass dich nur nicht zu sehr darauf."

Das war flirten, beschloss Mirash und blickte, die Arme um die
Ziege geschlungen, zu Flammenfinger auf. "Ist der Rat auf die
Zuneigung der Ziege bezogen, oder auch auf deine?"

"Beide." Flammenfinger setzte sich sehr gerade hin und
überschlug die Beine. "Warum bist du hier? Was ist dein
Begehr?"

"Ich kenne kaum einen anderen Ort. Ich habe keine anderen
Kontakte außerhalb der Zeitbasis.", sagte Mirash. "Aktuell
ist mein Begehr, von dir ein paar Dinge in Erfahrung zu
bringen. Vielleicht die versprochene Einführung in verspätet.
Wichtiger noch, was du damit gemeint hast, dass in dem,
was ich sagte, sehr viel drinstecke. Und vor allem: Flirtest du mit mir?"

"Wenn du willst, flirte ich mit dir.", antwortete Flammenfinger
in bewusst anzüglichem Ton auf die letzte Frage.

Ein weiteres Ziehen oder Rauschen lief durch Mirashs Körper. "Was wäre
es sonst, was du da tust, wenn ich nicht wöllte?"

"Schöner Konjunktiv.", kommentierte Flammenfinger. "Herumgealbere? Für
mich macht das keinen Unterschied. Ich bin gern spielerisch albern
mit Personen, die ich mag, bin sehr offensiv damit, zu sagen, dass
ich sie mag, und versuche dabei, nicht unangenehm zu sein. Bitte
sag mir ins Gesicht, sollte ich letzteres sein und es nicht anders
mitbekommen."

Mirash nickte. "Ich mag dich auch." Die Worte waren schneller
aus sainem Mund, als as mit einer bewussten Entscheidung
dafür gebraucht hätte. "Ich mag den Flirt-Stil sehr gern. Ich
sollte vielleicht dazu sagen, dass irgendwelche romantischen
oder fast beliebigen anderen Anziehungsgefühle bei mir beim
Kennenlernen oft sehr stark sind und dann schnell
verblassen."

Flammenfinger lächelte sanft und senkte den
Blick. Ein perfekter Theaterblick, so wie das eben
maskiert möglich war, mit schöner Handgeste, gerade so nicht
übertrieben. "Sehr fair, dass du das dazusagst. Du
bist mir aber auch keine Rechenschaft schuldig."

Mirash wünschte sich auf einmal Flammenfingers Finger
irgendwo an sainer Wange. Aber fühlte sich zu unsicher, das
Gespräch darauf zu lenken. "Was ist mit den anderen Fragen?"

Flammenfinger legte die Finger über den halben Tisch entfernt
von Mirashs Gesicht wieder aneinander. Sie sprühten
dieses Mal nur Funken, die ungewöhnlich langsam durch die Luft
flirrten. "Was macht dir Angst daran, dass andere in der Zeitbasis
herausfinden könnten, dass du hier gewesen bist? Kannst du noch
keinen Zeitverlangsamungszauber, der dem Abbremsen eines
gewissen Schlüssels dienst und solltest nicht hier sein können?"

Mirash nickte nicht zu offensichtlich aber schmunzelte. "Und
wenn es so wäre?"

"Wie bist du rausgekommen?", fragte Flammenfinger.

"Du sagtest, es stecke viel drin in dem, was ich sagte. Nicht, dass
du einen Haufen Fragen hättest.", erinnerte Mirash, statt zu
antworten.

"Egal, wie du die Fragen beantwortest: fast alle der Motivationen, die
du haben könntest, den Umhang abzulehnen, sprechen dafür, dass du
eine Enttäuschung erlebt hast. Im wörtlichen Sinne. Dass du
etwas anderes erwartet hättest, als du vorgefunden hast.", sagte
Flammenfinger und bemühte sich, einfühlsam zu klingen, was dieses
Mal nicht so gut klappte. Und Mirash fragte sich, ob das
Absicht war. "Und dann hast du diese ominöse Sache gemacht, in
Frage zu stellen, ob du überhaupt zurückkehrst."

Mirash nickte zögerlich. "Eigentlich möchte ich schon.", sagte
as. "Ich bin übrigens Mirash. Irgendwie fühle ich mich gerade danach, als
könnte ich dir das Mal sagen." War es ein unterbewusstes Ablenkungsmanöver
sainerseits?

"Ich danke dir.", sagte Flammenfinger. "Es wäre bei einem dritten
Treffen zwischen uns, solltest du zwischendurch neue Kleidung erlangen, vielleicht
auch anstrengend geworden, so zu tun, als hätte ich das noch nicht gewusst."

Mirash hob eine Augenbraue und runzelte die Stirn. Zum Glück
waren gerade beide Ausdrücke als Kombination sehr passend. Mirash
konnte ersteres nicht ohne letzteres. "Du hast mir was vorgemacht und
wusstest es die ganze Zeit."

Flammenfinger hörte nicht zu lächeln auf und nickte schuldbewusst. "Es
tut mir leid. Berufsgewohnheit."

"Woher weißt du meinen Namen?", fragte Mirash.

"Du bist eine Woche im Spiel und ich habe Kontakte. An mir geht
nicht so leicht etwas vorbei.", antwortete Flammenfinger.

"Wie sehr hängt deine plötzliche Offenheit damit zusammen, dass andere
das wissen und mir mitteilen würden, dein Vorspielen also
rasch auffliegen würde?", fragte Mirash.

Flammenfingers Lächeln erlosch. "Nicht." Sey machte eine Pause, von
der sich Mirash nicht sicher war, ob sie für Drama da war. "Ich
fand es nicht fair, dich im Unwissen zu lassen, nur weil du im Gegensatz
zu den meisten hier Spielenden das Game nicht so beherrschst, zu
erkennen, was wo zwischen den Zeilen gespielt wird. Die meisten
hätten mein Spiel längst durchschaut."

Mirash spürte das irritierende Gefühl des eigenen glatten
Gesichts und innerer Ruhe, während as wusste, wie sehr so eine Aussage andere
Leute verletzen konnte. Zeitkick zum Beispiel, das hatte
as inzwischen mitbekommen. Zeitkick hatte ein Problem damit, wenn mit ihr
über ihre Schwächen gesprochen wurde. Das hatte Mirash in
den zwei Unterrichtseinheiten miterlebt, in denen Holgem sie
beide gelehrt hatte. Es war interessanter Unterricht gewesen, aber
Zeitkick war hinterher mental etwas fertig gewesen.

Mirash verletzte es nicht. Trotzdem fühlte es sich
für as unkomfortabel an, auf
Flammenfingers Gutmütigkeit angewiesen zu sein, und as fragte sich, was
as noch nicht durchschaute.

"Du möchtest gern zurückgehen, sagtest du. Aber vorhin hast
du es in Frage gestellt.", lenkte Flammenfinger halb vom
Thema ab. "Es geht mich nichts an, aber wenn du darüber
reden möchtest, bin ich gern da."

Mirash schüttelte den Kopf. "Ich mag das Element Zeit. Ich mag
zumindest ein paar Leute in der Zeitfraktion. Ich habe mich
früher von Lehrmethoden nicht beeindrucken lassen, weil ich
das Fach interessant fand und nicht, wie es vermittelt
wurde. Ich denke, irgendwie wird das hier auch funktionieren."

Die Ziege kommentierte dies mit einem lauten Muhen und
zappelte mit den Hufen, bis Mirash sie von sainem Schoß
herunterließ.

"Schwierig." Flammenfinger neigte den Kopf. "Lehrende wollen
ihre Lehre natürlich gut bewertet haben. Das ist der einzige
Skill-Mechanismus. Wenn du nicht lehrst und dafür gut
bewertet wirst, kannst du die Qualität
oder die Anwendungsdauer einer Fähigkeit nicht steigern."

"Was, wenn ich nicht gut bewerte?", fragte Mirash. "Was
wollen sie dann schon machen? Außer ihre Lehre an meinen
Bedarf anpassen."

"Warum bist du hier? Warum hast du Angst, mit einem Umhang
zurückzukehren?", fragte Flammenfinger zurück.

Das hatte sey vorhin schon einmal gefragt. Mirash glaubte, dass
Flammenfinger hier falsche Vorstellungen hatte, und hatte sie
eigentlich nicht korrigieren gewollt. Aber nun fühlte as sich
doch danach, weil Flammenfinger etwas für as richtig gestellt
hatte. "Nicht meinetwegen." Es ging um Zeitkick.

"Es geht um die Person, die dich rausgelassen hat?", schloss
Flammenfinger überraschend geistesgegenwärtig.

Mirash erschreckte es fast. "Wüsstest du sogar wer?"

"Ich habe meine Einschätzungen. Wenn ich sie ausspreche, kann
ich aber vielleicht aus deiner Reaktion eine Antwort ablesen.", warnte
sey.

"Ich werde mein Gesicht versteinern.", versprach Mirash.

"Die Person, mit der du zuletzt weggegangen bist.", riet Flammenfinger
richtig.

Mirash hatte in Gedanken Spontanreaktionen, unterdrückte aber für ein
paar Momente wie geplant jeglichen Output. Und ratterte sie
dann herunter, als as sich halbwegs
sicher war, dass die Fragen sich nicht gut für Mutmaßungen
seitens Flammenfinger eigneten. "Deren Namen du kennst, aber warum
nicht sagst? Und warum schätzt du das so ein?"

"Natürlich kenne ich Zeitkicks Namen.", bestätigte
Flammenfinger. "Gewohnheitsgemäß nenne
ich nur Namen von Personen, die recht bekannt sind. Das ist
sie nicht. Wobei ich durchaus das Vergnügen hatte, sie ein
wenig kennen zu lernen."

Mirash entschied, provokativ zu fragen: "Was hältst du von ihr?"

Flammenfinger blickte Mirash lange unergründlich an. Statt, dass
einzelne Finger in Flammen aufgingen, loderten sere aneinandergelegten
Hände in einer riesigen Stichflamme auf. "Hast du schon Level fünf?"

"Ist das ein Themenwechsel oder hat das etwas mit der Frage zu tun?", fragte
Mirash.

"Es ist ein Themenwechsel." Flammenfinger klang gefährlich leise. "Möchtest
du, dass irgendeine Person eine Antwort auf so eine Frage bekäme, wie
ich dich einschätzen würde?"

Mirash senkte den Blick. "Du findest mein Verhalten nicht in
Ordnung. Das verstehe ich. Es tut mir leid."

"Du hast geschaut, wie weit du gehen kannst.", beruhigte
Flammenfinger. "Und vor allem wolltest du herausfinden, wie ich
reagiere."

Mirash nickte. "Es war trotzdem nicht in Ordnung."

"Es ist ein fieses Spiel. Natürlich darfst du dich entscheiden, fies
zu spielen.", widersprach Flammenfinger. "Meine Reaktion darauf ist
eben nur einfach keine."

Sie schwiegen ein paar Momente. Erst dadurch fiel Mirash auf, dass das
Kind mit der Ziege damit beschäftigt war, fangen zu spielen. Der Hecht
hatte einen Teil seines riesigen Kopfes auf das Ufer gelegt und sah
mit seinen rosa Lippen und dem grell-blauen Lidschatten zu. Die
Kiemen verweilten größtenteils unter Wasser. Irgendwo am anderen
Ende der Dramaturge hatte sich außerdem ein Grüppchen aus
vier Personen zusammengefunden, die leise und immer wieder
lachend ein Kartenspiel spielten. Wobei, eigentlich hatte Mirash
deren reinkommen vorher wahrgenommen, aber die Wahrnehmung noch
nicht registriert.

"Ich habe gestern Level fünf erreicht.", antwortete Mirash.

"Dann kannst du jetzt ausbilden. Das geht ab Level fünf.", informierte
Flammenfinger.

Das wusste Mirash auch. "Hast du eine bestimmte Person dafür im
Auge?" Ausbilden bedeutete vor allem, dass die Dauer oder Intensität
der Anwendung der Magie gesteigert werden konnte. Eigentlich reichte
für den Schlüsseltrick das Ausgangsmaß, wenn Mirash denn die Bewegungen
für den Zauber ausreichend präzise hätte nachahmen können. Die
Zeitfraktion fand, dass es ein gutes Mittel der Pädagogik wäre, wenn
Mirash diese Präzision erlernt hätte, bevor as ausgebilden würde.

"Mich.", schlug Flammenfinger vor. "Ich habe gerade zwei, drei Stündchen
Zeit. Das sollte, wenn du ausbildest, reichen, dass du eins der
Schlösser aufbekommst."

"Und du profitierst auch irgendwie davon?", mutmaßte Mirash schmunzelnd.

"Ich würde es hinterher vielleicht endlich auch können. Aber
vielleicht braucht es noch ein oder maximal zwei weitere
verzweifelte neue Personen in der
Zeitfraktion." Flammenfinger legte sehr sachte die lediglich
heiß glimmenden Finger aneinander und lächelte. "Ich verstehe, wenn
du den Deal nicht eingehen möchtest."

Oh, doch, Mirash wollte. Mindestens für Chaos. Aber auch wegen
des Flirtens, wegen Flammenfingers Gesellschaft und weil as
dann freier wäre. "Du bist also nah an ungefähr zwanzig Wochen
Ausbildung für diese Sache.", widerholte Mirash Zeitkicks
Abschätzung.

"Ungefähr. Ich bin hier eines der Urgesteine im Spiel.", bestätigte
Flammenfinger.

Mirash nahm den Zylinder vom Tisch und setzte ihn wieder auf. "Deal."
