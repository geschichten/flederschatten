Umplanen
========

Mirash fühlte den vorsichtig schmunzelnden Gesichtsausdruck im
eigenen Gesicht, als as sich mit Flammenfinger gegenübersaß und
die Slik-Figur nun zum dritten Mal erfolgreich ausreichend langsamer
durch die Luft gen Tischplatte segelte, dass, wäre sie ein
Schlüssel, Mirash ihn hätte rechtzeitig greifen können. Das Lächeln verblasste
nicht, als eine Hand sie aus der Luft griff, die da vorher noch
nicht war. Mirash kannte die Spitzenhandschuhe und blickte in
Holgems Gesicht. Immerhin lächelte auch dieses.

"War es das wert?", fragte Flammenfinger. "Das zur Schau stellen
deiner ach so mächtigen Fähigkeiten?" Die Tür der
Dramaturge fiel ins Schloss.

"Den Teleport von der Tür zum Tisch meinst du?" Holgem stellte
die Slik-Figur auf das Spielbrett, das Flammenfinger vorhin auf dem
Tisch platziert hatte.

Nicht auf das Feld, wo sie hingehört hatte. Mirash war so frei, die
Position zu korrigieren. "Du bist am Zug.", informierte as Flammenfinger.

Sere kaum sichtbare Augen hinter der Maske loderten kurz
auf. Im wörtlichen Sinne natürlich.

"Erzähl du mir was von Zurückhaltung, wenn es um das zur Schau
stellen von Zauber-Fähigkeiten geht.", sagte Holgem schmunzelnd.

Damit hatte sie wohl recht. Flammenfingers Zauber waren ausschließlich
für Show gedacht. Zumindest von allem, was Mirash bisher mitbekommen hatte.

Flammenfinger lächelte milde. "Erinnerst du dich an einen gewissen
Streit? Über einen gewissen Ernst, den gerade ich als große Vorbildfunktion
mal an den Tag legen sollte? Auf diese Show-Art würde ich neuen Leuten
einen falschen Eindruck von Ernstlosigkeit vermitteln und sie zu
Unvorsicht anstacheln und so? Und auf die Idee bringen, dass alles ganz
einfach wäre?"

Holgem wandte sich an Mirash. "Und? Was ist dein Eindruck? Was
glaubst du, wie lange Flammenfinger daran gearbeitet hat, diesem
Nickname gerecht zu werden?"

Mirash schloss die Augen. Dann senkte as auch den Kopf, als
as einfiel, dass das Augenschließen mit der Maske
nicht so sichtbar war, und legte die nicht
funkenden Finger aneinander. Zwanzig Wochen hatte es Flammenfinger
zum Lernen dieses einfachen Zeittricks gekostet, weil
Flammenfinger nicht zu Zeit gehörte. Optik auf
der einen Seite war die zweiteinfachste Magie-Form. Auf der
anderen Seite waren solche Dinge, die Flammenfinger machte, auf
der Bühne gelandet, und dort landete bestimmt nicht viel, was
Leute in zwei Tagen lernten. Oder doch? Mirash überlegte, was es schwieriger machen
könnte, so eine Illusion zu erzeugen, und die erste Idee, die
ihm dazu kam, war, dass das Licht ja irgendwo herkommen musste. Vielleicht
war es mehr Energie die in die Tricks hineinfloss, je weiter die
Quelle des geschöpften Lichts entfernt war, und je mehr die Erscheinung
im Vergleich zur ursprünglichen verändert würde. Wenn Flammenfinger
Flammen erscheinen ließ, dann kamen sie wie aus dem Nichts.

Mirash öffnete die Augen wieder und hob den Kopf. As widerstand
dem Drang, einfach 'lange' zu sagen und rechnete noch kurz.
Zwanzig Wochen waren grob 5 Monate. "Zwischen zwei und
fünf Jahren, schätze ich."

"Zwei etwa, bis ich die Tricks grundsätzlich ausführen konnte, aber
jeweils nur so ein paar Mini-Momentchen.", bestätigte Flammenfinger
grinsend und blickte Holgem mit schief gelegtem Kopf herausfordernd
an.

"Du bist gut!", lobte diese. "Das hätte ich dir nicht zugetraut, Mirash,
muss ich zugeben."

"Falls du gehofft haben solltest, dass mich das Lob irgendwie
motiviert oder pädagogisch wertvoll ist: Nein.", blockte Mirash
vorsichtshalber ab.

"Was ist denn aus deiner Sicht pädagogisch wertvoll?", fragte Holgem. Sie
setzte sich. Auf einen Stuhl, der da bis gerade auch nicht gestanden
hatte. Sie hatte zu dem Zweck kurz geflirrt.

Mirash versuchte, durch die entstandenen Geräusche zu rekonstruieren, was
gerade im Bruchteil einer Sekunde geschehen sein mochte. Dies war einer
der sehr deutlichen Momente, in denen as vermutete, dass Personen
ohne saine Sehbeeinträchtigung vielleicht doch hätten gerade so ausmachen
können, dass Holgem sich einfach sehr schnell durch den Raum bewegt
und einen Stuhl geholt hatte. "Der Trick ist, dass ich meine eigene
Motivation mitbringe.", erklärte as. "Lasst mich mitgestalten. Stellt
keine künstlichen Regeln ohne mein Einverständnis auf. Redet mit mir. Fragt
mich, was ich will. Das fällt mir spontan ein."

"Ich habe dich gerade gefragt, was du willst.", erinnerte Holgem, bemühte
sich dabei halbwegs erfolgreich, sehr freundlich zu klingen. Obwohl
Mirash fand, dass das doch eine weite Auslegung sainer Worte von
eben war. "Ich finde deine Vorschläge gut. Ich bin hier nicht hergekommen, weil
ich dich bestrafen wollte oder so etwas. Nicht, dass du das denkst, wo du
unsere Pädagogik für so furchtbar hältst. Ich hatte einfach nicht schnell
genug verstanden, was du brauchst oder vielmehr, was du nicht brauchen
kannst. Ich würde dich gern in größere Pläne der Zeitfraktion einweihen und
mit dir besprechen", Holgem ließ sich von Flammenfingers Grummeln
unterbrechen.

"Diese manipulative Übertreibung, dass Mirash eure persönliche Pädagogik
für furchtbar hielte, musste die sein?", fragte sey, die gleiche
Wortwahl wie vorhin wählend. "Ich könnte dir jetzt noch einmal analysieren,
warum diese Art nicht das harmlose Scherzen ist, von dem du denkst, dass
es das ist."

"Als ob du mit deiner Theaterspielerei nicht viel eher manipulativ wärest.", erwiderte
Holgem. "Ich habe keine Lust zu Streit. Kriegen wir das irgendwie anders
hin?"

Flammenfinger warf einen ausgiebigen Blick auf Mirash. Schließlich
sagte sey leise und nicht weniger theatralisch: "Wo sie recht hat.", bevor
sey sich wieder Holgem zuwandte: "Und das war dann Ad-Hominem-Argument
Nummer zwei. Mich zu belasten entlastet dich nicht."

"Tut es nicht.", stimmte Holgem gelassen zu. "Es macht dich wiederum nur
relativ ungeeignet, Mirash über eventuelle Intrigen oder Manipulationen
aufzuklären. Wir haben in zwei Stunden unsere Ratssitzung. Da sitze
dann nicht nur ich. Das, dachte ich, wäre eher ein guter Ort für as, sich
ein eigenes Bild der Lage zu machen. Meinst du nicht?"

Flammenfinger reagierte nicht gleich, aber nickte dann schließlich. "Wenn
ich gerade zum Manipulieren aufgelegt wäre, würde ich Mirash davon
wohl überzeugen. Aber so?"

Mirash kicherte. "Das ist verwirrend und witzig. Du möchtest also eigentlich
schon, dass ich da hingehe?"

"Eigentlich", Flammenfinger machte eine künstliche Pause und legte die
Finger aneinander, die dabei, wie so oft, aufflammten, "möchte ich, dass
du deine eigenen Entscheidungen fällst, und wir nicht über dich reden, als
wärest du nicht da."

"Flammenfinger, so unwahrscheinlich das ist, aber in diesem Punkt hast
du sehr recht.", stimmte Holgem zu.

Mirash spürte die Blicke beider abwartend auf sich, schmunzelte und
rückte den Zylinder zurecht. "Eine Versammlung, auf die zwei Personen, die
sich streiten, mich beide schicken möchten, klingt durch diesen
Umstand ja extrem interessant." As drehte das Spielbrett eine
Nuance, sodass es nur noch fast parallel zu den Tischkanten ausgerichtet
war. "Ich würde gern zuvor dieses Partie Slik zu Ende spielen. Ich
denke, das sollte nicht lange dauern und ich sollte pünktlich sein
können."

Flammenfinger rückte das Brett wieder richtig. Das war Teil ihres
Flirtens während der letzten Stunden gewesen. "In der Tat nicht.", sagte
sey. "Ich mache diese Karpfade und du bist platt."

Mirash schluckte. Die Möglichkeit der Karpfade, auch Weg des Dunkels
genannt, hatte as völlig übersehen. Die neue Figuren- und
Felder-Konstellation war nicht direkt die Matt-Situation, aber
nun lagen die entscheidenden Züge klar vor ihm. "Du bist gut.", murmelte
as.

"Du auch.", flüsterte Flammenfinger flirty. "Sehr gerne wieder."

---

Oh, fosh, was für eine Person. Als Mirash neben Zeitkick an einem
Pult im Deeskalat saß, war sain Körper immer noch voll von diesem
Gefühl, das Flammenfinger in ihm entflammte. Lodernd und in Wellen durchlief
es sainen Körper, immer wenn as kurz vergessen hatte, an sem zu
denken und sey ihm wieder einfiel.

Die Versammlung lenkte allerdings gut ab. Es gab eine Vorstellung der Neuen -- also
von Mirash -- aber auch die anderen stellten sich Mirash kurz vor. Es gab
ein Zusammenfassen der Lage. Mirash wurde auch daraus nicht ganz klar, was
Elementeile waren. In der vergangenen Woche hatte die Zeit-Fraktion
wohl ein A-Elementeil ergattern können, aber das C-Elementeil von der Woche
zuvor war zur Abwechslung an die Optik-Fraktion gegangen. Jene bekam
eher selten direkt eines ab, sondern eher später in Verhandlungen, oder
indem die Fraktion fremde Basen infiltrierte.

Tatsächlich war der Zeit-Fraktion bekannt, dass die Optik-Fraktion
aber eigentlich gerade ein E-Elementeil brauchte, dafür aber
die Magnetismus-Fraktion das C-Elementeil, weshalb es nahe lag, das
ein Tausch stattfinden würde. Die Magnetismus-Fraktion, so erklärte
Holgem, war recht tauschwütig.

"Sie sind ja weniger koordiniert als wir, aber sie gleichen das etwas
durch Handel aus, an dem sie uns nicht beteiligen." Holgem stand
an einer Tafel auf einem kleinen Podest, während die gekrümmten Tische, die eigentlich
Teil eines runden großen Tischs für die Friedensverhandlungen
gewesen waren, einzeln in Halbkreisen aufgereiht waren, sodass
alle gut zur Bühne blicken konnten. "Das ist
ja auch an sich völlig in Ordnung. Niemand sagt, dass das schlechte
Methoden wären. In diesem Fall habe ich eine Idee, wie wir vielleicht
Horandas Plan von damals doch in die Tat umsetzen könnten. Mit
Mirash in der Hauptrolle, wenn as möchte."

Mirash hatte tatsächlich eigentlich nicht damit gerechnet, eine
besondere Rolle in dieser Sitzung zu spielen. Eigentlich war saine
Erwartung eher gewesen, dass Holgem as mit ein, zwei anderen nach
der großen Sitzung zur Seite nehmen würde, um zu besprechen, wie das
mit ihnen weitergehen sollte. "Ich bin positiv überrascht, nun eure gesamte
Aufmerksamkeit zu genießen." Mirash bemühte sich, es irgendwo genau zwischen
ironisch und ernstgemeint klingen zu lassen, und rückte wieder einmal mit
den Fingerspitzen den Zylinder zurecht.

"Meinen Plan?", fragte eine dünne Stimme.

Mirash blickte sich zu Horanda um. Ohne die Nametags wäre as bei der
Menge neuer Namen aufgeschmissen gewesen, aber Horandas Name mit
den Pronomen 'xier, xies(e), xiem, xien' schwebte über der leicht
zitternden Gestalt. Horanda war schmal gebaut, trug eine ärmellose
Weste mit leicht glitzrigen Nadelstreifen und fingerlose, schwarz-glitzernde
Spitzenhandschuhe, die mit Armgürteln an den
Oberarmen befestigt waren, sowie einen grazilen
samtenen Hut mit schwarzem Netz, das die Maske halb verdeckte.

"Du erinnerst dich, woran er gescheitert ist?", fragte Holgem.

"Dass er zu riskant ist und wir alles verlieren könnten?", fragte
xier unsicher. "Zumindest hattest du damals gesagt, er sei abstrus."

"Das tut mir leid.", sagte Holgem. "Es ist eben eigentlich kein Plan, der
von einer neuen Person ausgeführt werden sollte, um niemanden gleich
zu frustrieren, und gleichzeitig einer, bei der
schon länger trainierende Zeit-Personen alles bisher erreichte
leicht verlieren könnten. Aber Mirash ist eine besondere neue Person." Holgem
machte eine kurze Pause, in der sie unsicher wirkte, was wirklich
selten vorkam, soweit Mirash mitbekommen hatte. "Willst du den Plan
selbst vorstellen, oder soll ich?"

Mirash riet an diesem Abend das zweite Mal falsch: As hätte diesem
unsicheren Charakter nicht zugetraut, so spontan die Bühne zu
übernehmen. Xier stand auf, ging unsicheren Schrittes nach
vorn und trat die eine Stufe hinauf, wo
Holgem bei der Tafel stand. Holgem stellte xien noch einmal
vor, bevor sie wiederum die Bühne verließ und sich ins Publikum
setzte. Auf Mirash macht sie den interessanten Eindruck einer
viel älteren Lehrperson, die sich aus Spaß in eine Gruppe Kinder
einreihte. Nicht, dass es schlimm gewesen wäre, sich wie ein Kind
zu fühlen.

"Ja, also, es ist schon ein gutes halbes Jahr her, dass ich
den Plan das erste Mal dargelegt habe.", sagte Horanda und
holte erst einmal hastig Luft. "Verzeiht, wenn ich mich
verhaspele."

Ein motivierend zustimmendes Gemurmel ging durch die
Zuhörenden. Mirash beteiligte sich nicht daran, sondern
musterte xiem weiter. Xies Haar war schwarz, schulterlang
und schloss dort mit einer halben Innendrehung ab. Xier
trug einen eng anliegenden Rock und Lackstiefel bis
zu den Knien, dazwischen Netzstrumpfhosen.

"Wir haben damals darüber nachgedacht, wie wir mit dem
Problem Flederschatten umgehen. Einigen von uns kamen immer
wieder Gerüchte zu Ohren, dass
Flederschatten etwas Böses gegen uns plane. Wir konnten aber
nie genau herausfinden, ob das einfach Leute waren, die uns
Angst machen wollten, oder ob an den Plänen was dran ist. Wir
konnten die Quellen nicht identifizieren. Wir
haben uns darüber Gedanken gemacht, wie wir an sicherere
Informationen kommen könnten. Soweit die
Vorgeschichte.", berichtete Horanda. Xier wurde beim Reden
allmählich schneller, aber nicht unbedingt sicherer. Und
schließlich sprach xier in einem sehr raschen Tempo, das für nicht
maschinell wiedergegebene Sprache erstaunlich gut zu
Mirashs Aufnahmefähigkeit passte. "Mein Vorschlag war, bei so einer
Übergabe zwischen anderen Fraktionen, wie nun zwischen Optik
und Magnetismus wahrscheinlich ausstehend, für die ja oft die
Dramaturge entsprechend ungefährlichen, neutralen Grund
bietet, in selbiger unsere Sprengladungen unterzubringen, für
die wir schließlich auch bisher keine ausreichend lohnende
Verwendung haben. Wir würden sie mit Zeitauslösern ausstatten, sodass
sie im Zweifel, wenn sie nicht vorher eingesammelt und entschärft
werden, die Dramaturge in die Luft jagen, einschließlich
aller Anwesenden. Was ihnen zumindest ihr Inventar sowie
ihr Tauschmaterial kosten sollte."

Mirash runzelte abermals, wie vorhin, die Stirn und hob
eine Braue. Weil Horanda den Blick auffing und unterbrach
wagte Mirash einzuwenden: "Das klingt zwar durchaus
nach Chaos, aber nicht nach welchem, in dem ich die Hautprolle
spielen mögen würde. Ich mag die Dramaturge bisher."

Horanda grinste. "Das Risiko, dass es zum Hochjagen der Dramaturge
kommt, ist tatsächlich im Plan einberechnet, aber gering.", sagte
xier. "Die Idee dabei ist, dass du behaupten würdest, -- also
falls du die Hauptrolle spielen würdest --, dass Flederschatten
dich schicke, und du für Flederschatten rausfinden sollst, was
für Mordanschläge auf Flederschatten geplant wären. Du behauptest
weiterhin, dass Flederschatten vor der Tür warten würde, damit
die Leute nicht fliehen könnten. Du sagst also, Informationen
über Mordanschlagsplanungen auf Flederschatten, oder Boom!" Xier
warf einen abwartenden Blick auf Mirash.

"Sind Mordanschläge auf Flederschatten geplant?", fragte Mirash.

"Keine konkreten, von denen wir wüssten.", antwortete Holgem. "Wir
haben durchaus den Plan, Flederschatten irgendwann etwas entgegensetzen
zu können, aber im Moment kann das niemand. Flederschatten macht, was
Flederschatten will, und hat potenziell die Macht, das ganze Spiel
in eine andere Richtung zu lenken. Und es klingt eben durch, dass
Flederschatten auch den Plan dazu hat. Wir wüssten gern genaueres
über den Plan."

Mirash nickte. As wusste überhaupt nicht, was as von Flederschatten
halten sollte, aber verstand sehr wohl, dass es einen Bedarf gab, eine
sehr overpowerte Person mit der Macht, alles zu verändern, besser
zu kennen. "Ich verstehe noch nicht so recht, wie der Plan dazu
führt, mehr über Flederschattens Pläne zu erfahren. Bisher ist
es kurzgefasst: Vielleicht jagen wir die Dramaturge
in die Luft. Unabhängig davon, ob es klappt oder nicht, schieben wir das Flederschatten
in die Schuhe und betreiben auf diese Art Rufmord."

Horanda lachte ein unsicheres, aber sehr amüsiertes leises Lachen. "Ich
liebe deine Zusammenfassung.", sagte xier. "Flederschatten hat sich
eine Zeitlang viel in der Dramaturge aufgehalten. Wir mutmaßen, dass
Flederschatten den Raum mag und weiterhin im Auge behält. Und dass
Flederschatten mit einer großen, großen Menge Zeitmagie durchaus in der
Lage wäre, die Sprengung zu verhindern."

"Indem diese sagenhafte Person den Stuhltrick anwendet, den Holgem
vollführt hat, also sehr schnell in allen Ecken des Raums gleichzeitig
ist?", fragte Mirash.

"Du hast in der Dramaturge Zeitmagie angewendet?", fragte eine bisher
noch nicht beteiligte Stimme überrascht.

"Zur Abwechslung ein bisschen." Holgem klang nicht unbedingt begeistert, aber
auch nicht allzu genervt. "Und jein. Ja, prinzipiell der Trick, aber noch
etwas mächtiger. Einfach nur Dinge viel schneller erledigen, wird nicht
ausreichen, ohne dass für alle anderen auch die Zeit abgebremst wird. Also, wenn
Flederschatten die Dramaturge rettet, dann wirst du vorübergehend Dinge
nur in Zeitlupe machen können."

Mirash nickte zögerlich. "Das klingt in der Tat sehr mächtig."

"Der mächtigste Charakter im Spiel.", wiederholte Zeitkick. Sie wirkte
in dieser großen Runde stiller und zurückhaltender. Mirash
war sich nicht sicher gewesen, ob noch etwas zwischen ihnen stand und
ihre Zurückhaltung daran lag, tippte aber nun eher auf Scheu vor großen Gruppen.

"Weiter im Text." Horanda wirkte auf einmal sehr aufgeregt. "Flederschatten
könnte die Explosion verhindern, und das ist auch im Prinzip unsere Hoffnung. Wenn
nicht, dann haben wir den Ruf ziemlich gut geschädigt und Flederschatten
bekommt zumindest weniger leicht Unterstützung." Horanda wirkte kurz, als
müsste xier sich erst wieder sammeln und zappelte dabei mit den Fingern. "Wenn
aber die Explosion erfolgreich verhindert wird, dann wird Flederschatten
wissen wollen, was dir denn mit dem Rufmord einfiele. So zumindest schätzen
wir alle Flederschatten einigermaßen ein."

Wieder machte Horanda eine Redepause, die Mirash zum Nachdenken und
Revuepassieren der Argumente nutzte. "Aber, sollte ich die Person sein, sind
trotzdem viele Leute, einschließlich Flederschatten, sehr sauer auf mich. Weshalb
ihr dafür alle zu viel zu verlieren hättet und mich fragt.", hielt Mirash
fest, was as verstanden hatte. "Wo hilft das jetzt wem?"

"Du würdest Flederschatten offenlegen, dass wir dich zu Rufmord angestiftet
haben, aber darfst ansonsten so tun, als wärest du nicht so
glücklich mit uns und dem Outcome. Ich würde davon
ausgehen, dass Flederschatten das neugierig genug macht, dich zu
schützen.", erklärte Horanda. Mirash bemerkte, wie xiese Stimme sich
fast überschlug. "Da du, wie du gerade selbst gesagt hast, aber ganz
schön viele saure Leute auf dem Hals hast, und wir als Zeitfraktion auch
so tun würden, als hätten wir dich nicht mehr lieb, weil du den Plan
vergeigt hättest, -- so würden wir das darstellen --, wird Flederschatten
dich mit in Flederschattens Basis nehmen müssen dazu, weil du nirgends
sonst auch nur halbwegs sicher wärest."

Mirash nickte langsam. "Also ist der Ziel des Plans, dass ich hinterher Zugang zu
einer Basis und zu Informationen bekomme und vielleicht langfristig gespieltes Vertrauen
aufbauen kann.", schloss as. "Ihr hättet mich gern spionierend."

"Ich dachte mir, das könnte etwas für dich sein.", bestätigte Holgem. "Du
wolltest viel Eigenverantwortung, viel Freiheit und gute Lehre. Flederschatten
hat viel gelehrt und sollte da, nun, pädagogisch auch was drauf haben. Und
sehr viel Bedarf an Lernenden, weil ja Personen, die neu ins Spiel kommen und
Zeit wählen, normalerweise eher zu uns kommen. Du hältst dich nicht unbedingt
an vorgesehene Spielmechaniken und Regeln, und du hast, so scheint es, eine
Vorliebe dafür herausgefordert zu werden."

"Oh ja, letzteres stimmt definitiv." Mirash musste feststellen, dass Holgem
as überraschend gut einschätzte, dafür, dass das am Anfang alles nicht so
gut geklappt hatte. Vielleicht brauchte Holgem lediglich -- passend zur
gewählten Fraktion -- Zeit, um sich auf neue Personen einzulassen.

"So grundsätzlich erstmal, hättest du Interesse? Oder sollen wir
direkt aufhören, den Plan mit dir auszufeilen?", erkundigte sich Holgem. Sie
stand dabei erneut auf und betrat schreitend das Podest, ohne Mirash aus
den Augen zu lassen.

"Ich möchte gern sehr dringend eine genaue Einschätzung erlangen können, wie
groß das Risiko ist, dass die Dramaturge in die Luft fliegt, und
davon abhängig sehr genau abwägen, ob ich jenes wirklich eingehen möchte.", betonte
Mirash. "Insbesondere: Wie sicher überlebt Gate das? Oder würde Gate respawnen?
Aber ansonsten bin ich durchaus interessiert."
