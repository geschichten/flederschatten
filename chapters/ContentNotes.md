### Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Das ganze Buch ist in Sachen Gewalt
harmloser als fast alle Fantasy-Bücher, die ich kenne. Ich bin hier
ausführlich.

Ich nehme außerdem teils sehr selten Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch

**Zentrale Themen:**

- Physik
- Sterben in Spielen
- Waffen, wie Degen, Schwerter, auch Schusswaffen wie Bögen
- Kampfspiel
- Wettkampf
- Feuer, als Stilelement, dauernd
- Verletzungen In-Game
- Gaslightendes Verhalten einer toxischen Gruppe
- Flirty Main Character (aber es wird, wenn es passiert, bewusst
aus dem Subtext geholt und direkt kommuniziert.)

**Einzelne Szenen**

- Sex
- Genitalien
- Körperflüssigkeiten, Schleim
- Schleim auflecken
- emotionales, selbstverletzendes Verhalten
- Fäkalien
- Ein Gespräch über Suizid, das aber eine etwas andere Bedeutung hat.
- Bedrohung
- Reclaimte Slurs
- Gespielte, abgesprochene Übergriffigkeit
- Dysphorie erwähnt
- Nacktheit
- Masturbieren
- Körperflüssigkeiten
- Ekel?
- Stalking erwähnt

### Elemente

- gaslightendes Verhalten einer Person

### Zeitkick

- emotionales, selbstverletzendes Verhalten

### Enttäuschung

- Gaslighting
- Offensives Flirten

### Umplanen

- Bedrohung

### Gemetzel

- Ein Gespräch über Suizid, das aber eine etwas andere Bedeutung hat.
- Fäkalien
- Drohung
- Gemetzel

### Planung

- Reclaimte Slurs
- Sex
- Küssen
- Romantik
- Gespielte, abgesprochene Übergriffigkeit
- inter Repräsentation, die noch ein SR braucht
- Dysphorie erwähnt
- Genitalien erwähnt

### Sex

- Partielle Nacktheit
- Sex
- Genitalien angefasst
- Körperflüssigkeiten, Schleim
- Schleim auflecken

### Besprechen

- Kuscheln
- Küsschen
- Gaslighting

### Wald

- Gaslighting
- toxische Beziehungen in Rückblende
- Masturbieren
- Körperflüssigkeiten
- Ekel?

### Fallen

- Stalking erwähnt
