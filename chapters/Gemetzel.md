Gemetzel
========

"Es tut mir leid, dass ich manchmal so patzig bin.", sagte Zeitkick
schuldbewusst. "Ich wäre vielleicht einfach gern mehr wie du. Naja, außer
die Sache mit der Präzision."

Die Präzision von Bewegungen klappte bei Mirash immer noch nicht. As war nie
gut gewesen, Bewegungen anderer gut nachzuahmen. As lernte an sich
besser aus Büchern. Oder indem EM-Anzüge as sachte in eine Form
pressten. Aber das wäre keine Lehre durch andere, und Mirash
wollte die Spielmechanik in diesem Punkt nicht austricksen.

Es war tief in der Nacht. Über ihnen leuchteten die volle Mondscheibe
und allerlei Gestern. Gestern, -- Mirash musste einen Augenblick
schmunzeln. Es war das in der Vergangenheit, allerdings wahrscheinlich nicht erst
gestern abgestrahlte Licht von Sternen, sodass beide Betonungen
des Wortes zugleich Sinn ergaben. In der einen Weise
philosophisch und etwas nerdig und in der anderen lapidar. Wobei, hier in der
Virtualität war es natürlich frisches Licht. Es simulierte nur
längst vergangenes Verbrennen von Himmelskörpern. Es war jedenfalls
milde romantisch. Unter ihnen lag die Stadt. Still. Irgendwo
glimmte ein wenig Fackellicht, oder die Illusion davon heraufbeschworen
durch eine Optik-Person, das in den unteren Stockwerken der Stadt über
eine Brücke wanderte, sonst Dunkelheit und
Nebel. Sie saßen nebeneinander auf der Mauerkante des Geländes
der Zeit-Basis und blickten hinab.

"Magst du beim nächsten Mal dann hinterherschieben, dass du gerade
in so einer Stimmung bist, in der du solche Dinge sagst?" Mirash
hoffte, dass das keine unmögliche Bitte war.

"Es fühlt sich einfach so unfair an!", fuhr Zeitkick auf. "Und
ich würde ja am liebsten behaupten, dafür könntest du nichts, weil
du eine liebe Person bist, aber ich bin nicht einmal sicher."

"Was genau fühlt sich unfair an?", fragte Mirash.

"Ich habe ja gesagt, dass ich auch noch am Anfang bin. Und sie nehmen
mich halt noch nicht einmal auf Raids mit, so heißt es, wenn so ein Elementeil
spawnt und alle Fraktionen sich darum kloppen, es zu bekommen. Sie sagen,
da bin ich noch nicht reif für und haben dafür gute Argumente." Zeitkick
rang die Hände und blickte Mirash schließlich an. "Und dann kommst du,
bist gerade einmal eine Woche dabei, und bekommst eine mega komplexe Aufgabe. Und
zwar, weil du Scheiße gebaut hast."

Mirash runzelte die Stirn. "Was genau meinst du mit Scheiße."

"Ach komm, das weißt du selbst.", grummelte Zeitkick. "Flammenfinger
Zeittricks beibringen. Mich überreden, dich rauszulassen, obwohl du
noch nicht soweit bist."

"Letzteres ist falsch dargestellt.", hielt Mirash fest. Aber die
Worte drangen irgendwo zu ihm durch. As wehrte sich dagegen. As
wusste, dass as leicht auf emotionalen Druck ansprang und
toxisches Verhalten oft viel zu spät erkannte. Aber ob
so eine leichte Übertreibung da wirklich
hineinfiele? Zeitkick ging es mit der Situation nicht
gut, und das konnte Mirash verstehen. Das sollte sie
auch ausdrücken dürfen. Vielleicht war Mirash gerade besonders
empfindlich, weil Flammenfinger von Manipulation gesprochen
hatte. Flammenfinger hingegen hatte gute Charakterkenntnis,
vermutete Mirash, und konnte diese Worte sehr gut mit Absicht
fallen gelassen haben, um Mirash zu verunsichern. Aber mit
welchem Motiv?

"Es tut mir leid.", sagte Zeitkick. "Es hat sich so angefühlt. Du
wolltest dich quasi umbringen. Was meinst du, was das mit mir macht?"

"In einem Spiel mit Respawn.", gab Mirash zu bedenken.

"Du bist gerade auf Level 5 angekommen. Wenn du stirbst, verlierst du
ein Level. Und verlierst jeweils etwas vom Balken für deine
erlernten Fähigkeiten. Zeitleute gehen mit Sterben in diesem
Spiel nicht leichtfertig um." Zeitkick klang deutlich
harsch.

"Und warum hast du mir das nicht einfach stattdessen
erklärt? Sowas weiß ich doch nicht." Mirash stellte sich vor,
von vorn zu leveln. Das wäre anstrengend, das stimmte. Zeit
war schon zeitaufwendig. Das hatte as von vorherein
gewusst.

Zeitkick seufzte. "Ich vergesse immer, was du das alles noch
nicht weißt.", sagte sie. "Aber warum kannst du nicht ein
bisschen vertrauen? Dass wir über all die Jahre schon
gute, zum Element passende Methoden entwickelt haben." Sie
blickte wieder wie verloren in die Tiefe und fügte
ruhiger hinzu: "Zeit braucht Geduld. Was du gemacht hast, spricht
nicht dafür, dass du viel davon hättest, auch wenn du das eigentlich
immer behauptest."

"Wenn wir für in einer Woche verabredet sind, kann ich
sehr geduldig auf diesen Termin warten.", versuchte es
Mirash mit einer Metapher. "Wenn wir uns aber eigentlich
vorher sehen wollen und nichts im Wege stünde, außer, dass
wir eben vorher noch nicht verabredet sind, hat es aus
meiner Sicht nicht viel mit Ungeduld zu tun, in Frage
zu stellen, warum wir das vorher denn dann noch nicht
machen."

"Aber wir haben dir auch schon zich Mal erklärt, dass da
mehr als eine Regel hintersteckt.", erinnerte Zeitkick. Sie
hatte Recht. "Was sollte dich jetzt motivieren, das
Geschick und die Präzision zu erlernen, wenn du durch
den Workaround nun anders zum Ziel kommst? Und frag nicht
wieder, was so schlimm daran ist, anders zum Ziel zu
kommen. Es ist für den Schlüsseltrick nicht schlimm, aber
du kannst ohne Geschick und Präzision halt insgesamt weniger weit skillen."

"Es ist nicht zu spät.", sagte Mirash. "Oder habe ich
etwas grundlegend falsch verstanden? Wenn ich die Präzision
nachlernen möchte, dann kann ich das immer noch, sobald
ich beschließe, dass es dran ist, oder nicht?"

"Aber dann kommt irgendwann ein riesiger Berg auf dich zu, weil
du es vor dir herschieben wirst, bis du es wirklich brauchst.", sagte
Zeitkick. "Und du brauchst es erst, wenn die Geschicklichkeitsparts
sehr kompliziert werden, aber du Monate ausbilden müsstest, um
dann auszugleichen, was du mit der Präzision einfach so schaffen
könntest."

Mirash nickte. "Bist du schon bei so etwas angekommen?"

Zeitkick lachte bitter auf. "Nein. Noch lange nicht. Und das
ist der nächste Punkt. Ich trainiere so hart. Und du
kriegst eben einfach, was du willst, weil du, nun ja, ich
wiederhole es einfach: Scheiße baust."

Zeitkicks Wut schien trotzdem ein wenig verflogen zu sein. Mirash
zögerte mit sainer Frage, die nun aus sainer Sicht
anschloss, aber auch wieder neuen Unmut auslösen könnte. "Würdest
du mitkommen wollen? Oder hättest du die Aufgabe sogar
gern lieber selbst bekommen?"

Zeitkick schüttelte den Kopf. "Du bist so krass.", murmelte
sie und sprach dann wieder normal deutlich. "Ich meine, ich
bin froh, nicht gefragt worden zu
sein für so etwas. Nicht, weil ich es grundsätzlich für eine schlechte
Idee hielte. Aber auch ich möchte die Dramaturge nicht völlig
zerstört sehen. Und im Gegensatz zu dir hätte ich nicht den
Schneid gehabt, die Menge an Sprengstoff herunterzuhandeln, oder
den Plan so lange so hartnäckig mit festen Bedingungen
zu diskutieren, dass hinterher was rauskommt, was ich
vertreten könnte."

Mirash blickte Zeitkick lange nachdenklich an. War
das eine Träne in ihrem Gesicht? Mirash bot eine
Hand an, und Zeitkick verschränkte ihre Finger in
saine.

"Danke.", flüsterte sie, atmete einmal tief durch.

"Könntest du es denn nun in dieser Form vertreten?", fragte
Mirash.

Zeitkick nickte. "Ich hätte trotzdem zu viel Angst.", sagte
sie. "Wir sind dir im Zweifel eben alle egal. Ich aber
kann unglaublich schlecht damit umgehen, wenn meine eigene
Gruppe mich nicht mag."

Mirash lächelte und schüttelte den Kopf. "Ich kann im
Zweifel einen neuen Account anfangen.", erklärte
as. "Ich bin sehr gespannt auf die Dynamiken, die es
auslöst. Ich glaube daran, dass es nicht so gut ist, wenn
ein einzelner Charakter im Spiel so übermächtig ist, wie ihr
Flederschatten beschreibt. Dann wiederum, wenn die Zeit-Fraktion
bereit wäre, einen solchen Charakter sehr wütend auf sich zu
machen, spricht viel dafür, dass da eigentlich auch nicht
mehr viel rauszuholen ist, also Flederschatten schon
jede Situation nutzt, euch zu ärgern, die es gibt."

Zeitkick nickte energisch. "Flederschatten hält sich
noch ein bisschen zurück, aber wir glauben eben, dass
Flederschatten etwas plant. Mit besagter Provokation
haben wir die Hoffnung, Flederschatten aus der
Reserve zu locken, früher, als den Plänen des Charakters gut
tut, sodass wir uns vielleicht vor Schlimmerem retten
können. Wenn es noch nicht zu spät ist."

Mirash setzte sich in einen Schneidersitz und
streckte den Rücken durch. "Dinge, die mir
Angst machen:", zählte as nachdenklich auf. "Dass
ich in der Dramaturge Hausverbot für immer bekomme. Aber
ich bin dann wiederum sehr gespannt darauf, wie Flammenfinger
reagiert. Weil sey versichert hat, dass sey mich mag, und
mich auf die Versammlung mit diesem Plan schicken
wollte. Ich bin da extrem neugierig, ob das viel zu
weit geht, was ich mache." Mirash hielt einen zweiten
Finger hoch. "Ich habe die Angst, dass Flederschatten in Wirklichkeit
ein Mobbing-Opfer von euch ist."

Zeitkick lachte laut auf. "Diese Vorstellung allein!" Dann
wurde sie wieder ruhiger. "Im Ernst: Flederschatten ist der
mächtigste Charakter im Spiel. Das wirst du sehen, wenn Flederschatten
die Zeitbomben entschärft. Und Flederschatten wäre nicht ohne
Verbündete so mächtig geworden. Natürlich sind viele in dieser
Welt mit Flederschatten verfeindet, aber deine Frage mit dem
Mobbing ist ungefähr so, wie wenn du in irgendeinem historischen
Roman mit den negativ geframten Machthabenden sympathisierst, weil
auf ihrem Weg dahin, alles zu erobern und zu besitzen, Gruppen
auftauchen, die sie am liebsten enteignen wollen. Auf mehr oder
weniger brutale Art. Ich verstehe, wenn dir unser Weg zu
brutal ist, aber mit Mobbing hat das nichts zu tun."

Mirash nickte wieder. "Ja, so wirkte das auf mich auch. Aber
ich bin nun einmal neu."

---

Horanda gehörte zu den Personen, die so etwas wie einen Nebenberuf
neben dem Kämpfen für eine Fraktion hatten. Xier kümmerte
sich um Gärten. Xier hatte ein Händchen
für Nachtpflanzen und fand die meditative Arbeit sehr angenehm, sie
auszusähen, zu düngen, zu gießen, zu streicheln und ihnen gut
zuzureden -- was in diesem Spiel tatsächlich einen Effekt hatte. Pflanzen
waren nicht nur Zierde, auch wenn sie zu diesem Zweck durchaus sehr
taugten. Sie konnten auch zum Brauen von Tränken verwendet werden. Zwar
wurde in diesem Spiel nicht getrunken, aber Tränke konnten versprüht, auf
dem Boden verteilt oder über Personen gegossen werden und hatten
darauffolgend irgendeine Wirkung.

Horanda tauschte die gezüchteten Pflanzen gegen
Pflanzen anderer Leute, belieferte Personen, die
daraus tatsächlich Tränke brauten oder Heilsalben rührten
und xien damit bezahlten, oder bezahlte selbst damit für Rüstung,
Waffen und Kleider.

Einer xieser Gärten befand sich im Hinterhof der Dramaturge. Daher
kannte sich Horanda dort exzellent aus, kannte die Schwachstellen
des Gebäudes und ging dort so regelmäßig mit waffenähnlichen
Gegenständen wie Umgrabwerkzeug ein und aus, von
der Brücke durch die Dramaturge in den Hinterhof
und zurück, dass xier mit Schmuggel verhältnismäßig
wenig Probleme hatte. Xier hatte schon vor einem halben Jahr
herausgefunden, dass die Waffen- und sonstiges Zerstörungsmaterial
erschnuppernde Ziege nicht darauf ansprang, wenn xier mit einer
Ladung Dung, der auch noch mit den persönlichen Fäkalien der
Ziege vermengt war, in den Kulturraum trat, selbst wenn darin
gewisse Mengen Sprengstoff verbuddelt waren.

Horanda schob die Schubkarre in den Raum, als er sich schon ein
wenig gefüllt hatte, und ließ sie dort stehen, als xier
sich wie zufällig in ein Gespräch verwickeln ließ. Die Karre
war wegen des Geruchs mit einer Plane abgedeckt. Irgendwann
würde sich eine Person beschweren und xier würde sie wieder
hinausschieben, aber dann würde Holgem den Sprengstoff
daraus schon entnommen haben.

Es war fast ein wenig wie damals, als Mirash
hier das erste Mal war. Es war saine Aufgabe, Flammenfinger während
der kritischen Momente in Anspruch zu nehmen. Also
spielten sie wieder Slik und übten
dabei Zeittricks, bis der Laden zu voll würde. Holgem hatte
Mirash begleitet und spielte nun ihrerseits eine Partie Slik
mit der Ziege -- die ihre Figuren telekinetisch bewegte und
sagenhaft gut spielte. Mirash linste immer wieder auf das
Brett hinüber. Weil die Ziege die Figuren bewegte, spielte
Holgem das Brett. Das hieß, sie verschob immer eine Anzahl
von quadratischen Feldern horizontal oder vertikal, auf
denen gegebenenfalls Figuren der Ziege standen.

Flammenfinger grinste. "Holgem möchte mich wohl wieder einmal
beeindrucken.", sagte sey. "Wie in sehr alten Zeiten."

Holgem blickte Flammenfinger schmunzelnd ins Gesicht. "Muss
auch mal sein.", erwiderte sie. "Und irgendeine Beschäftigung
braucht meine leicht gelangweilte Wenigkeit ja, während ich
Zeit mit unseren Zuwachs verbringe."

"Bleibst du zur Vorstellung?", fragte Flammenfinger.

Holgem schüttelte den Kopf. "Oder besteht Hoffnung, dass Emeralone
auftritt? Oder gar Flederschatten?"

"Dachte ich es mir.", murmelte Flammenfinger und stand auf. "Wenig.
Emeralone bleibt verschwunden."

Mirash runzelte die Stirn, weil Flammenfinger nicht auf die
zweite Frage einging. Flammenfinger entfernte sich in Richtung
Eingang, weil die Ziege sich dorthin begeben hatte und an der eingetretenen
Person sehr ausgiebig schnupperte.

"So sieht das aus, wenn die Ziege Waffen findet.", murmelte
Holgem leise an Mirash gewandt, als Flammenfinger sich weit
genug entfernt hatte, um nicht mitzuhören. "Wo wir gerade
ein paar Momente für uns haben, dein Slik-Spiel sieht nicht
so gut aus, wie es könnte. Willst du heimlich einen Tipp
haben?"

"Nö, du?" Mirash kicherte. As hatte eigentlich keinen Plan, wie
Holgem ihre Lage verbessern könnte, aber es war ja auch nicht
die Rede von einem guten Tipp.

Holgem runzelte die Stirn und lehnte mit einem Kopfschütteln
ebenso ab, nun wieder den Blick konzentriert auf das eigene
Brett gerichtet.

Die Ziege plättete Holgem in drei weiteren Zügen. Einen
davon machte sie mit ihren telekinetischen Fähigkeiten quer
durch den Raum. Holgem seufzte und stand auf. "Ich mache mich dann
allmählich fertig."

Das war das Zeichen. Ab nun sollte Mirash möglichst zusehen,
Flammenfinger sehr abgelenkt zu halten, wenn sey nicht gerade
durch anderes ohnehin abgelenkt wäre. Sey war zum Spielbrett
zurückgekehrt, nachdem sich eine neue Gruppe Personen gemütlich
hingesetzt hatte, und verbeugte sich knapp zum Abschied in
Holgems Richtung.

"Ich wüsste gern mehr über Flederschatten.", leitete
as das Gespräch ein, als sie wieder halbwegs für sich waren. Das
war immerhin nicht nur als ablenkende Frage gedacht, sondern
as hatte sich das Thema gut im Vorfeld überlegt. "Ist Flederschatten
hier schon einmal aufgetreten?"

"Vor sehr langer Zeit." Flammenfinger setzte sich. Die
Worte klangen, als wollte sey damit ein Märchen einleiten. "Die
wenigsten der heute Spielenden haben Flederschatten noch auf der
Bühne erlebt. Holgem gehört gerade noch dazu. Seitdem ist viel passiert."

"Was ungefähr?", fragte Mirash.

"Hat dir deine Fraktion etwas zu diesem übermächtigen
Charakter erzählt und du möchtest
nun von mir wissen, wie stimmig deren Bild ist?", riet
Flammenfinger richtig.

Mirash nickte. "Und du redest nicht gern über Personen
in Abwesenheit.", fiel ihm ein.

"Ich rede durchaus über Flederschatten. So wie ich auch
über Emeralone reden würde. Oder über andere politisch
relevante Leute.", sagte Flammenfinger warm und
milde. Dann holte sey tief Luft. "Flederschatten ist ein
schwieriger Charakter. Ich versuche, meine Voreingenommenheit
mal, so gut es geht, herauszufiltern. Flederschatten
ist, das wurde dir wohl nicht vorenthalten, sehr mächtig. Diese
Person allein könnte vermutlich in die Dramaturge spazieren,
unbewaffnet und quasi nackt, und allein mit Zeit-Magie alle
Wachen hier fast simultan ausschalten. Ich habe tatsächlich
keine genaue Einschätzung darüber, wie weit die Fähigkeiten
reichen, aber das mindestens. Vielleicht könnte Flederschatten
es sogar mit Gate aufnehmen, aber ich glaube, Flederschatten
hat da kein Interesse dran."

Flammenfinger machte eine Redepause, in der as eine
Frage einwarf: "In welcher Weise bist du voreingenommen?"

"Haben sie dir auch gesagt, dass Flederschatten die Dramaturge
mitbeschützt?", fragte Flammenfinger.

"Angedeutet." Mirash schmunzelte. "Daher bist du positiv
voreingenommen?" Und als Flammenfinger mit einem
Lächeln um die Lippen nickte, "Nur deshalb?"

"Wir sind uns in vielen Punkten ganz und gar nicht eins. Ich werde über
meine persönliche Neigung nicht im Detail erzählen.", sagte
Flammenfinger. "Aber als Person, die in Fork mit der Dramaturge
einen einigermaßen neutralen, friedlichen Hafen
stellen möchte, und das Geschenk annehmen kann, dass dieser
unter dem Schutz der mächtigsten Person
im Spiel steht, habe ich wohl verständlicherweise eine
gewisse Motivation, mich nicht so gern in Machtkämpfe gegen
Flederschatten verwickeln zu lassen."

"Du sprichst von neutralem Hafen.", wandte Mirash ein. "Wenn
so ein mächtiger Charakter dermaßen wesentlich zum Schutz beiträgt, ist
der Hafen dann noch neutral."

Flammenfinger lächelte und nickte. "Es ist hier noch nie eine
Person zu Schaden gekommen, die auf den Tisch gesprungen wäre und
über Flederschatten geflucht hätte. Und ansonsten ist er
natürlich nicht völlig neutral. Wir haben eine strikte Agenda
gegen Diskriminierung, wie in vielen Räumen üblich. Und das
Waffenverbot stellt vielleicht auch eine gewisse Nicht-Neutralität
dar. So neutral, wie es dieser Rahmen aber hergibt, ist die
Dramaturge aber schon, und daran ändert Flederschattens Angebot nichts."

"Wie oft kommt so etwas vor? Dass eine Person auf den Tisch
springt und über Flederschatten flucht?" Mirash grinste nicht nur bei der
Vorstellung über irgendwelche anderen Personen, die dies
täten, sondern auch darüber, dass as etwas ganz ähnliches
plante. Das würde aufregend werden.

"Häufig.", antwortete Flammenfinger überraschend trocken. "Du hast
von den Elementeilen gehört?"

Mirash nickte. "Ich weiß noch nicht, was das ist, außer, dass
sie mit Buchstaben spezifiziert werden und es ihretwegen Raids gibt."

Flammenfinger nickte und neigte sich gespielt verschwörerisch
zu Mirash. "Es gibt sechs verschiedene Typen von Elementeilen. Sie
haben auch komplizierte Namen, wie: Alchemie-Elementeil,
Bibliotheks-Elementeil, Charakter-Elementeil,
Dämon-Elementeil, End-Elementeil und
Feenstaub-Elementeil. Vielleicht ist dir aufgefallen, dass dort
einfach Magie-assoziierte Wörter zu den Buchstaben A, B, C, D, E
und F gefunden wurden, also kürzen die meisten Spielenden sie
mit ihren Buchstaben ab." Flammenfinger ließ Mirash einen Moment zum Abspeichern
der neuen Information und blickte sich im Raum um.

Mirash folgte dem Blick und stellte beruhigt fest, dass es
recht voll war, Holgem also gut versteckt oder längst fertig
und verschwunden. As versuchte, dabei möglichst natürlich zu
wirken. Es war schließlich nicht ungewöhnlich, sich in einem Raum
umzublicken.

"Jedenfalls: Sobald eine Fraktion von jedem der sechs verschiedenen
Elementeile eines gefunden hat, kann sie sie zusammenfügen und
gegen eine neue Fähigkeit freischalten, die dann die ganze
Fraktion erlernen kann.", sagte Flammenfinger. Mit verspielt
halb ironisch überspitzer Stimme fuhr sey fort: "Und Flederschatten
spaziert bei manchen der Raids, die diesem Charakter in den Plan passen,
durch die Kämpfenden und sammelt sich da heraus, was Flederschatten
haben will. Das löst ein Gefühl von Machtlosigkeit aus, ein berechtigtes
wohl. Flederschatten lässt auch die Finger von vielen Raids, aber
besonders, wenn die Zeit-Fraktion profitieren würde, wendet
Flederschatten häufig das Blatt. Flederschatten hat allerdings
noch nie etwas eingelöst, sondern tauscht Elementeile
gegen Verträge, Material oder verschenkt sie einfach."

Mirash runzelte die Stirn. Was auch immer as davon halten
sollte, aber as machte sich erst einmal Gedanken zu der
Elementeil-Mechanik. "Ist das nicht schwierig mit
dem Balancing dann, -- also ohne Flederschatten und vom
Spiel aus --, weil starke Gruppen eher Elementeile
finden und in Folge dessen noch stärker werden?"

Flammenfinger schüttelte den Kopf. "Die neuen Fähigkeiten machen
nur vielfältiger, nicht stärker. Und sie müssen genau so gelernt
werden, wie andere Fähigkeiten auch."

"Ich verstehe, glaube ich.", murmelte Mirash. "Bilden die Elementeile
also nur den Anlass zu Raids, sind aber eher so etwas wie
Trophäen, und eigentlich geht es dabei um Käftemessen?"

"Um Kräftemessen auch, aber besonders die Optik-Fraktion arbeitet
lieber mit Fallen und Hinterhalten, nicht nur die Fraktion, aber
besonders die.", fasste Flammenfinger zusammen. "Du hast aber
richtig erkannt, dass das Spiel hier nur Anlässe für Kampf gibt, die
an sich aber nicht so dringend erreicht werden müssen, während
das simple Überleben beim Erkämpfen selbiger einen viel höheren
Effekt auf die Stärke einer Gruppe hat."

"Sterben und Respawnen ist in diesem Spiel eher unlustig?", schloss
Mirash ein weiteres Mal.

"Wenn du Schwerkraft wählst und nicht viel wertvolles Material
mit dir rumträgst, geht es. Sterben in einem Raid heißt eben, du
droppst alles, verlierst ein Level und ein bisschen deiner
Fähigkeiten wird abgeknappst, und du landest woanders.", erklärte
Flammenfinger. "Natürlich passiert das in Raids regelmäßig und
Leute haben auch ihre Reserve-Waffen und Reserve-Rüstung irgendwo
rumliegen, aber Sterben ist schon für viele mit hohen Kosten
verbunden."

"Und trotzdem gibt es die Straßenkämpfe.", hielt Mirash fest.

"Das ist Training. Es gehört zur Lehre, daher kommt Verlust
von Fähigkeiten hinterher auch wieder rein. Level gehen runter, das
stimmt.", sagte Flammenfinger. "Aber vielleicht hast du mitbekommen, dass
Waffen und Schilde hinterher oft zurückgegeben werden."

Mirash nickte. Daran erinnerte as sich.

As hätte gern mehr über Flederschatten erfragt, stellte aber
mit Schrecken fest, wie weit Zeit einfach so weitergelaufen war, und
stellte die letzte Frage, die as loswerden wollte, bevor as auf
einen Tisch springen und etwas verkünden würde. "Würde es dich
wundern, wenn ich mit Flederschatten schon Bekanntschaft
gemacht hätte?" Damit wollte Mirash ein wenig auffangen, dass
as zuvor über den Charakter ausgefragt hatte, aber gleich so tun
würde, als arbeiteten sie zusammen.

Flammenfinger grinste. "Es würde mich überraschen, wenn nicht."

Mirash hatte mit vielem gerechnet, aber nicht damit, mit einer Antwort
auf diese letzte Frage völlig aus dem Konzept gebracht zu
werden. As gefror gefühlt zu Stein, bemühte sich, das sein
zu lassen, aber starrte Flammenfinger irritiert an. Gleich, gleich, wenn
as nichts täte, würde die Dramaturge explodieren. Teile
davon. Mirash hatte als Alternative zum erhofften Eingreifen durch Flederschatten
durchaus eingeplant, einfach zu verraten, wo die Zeitbomben
versteckt wären -- sofern sich Holgem an die verabredeten Verstecke gehalten
hatte --, aber auch dazu müsste Mirash nun aufstehen.

"Du solltest Schauspiel noch üben." Flammenfingers warme Stimme war
fast ein Flüstern, und enthielt so viel schmunzel-flirtigen
Unterton, dass es Mirash abermals wie heißes Öl durchströhmte. Zu
heißes Öl.

"Ich mache dann mal schlimme Dinge.", sagte as leise, mit gefühlt verknoteten
Stimmbändern. Was auch immer Flammenfinger meinte, musste warten. Auch, wenn
Mirashs Gehirn schon an der Frage arbeitete und mögliche
Antworten sich dort breit machen wollten.

Flammenfinger stand auf und machte eine einladende Geste Richtung
Tisch, die durch eine Flammenflut im ganzen
Raum nicht zu übersehen war, die über sere Arme rann.

Mirash kletterte erst auf den Stuhl, der dabei gefährlich kippelte, und
dann auf den Tisch. Es brauchte ein paar wenige Momente vom
inneren Zittern, das Mirash selten so sehr spürte, bis die
Sicherheit durch as strömte, die Bühnen und Mittelpunkte ihm gaben. Die
meisten Blicke richteten sich auf as und es wurde still. Dann
sprach as in die Stimmung hinein: "Flederschatten beschützt die
Dramaturge, aber hat auch die Macht, sie zu zerstören. Das
solltet ihr wissen.", sprach Mirash. "Zum jetzigen Zeitpunkt befinden sich
Sprengsätze mit Zeitauslöser in der Dramaturge. Niemand verlässt
den Raum, wenn ihr da draußen nicht von Flederschatten
direkt abgefangen werden wollt." Mirash machte eine kurze
Kunstpause und blickte in irritierte Gesichter. Irgendeine
Person brüllte, sie habe ja immer vor Flederschatten gewarnt. Eine
andere Person hob einen scheinbar brennenden Mittelfinger. Das klappte
ja hervorragend. "Ich habe mich Flederschatten angeschlossen
und möchte von euch Informationen haben. Was plant ihr gegen
Flederschatten? Welche Bündnisse gibt es gegen Flederschatten? Wenn
ich in einer Viertelstunde nicht genug Information habe, fliegt
hier so einiges in die Luft." Mirash betonte den letzten Satz
mit Genuss. As hatte schon immer Mal so etwas in eine Menge rufen
gewollt.

Dann setzte sich Mirash wieder zu Flammenfinger an den Tisch. "So
etwa?", fragte as. "War das Schauspiel nun brauchbar?"

"Sehr interessant.", murmelte Flammenfinger. "Damit habe
ich offen gestanden nicht gerechnet. Du entschuldigst, dass
ich dich hier ein bisschen dir selbst überlasse, Antagone mit
der Ziege wegschicke und Leuten ihre Waffen aushändige?"

Für Mirash war nicht erkennbar, wie sich Flammenfingers
Einstellung ihm gegenüber geändert hätte. Aber nun überschlugen
sich die Ereignisse.

Ein paar Personen nahe des Augsgangs versuchten
die Dramaturge zu verlassen, wo allerdings
nicht Flederschatten, sondern Holgem mit ein paar anderen von
der Zeitfraktion warteten und gegen sie antrat. Die Flüchtenden despawnten
kurz und schmerzlos, ohne zu wissen, wie ihnen geschah. Die Türen
wurden wieder geschlossen. Leute
rannten halb panisch durch die Dramaturge, um sich bei Flammenfinger
ihre Waffen abzuholen oder sich zusammen zu finden. Viele
versuchten, sich schnell zu beraten. Es erklangen viele Flüche
gegen Flederschatten, die sich dem ersten anschlossen. Stimmen
wurden laut, dass so einem mächtigen Charakter niemals so viel
Freiraum gegeben werden dürfe. Andere Stimmen sagten, dass Mirash
sich niemals so einem Charakter anschließen würde, oder dass
hinter dem ganzen ein anderer Plan stecken würde. So eine
Einfallslosigkeit wäre Flederschatten nicht zuzutrauen. Irgendeine
laute Stimme, die trotzdem im Getümmel unterging, versuchte, eine
Analyse von Mirashs genauem Wortlaut zu vermitteln.

Waffen und Rüstungsteile wurden durch die Menge gereicht oder
geworfen. Leute baten Mirash schreiend darum, es sich
anders zu überlegen und den Ort der Bomben zu verraten. Andere
suchten nach ihnen.

Mirash hatte die eigene Bekanntheit unterschätzt. Das
passierte häufig. So viele hier sprachen as mit Namen
an. Einige kamen kurz an sainem Tisch vorbei und sagten ihm, was
sie von ihm hielten. Und plötzlich spürte as ein Messer
am Hals.

"Nicht umbringen!", schrie eine andere Stimme, panisch, alle
anderen übertönend. "Nicht bevor wir wissen, wo die Sprengsätze
sind!"

"Wir sterben ohnehin alle! Da kann ich auch vorher gestenreich
mitteilen, was ich davon halte.", schrie die Stimme der
Person mit dem Messer direkt neben Mirashs Kopf zurück.

Im nächsten Augenblick wurden die Arme locker, das
Messer plumpste an Mirashs Oberkörper entlang in sainen
Schoß, ohne as zu verletzen. Die Person löste sich
in nichts auf, fühlte sich dabei kurz sehr kalt an. Das
Messer, das Mirash nun
vorsichtig in die Hände nahm, war alles, was von ihr übrig
blieb. Mirashs Blick ruhte allerdings nicht lange auf der
verzierten Waffe, sondern wurde von einer riesigen, organenen Kröte
vereinnahmt, die mit einem Bauchplatscher auf
dem Slik-Brett vor ihm landete. Die Figuren flogen
zu allen Seiten weg. Die Superkraft dieser Kröte schien zu sein, dass
sie sich nicht lange damit aufhielt, eigentlich Matsch zu sein. Sie
regenerierte unscheinbar, während sie sich aufrappelte,
und thronte binnen kurzer Zeit in ihrer orangenen, schrumpligen
Schönheit auf dem Tisch. Mirash konnte nicht anders, als sie anzulächeln.

In der Dramaturge war es mit einem Mal überraschend leise
geworden. "Flederschatten.", murmelte eine Stimme, fast
andächtig. So gut wie alle Blicke und Waffen richteten
sich auf Mirash, das immer noch am Tisch saß.

"Die Dramaturge fliegt nicht in die Luft!", verkündete eine Stimme
aus dem Dachgebälk, aber als Mirash nach oben blickte, erkannte as, dass
die zugehörige Person schon auf dem Weg nach unten war. Sie
fiel erst langsam, dann in Zeitraffer und zum Schluss bremste
sie noch einmal rapide ab, sodass sich das kurze, schwarze
Cape über dem schwarzen Hoodie nur langsam legte. Auch der
schwere, lange, dunkelbraune Zopf brauchte überraschend lang, um
die Schwerkraft ernst zu nehmen. Pfeile und andere Geschosse prasselten
über ihnen aufeinander, wo Flederschatten eben noch gewesen
war, und rieselten anschließend, einen Kreis um den Tisch und
sie beide bildend, auf den Boden. Mirash konnte sich nicht so
schnell einen Reim darauf machen, wieso die Dinge nicht vertikal
fielen.

"Ich würde mich gern mit Mirash unterhalten.", verkündete die
Person laut. "Ich werde mir in diesem Fall nehmen, was ich
will, es sei denn Mirash möchte selber nicht. Denjenigen, die
lieber nicht so viel verlieren wollen, nicht gegen uns kämpfen
wollen, empfehle ich, die Dramaturge zu verlassen. Es besteht
dabei wahrscheinlich innerhalb der nächsten halben Stunde
keine Gefahr." Flederschatten knüpfte ein Bündel vom Gürtel
unterhalb des Hoodies los und warf es Flammenfinger zu, dey
es aus der Luft auffing. "Das ist der entschärfte Sprengstoff."

"Ich hatte ja ein bisschen auf eine leere Drohung gehofft.", sagte
Flammenfinger. Sey saß beinebaumelnd und mit geradem Rücken auf dem
Tresen, die weichen, flammenden Finger links und rechts um die
Kante gekrümmt.

"Es hätte die Wand eingerissen." Flederschatten deutete auf die Wand
gegenüber des Beckens. "Nicht die ganze Dramaturge."

"Was soll dieses Schauspiel?", schrie eine wütendende Stimme. "Du
willst doch nur so tun, als bräuchten wir dich nicht zu
fürchten, als würdest du uns alle retten wollen! Wem willst
du was vormachen?"

Ein Messer flog in Flederschattens Richtung, das aber plötzlich
in der Luft zitterte, einen Bogen flog, und die Person, die gesprochen
und es geschleudert hatte, in die Brust traf. Sie
despawnte. Mit einem Scheppern landete
auf dem Boden, was sie am Körper getragen hatte.

"Ich bin nicht hier, um irgendeinen positiven oder negativen
Eindruck zu machen.", versicherte Flederschatten.

"Gemetzel?", fragte eine Person aus der Mitte der Menge herausfordernd.

"Erst die rauslassen, die nicht wollen!", schrie eine andere, etwas
piepsigere Stimme.

Es hatten sich längst einige zur Tür hinausgeschlichen, aber
nun wurde das Gewusel wieder größer und lauter. Vielleicht ein Viertel
der Anwesenden versuchten -- nach und nach erfolgreich -- zur Tür
zu kommen, während wieder Rufe
durch den hohen Raum hallten, dieses Mal eher Kampfrufe. Codes, vermutete
Mirash, für Kampfstrategien, die aber Nicht-Eingeweihten wenig
sagten. As konnte nicht leugnen, gespannt zu sein, ob as diesen
Kampf überleben würde, oder wie er ablaufen mochte.

Flammenfinger kämpfte sich durch das Gewusel zu ihrem Tisch und richtete sich an
Flederschatten. "Ich werde die Dramaturge nicht verlassen.", sagte
sey.

"Möchtest du getötet werden?", fragte Flederschatten.

Mirash runzelte die Stirn. Niemand sonst bekam von diesem
seltsamen Gespräch mit. As sah gerade rechtzeitig hin, um
Flammenfingers Nicken mitzubekommen. "Ich greife dich vorher
an."

"In Ordnung.", sagte Flederschatten.

Flammenfinger verneigte sich sehr unauffällig, verschwand wieder
in Richtung Tresen und kletterte dieses Mal hinauf. Allein das Klettern sah
in diesem roten Kleid wunderschön aus. Flammenfinger wechselte
Kleider häufig, aber alle waren sie rot. Dieses hatte eine riesige,
an Kaminfeuer erinnernde Schleife, die das Kleid vorn zusammenhielt und es stark
taillierte, sowie eine eingebaute Polsterung, die den Hintern übertrieben
betonte.

"Gibt es noch Leute, die die Dramaturge verlassen möchten?", fragte
sey. Die klangvolle Stimme kam gegen den Lärm im Raum mühelos an. Niemand
meldete sich. Die kampfbereite Menge wurde wieder ruhiger und richtete ihre
Aufmerksamkeit nach vorn zum Tresen. Flammenfinger sprach in die
bedrohliche Stimmung: "Ich hebe für die nächsten zwei Stunden hiermit das
Kampfverbot in der Dramaturge auf."

Mirash fragte sich, warum Flammenfinger das tat, als das absolute
Chaos ausbrach -- für wenige Augenblicke. Mirash spürte, wie sain
Körper sich plötzlich nicht mehr so frei bewegen konnte wie
zuvor. Das Phänomen, das Holgem angekündigt hatte. Holgem hatte
allerdings auch angekündigt, dass Mirash während des Zaubers
entweder Flederschatten sehr schnell um sich herumwuseln
sehen, oder aber, wenn es ein noch fortgeschrittener
Zauber wäre, as lediglich ein Standbild sehen würde. Stattdessen
sah as, wie Flederschatten sich unbeeindruckt hinsetzte, als wäre
nichts, während im Hintergrund Waffen auf sie zufliegen wollten, aber
stattdessen aus der Luft fielen oder ihre Flugbahnen änderten. Flederschatten
stützte die Ellenbogen links und rechts neben der Kröte auf den Tisch
und legte das Kinn in die gefalteten Hände, während die Personen in
der Dramaturge reihenweise despawnten. Das halb
maskierte Gesicht lächelte as gelassen an. Dann war alles vorbei. Mirash
konnte sich wieder frei bewegen. Es schepperte und klapperte im ganzen
Raum, und als as sich umblickte war wirklich niemand mehr
da. Überall lagen Rüstungen, Waffen, Schilde am Boden und blieben
dort teils schaukelnd liegend, oder so vibrierend, wie
eine Münze, die angedreht worden war und irgendwann an Schwung verlor. Mit
einem vernehmlichen Muhen tauchte die Ziege wie aus dem Nichts auf
dem Tresen wieder auf.

"Sie bleibt diesem Ort nie lange fern, sondern teleportiert sich
nach kurzer Zeit zurück, wo auch immer sie sonst ist.", erklärte
Flederschatten gelassen.
