Sex
===

Flederschatten ließ sich Zeit. Sie standen sich gegenüber, etwa
drei große Schritte auseinander. Vier, wäre Mirash an die Wand
zurückgewichen. Flederschatten betrachtete as einfach und über
sainen Körper floss dabei ein Gefühl wie trockenes, heißes
Wasser in Schauern.

Ohne Vorwarnung, ohne dass Mirash viel davon gesehen hätte, stand
Flederschatten plötzlich direkt vor ihm. Mirash stockte der Atem, wie
kaum anders zu erwarten. As unterdrückte zunächst den Reflex, den
letzten Schritt an die Wand zurückzutreten. Die Nähe war gut. Aber
dann tat as es doch, in der Hoffnung, Flederschatten würde mitgehen
und as an die Wand drängen.

Stattdessen hob Flederschatten die Unterarme, sodass sie
einen 90° Winkel zu shanem Körper bildeten, Handflächen nach
oben gedreht. Sie berührten as nicht, dazu war die Geste nicht
gedacht. Sie forderte as heraus, dem Drang nachzugeben, die
Handflächen oder die weichen, fingerlosen Handschuhe zu
berühren. Mirashs Blick wurde von den Händen gefangen
genommen. Die zunächst ausgestreckten Finger entspannten sich, sodass
sie leicht gekrümmt, flackernden Schatten aufeinander warfen. So schön.

Mirash atmete zittrig ein und hob schließlich die eigenen Hände. As
erinnerte sich an das Gespräch über Vorfreude, über das Warten, und näherte
sich sehr langsam mit den eigenen Fingern. As wanderte mit den Händen
unter die Flederschattens, dicht an ihnen entlang, aber immer noch
ohne selbige zu berühren, und näherte dann
sehr langsam saine Daumen shanen. Mirash blickte mit einem sanften
Lächeln auf, als as das erste Mal eine heftige Unregelmäßigkeit
im Atem des Gegenübers wahrnahm. Flederschatten blickte zurück. Kein
Lächeln auf dem Mund, ein ruhiges Gesicht und doch voller Anspannung. Es
strahlte für Mirash eine gewisse Dominanz aus.

Weil Mirash nicht mehr dort hinsah, berührte sain einer Daumen
überraschend doch schon für einen Moment Flederschattens. As zuckte
zurück. Flederschatten nutzte die nun sicherere Distanz zwischen ihren
Daumen, um einen halben Schritt auf Mirash zuzugehen, weiterhin, ohne as
zu berühren. Aber nun spürte Mirash shanen Atem in sainem Gesicht.

Flederschatten war etwas kleiner als Mirash. Und roch gut. War das
die Virtualität, die einen eigenen Geruch für Flederschatten
erfand, oder übertrug sha den eigenen? Ob as für
sham auch gut roch? Nein, nicht die
Stirn küssen, das war noch nicht dran. Mirash atmete sehr
langsam. Hielt sich mühsam zurück. Aber die Daumen vorsichtig
berühren, das durfte as.

As hatte nicht bemerkt, wie as die Augen geschlossen und das Kinn
gesenkt hatte, und verband den Blick nun wieder mit Flederschattens, als
as sehr vorsichtig und dieses Mal absichtlich mit den Daumen die
Außenseite shaner Daumen berührte. Es war erleichternd. Und Flederschattens
bewusste Atemkontrolle war eine schöne Reaktion.

Mirash strich sehr zart an der Seite der Daumen entlang, versuchte, nicht
innezuhalten, aber ihre Haut haftete zwischen den Gelenken an den weichen
Stellen doch kurz aneinander. As strich einmal hin und einmal zurück, und
nahm dann von unten Flederschattens Hände vorsichtig in saine. Diese
Berührung reichte aus, um Mirashs Körper von innen zu elektrisieren, vielleicht
gerade weil sie so sachte und wenig war. Aber es waren die Hände! Mirash
liebte Hände. Der Stoff fühlte sich weich an, wie erwartet.
Flederschattens Fingerspitzen waren kühl.

"Wow.", hauchte Flederschatten.

Mirash blickte wieder auf. Nein, küssen war immer noch nicht dran. Mirash
sehnte sich nicht einmal danach, dass sich ihre Lippen aufeinander legten, sondern
überhaupt etwas dieser anderen, schönen Person mit den Lippen zu erforschen. As
riss sich zusammen. Und wanderte stattdessen vorsichtig mit den Händen an
der Seite der Handschuhe entlang zum Ärmelstoff des Hoodies. As strich so
vorsichtig über den Stoff, dass er sich zwar über die Haut darunter bewegen
musste, aber as selbst den Arm nicht spürte. Sain Blick hatte sich schon
wieder von alleine gesenkt und beobachtete nun den zitternden Brustkorb vor
ihm. Kurz darauf hörte as den nicht mehr ganz so leisen Atem aus dem halb geöffneten
Mund Flederschattens -- und grinste.

"Stört dich, wenn ich rede?", fragte sha. "Oder wäre dir Schweigen
lieber?"

Mirash hielt in der Bewegung inne, atmete einmal durch, um sich zu fokussieren, und
blickte Flederschatten wieder ins Gesicht. "Kommt darauf an, worüber du
reden willst." As merkte, dass saine Stimme keineswegs so gut
ansteuerbar war, -- womit as auch nicht so richtig gerechnet hatte. "Für mich
wäre nun eher ein schlechter Zeitpunkt, dir beim Darlegen über irgendwelche
thermodynamischen Gesetze oder so etwas zu folgen."

Flederschatten kicherte. "Ich mag dich spontan schon ganz schön gern.", sagte
sha. Und korrigierte sich wieder: "Ich habe eigentlich keine Ahnung, ob ich
dich mag. Das machen die Hormone mit mir."

"Oh, das kenne ich sehr gut.", warf Mirash ein.

"Ich fasse es noch nicht ganz." Flederschatten kicherte immer noch, schnaubte
sogar einmal. "War das eine komplizierte Art, mir mitzuteilen, dass ich
besser nicht über das Wetter reden soll?"

"Oh, ups, jetzt, wo du es sagst." Mirash grinste nun auch, runzelte aber
zugleich die Stirn. "Ist Thermodynamik immer Wetter?"

"Ich könnte darüber nun ins Detail gehen," -- Flederschatten betonte
den Satz überzogen, als hätte sha von Flammenfinger gelernt -- "aber
eigentlich war mir viel eher danach, dir was anderes zu sagen."

Allein diese Ankündigung. Mirashs Blick wanderte über dieses
schelmische Gesicht etwas unterhalb von sainem. "Sprich!", flüsterte as.

"Du bist sehr gut.", flüsterte Flederschatten zurück. "Die Langsamkeit
funktioniert."

"Das habe ich auch schon bemerkt. Du atmest so schön." Mirash lächelte
sanft, als das nicht unerwartete, etwas raschere Einatmen des Gegenübers
nicht ausblieb.

"Ich lasse dich noch ein bisschen machen." Flederschatten lächelte
und schloss die Augen, die Mirash gerade so unter der Maske
erkennen konnte. "Bevor ich übernehme."

Diese eine beiläufige Bemerkung, und Mirash brannte von innen. Saine
Hände zitterten, als as sie vorsichtig weiter über den Stoff
führte. Und dann waren da plötzlich Flederschattens Hände, die
die ganze Zeit ja schon in der Gegend verweilt hatten, auf
Mirashs Brust. Sie ruhten sich dort im Wesentlichen aus. Und
es war sehr schön, wie sie das taten, manchmal ein zartes Zucken
in den Fingern.

Mirash wanderte vorsichtig weiter die Ärmel hinauf, bis zu den
Schultern, und wieder hinab. Bei der Wiederholung der Bewegung
spürte as dieses Mal durch den Stoff hindurch, ertastete den
Körper darunter, mit sanften Berührungen. Mirash merkte, dass
as Beherrschung verlor, sich weniger Zeit ließ, mit den Händen nun
an Flederschattens Seiten herabzurinnen. Wieder erst so sanft, dass
as den Körper unter dem weichen, warmen Stoff nicht fühlte, und
dann mit einer Spur mehr Druck. As fühlte das weiche Fettgewebe. Sich
an die Hinweise zu Flederschattens Brüsten erinnernd, fuhr as
nur einmal sachte an den Seiten der Brüste entlang. Der Körper
zuckte und lehnte sich einen Moment sanft gegen Mirashs Vorderseite.
Flederschattens Hände verweilten dazwischen, Mirashs Rücken spürte
die Wand. Flederschattens Kopf war für einen Moment sehr dicht
neben sainem und as hielt sich abermals mit Gewalt davon
ab, nicht shanen Hals zu küssen.

Dann war diese Nähe wieder vorbei. Also, Nähe war noch da, aber
sha stellte sich wieder hin wie zuvor. Mirashs Finger klammerten sich an den
Saum von Flederschattens Hoodie, wie auch immer sie sich dahin
verirrt hatten. Mutig schob as die Hände vorsichtig
unter den Stoff. Spürte, wie die Bauchdecke sich unkontrolliert
hob und senkte.

Plötzlich ging einiges sehr schnell. Flederschattens Hände
wanderten zügig um Mirashs Brust herum auf sainen
unteren Rücken, zogen as in eine sehr feste Umarmung und
hoben as von den Füßen. Im nächsten Moment landeten sie
auf dem Bett, nebeneinander. Zeitmagie. Flederschatten löste vorsichtig
die Arme wieder und ließ Mirashs Atem Platz.

Mirash tat erst einmal gar nichts, außer beinahe zu
hyperventilieren. Gerade, als as wieder ein wenig Kontrolle
darüber wiedererlangen zu können glaubte, berührte Flederschatten
as sachte und fies schmunzelnd mit einem Zeigefinger auf der
Wange. "Kontrollverlust?", fragte sha leise.

"Hättest du wohl gern." Ja, Mirash fühlte sich, als wäre da
wenig Kontrolle, aber hätte as keine, würde Flederschatten nun
unten liegen und Mirashs Lippen wären irgendwo auf shanem Körper
verteilt.

Flederschatten lächelte vorsichtig. "Schon."

Das war zu viel. Fast. Mirash streckte den oberen Arm aus
und grub saine Hand unter Flederschattens Kleidung, schob sie den
Rücken hinauf. Nicht so sanft dieses Mal. Ertastete das
Rückengewebe, zog shan näher an sich heran. As blickte
sham ins Gesicht, während as sain Becken gegen shanes
lehnte und shan an diesem, ein Bein über shan legend, auf
den Rücken drehte. As legte die andere Hand auf Flederschattens
Stirn, verteilte das ganze eigene Körpergewicht auf der
Person unter sich und hielt Milimeter vor einer Berührung
mit den eigenen Lippen vor shanen inne.

Flederschatten wimmerte. Es war ein schönes, zartes Wimmern, ein
wundervolles Geräusch, das sich halb in Zittern und
Atem auflöste. Aber Mirash wollte es trotzdem rückversichert
haben, es nicht misszuverstehen. "Gut?"

"Ja." Der Laut war nicht weniger Wimmern als der Atem. "Wow."

Mirash lächelte und vergrößerte die Distanz ihrer Münder ein kleines
bisschen. Nur um Flederschatten nicht ausversehen zu küssen. Dann
streichelte as shanen Körper, wo as jeweils mit den Fingern
hinkam, schob shane Kleidung nach oben, dazu die Knie links
und rechts vom Körper unter sich platzierend. Flederschatten
erwiderte die Zärtlichkeiten. Shane Hände wanderten über Mirashs
Rücken, versuchten sich unter die zu enge Kleidung zu graben
und zogen sie Mirash schließlich über den Kopf.

Mirash fragte sich etwas spät, wie das eigentlich in diesem
Spiel mit Nacktheit war. Eigentlich war as per Spielmechanik
verpflichtet, Schwarz zu tragen, oder Schattierungen davon, und
hellgraue Wappen. Aber Flederschatten wusste bestimmt
Bescheid. Saine Gedanken wurden allerdings jäh davon unterbrochen, dass
Flederschatten etwas tat, was in eine klassische Reihenfolge
von Körpererforschen so überhaupt noch nicht passte. Shanes eine
Hand, die gerade noch Mirashs Kleidung wegsortiert hatte, landete
ohne Vorwarnung für einen Moment wie beiläufig zwischen sainen
Beinen. Mirash atmete heftig ein. Das entstandene Zittern ließ
sich kaum in den Griff kriegen.

"Zu frech?", fragte Flederschatten. "Du wolltest Übergriffigkeit. Ist
es zu viel?"

Mirash nickte, dann schüttelte as den Kopf. Die Gesten waren genau falsch
herum, aber Mirash konnte ärgerlicherweise gerade nicht
auf Stimmnutzung zugreifen. Flederschatten lag still und ruhig
unter ihm, während Mirash versuchte, sich wieder zu fassen. Shan
Gesicht vermittelte zunehmend Besorgnis.

"Grün.", hauchte Mirash schließlich. Das Gesicht unter ihm entspannte
sich wieder. "Und, oh my, bin ich nun sehr plötzlich sehr feucht."

Die Worte verfehlten ihre Wirkung nicht. Flederschatten sah
fast ein wenig erschreckt aus, ein ähnlicher Gesichtsausdruck, wie vorhin
beim Wimmern. Mirash bekam Lust, dieses Wimmern wieder
hervorzurufen. Aber das musste warten. Denn Flederschattens
streifte die Handschuhe ab und shane Finger wanderten
zu Mirashs Hosenbund. Die eine zog daran, sodass
sich ein Eingang bildete, die andere glitt in Mirashs Hose und
fühlte im Schritt nach.

Flederschatten nickte. "Dein Anzug weiß, wie feucht du bist? Oder
wird das anderswie nachgeneriert."

"Den Effekt hatte ich vorhin auch vergessen zu erwähnen.", murmelte
Mirash. "Der Anzug weiß es. Also meiner, nicht alle Anzüge
können das." Dann versagte as die Stimme, als as sich in
die Berühung der Finger hineinfühlte, die
sehr vorsichtig über sain Genital streichelten, nicht zwischen
die Lippen drangen, und senkte dabei den Kopf sehr langsam
herab, bis sich saine Lippen abermals nur Millimeter über
Flederschattens Mund befanden. Shan Atem zitterte und shane
Hand hielt inne. Durfte es nun soweit sein? Oder war es
viel schöner, es noch ein wenig aufzuheben?

Mirash verharrte hier einfach. Wartete ab, wie der Atem dabei
von selber schneller wurde. Ihrer beider. Näherte sich noch
ein winziges Stück, aber hatte den Abstand unterschätzt, zog
sich zurück, als as die versehentliche Berührung fühlte, die
nun auf der Haut nachbrannte. Da war es wieder, das Wimmern. Und
dieses Mal auch ein Fiepen.

Flederschatten zog die Hand aus Mirashs Hose, verteilte anschließend
beide auf Mirashs Rücken, wo sie sich wundervoll anfühlten, und drehte
Mirash wieder auf die Seite. Bei der Bewegung hatten sich ihre Gesichter
wieder etwas entfernt, aber sha näherte sich ohne viel Hadern wieder auf diese
gefährlich nahe Distanz. Shane Hände wanderten über Mirashs Rücken,
zärtlich, fester, wieder sanfter, wieder fester. Wanderten immer
wieder in Mirashs Nacken, schoben sich in sain Nackenhaar und wieder
hinaus. Auch Flederschatten passierte es ein oder zwei Mal, dass
sich ihre Lippen ausversehen berührten. Oder war es kein
Ausversehen? Sie waren so zart. Sie bliesen warmen Atem über
Mirashs Haut, und senkten sich dann sehr langsam an die Stelle
links oberhalb sainer Lippe neben der Nase, um dort einen ersten
sehr zärtlichen, vorsichtigen Kuss zu hinterlassen.

Dieses Mal wimmerte Mirash. As war gar nicht bewusst gewesen, dass
das zu sainem Geräuschrepertoire überhaupt gehörte.

Flederschattens Lippen lösten sich nur sehr kurz, bevor sha kaum
versetzt zum ersten den zweiten Kuss sanft in Mirashs Gesicht
ausformte -- von Drücken konnte kaum die Rede sein. Diese
zarten, weichen Lippen. Mirash erlaubte
sich, einen Kuss zu erwidern. Saine Lippen befanden sich
gerade an Flederschattens Kinn, knapp unterhalb shanes
Mundes, also landete er dort. Ihre Nasen berührten sich
inzwischen. Mirash genoss es tatsächlich, all diese Berührungen
ihrer Körper, die kein Kuss auf den Mund waren. Mirash mochte
Küsse auf den Mund, hatte aber auch schon mit Personen sexuell
interagiert, die solche nicht mochten, und auch ohne solche
hatte ihm nichts gefehlt. Hier war es ein Suchen. Ihre Nasen wanderten
umeinander. Mirash lernte haptisch das andere Gesicht
kennen. Und es war haptisch ein sehr schönes Gesicht. Ihre
Lippen lösten sich zunehmend weniger vom Gesicht der jeweils
anderen Person, sondern wanderten erforschend darüber, bis sie
sich ausversehen fanden, aber doch noch nicht dort verweilten,
weil es anderswo gerade noch interessanter war. Aber
dann wollte Mirash, vergrub die Hand im Nacken Flederschattens, hätte
die Finger gern im Haar verklammert, aber der Zopf war zu fest, und
erlaubte sich, Flederschatten bewusst und länger auf den
Mund zu küssen.

Flederschatten atmete rasch und lehnte sich in den Kuss, klammerte
sich an Mirash fest, shanerseits eine Hand im Nacken, allerdings
tatsächlich fest in Mirashs Haar verkrallt. Der Kuss hielt
ein paar Momente an, bis mit Flederschatten etwas passierte, was
Mirash zu kennen glaubte. Etwas passte für shan nicht. Mirash
löste sich aus dem Kuss und blickte shan fragend an. Aber
Flederschatten sagte nichts, küsste stattdessen noch einmal
sanft Mirashs Wange, wie vorhin.

"Es geht dir wirklich um das Vorher, und das Küssen selbst war
nicht so deins?", fragte Mirash.

Flederschatten hielt inne, aber antwortete nicht. Der Atem ging
viel ruhiger als vorhin. Mirash konnte völlig falsch
liegen, aber es wirkte auf as so, als wäre der längere Kuss
ein Entfremdungseffekt für Flederschatten gewesen. Etwas, was
shan aus dem Flow gerissen hatte.

"Ich glaube, du hast recht.", murmelte Flederschatten. "Magst
du mich noch einmal kurz auf den Mund küssen? Zum Testen
sozusagen?"

Mirash tat es. Näherte sich dabei wieder langsam, und beobachtete, was
es mit Flederschatten machte. Die Erwartung löste wie vorhin
ein leichtes Zittern oder einen Drang aus, der spürbar war, aber
nicht so stark wie vorhin. Flederschatten erwiderte auch diesen
Kuss, und Mirash bemühte sich, ihn von sich aus zügig zu
unterbrechen. Es sollte ja nur kurz sein.

"Und noch einmal länger?", bat Flederschatten.

Mirash folgte auch dieser Bitte mit dem gleichen Ergebnis wie
beim ersten langen Kuss, außer, dass kein so starker Flow
vorweg gegangen war.

Vielleicht war es gar keine so gute Idee gewesen, es zu probieren,
überlegte Mirash, als sie nun nebeneinander lagen und die
Leidenschaft von eben irgendwohin verschwunden war. Flederschatten
konnte gerade nicht mehr, und das war natürlich auch in
Ordnung.

"Ich glaube, die Vorstellung von Küssen ist für mich einfach gut, sehr
gut, und mein Gehirn trickst mich volle Kante aus, mir einzureden, ich
würde es mögen, aber ich mag es gar nicht.", überlegte sha.

Mirash lächelte. "Freches Gehirn.", sagte as. "Ich habe irgendwo am
Rande mal von so etwas gelesen."

"Ich meine, es war ja erst gerade eben. Der letzte Kuss ist wirklich
nicht lange her, und mein Gehirn schreit schon wieder, du
sollest mich küssen.", fuhr Flederschatten fort. "Aber es wird sich ja
nicht jetzt plötzlich gut anfühlen. Trotzdem denke ich bei diesem
ganzen Vorspiel die ganze Zeit daran, zu küssen, und das macht das
Vorspiel besonders schön. Es ist so albern! Warum bin ich so?"

Mirash strich mit zwei Fingern über Flederschattens Wange. "Sexualität
und Romantik und Kink und all dies sind sehr, sehr seltsam."

Flederschatten nickte bedeutungsschwer. Sha streckte eine Hand
aus, um über Mirashs Schultern sainen Körper hinabzustreicheln, und
unvermittelt landete shane Hand abermals in Mirashs Schritt. Es
hatte fast die selbe Wirkung, wie vorhin. Flederschatten grinste,
und betrachtete Mirash eingehend dabei, wie as in shanen Händen
zerfloss. Mirash rückte vorsichtig der Hand entgegen. Diese
aber hielt abrupt inne. Deshalb wohl.

"Teased du?", fragte Mirash.

"Ich bin noch nicht sicher.", antwortete Flederschatten. Sha rückte
wieder näher an Mirash heran. "Bei mir ist langsam die Luft raus. Aber
ich bin gerade nicht sicher, ob das für dich überhaupt Sex war."

"War es.", hauchte Mirash zittrig. "Sehr guter. Wenn du nicht mehr willst, ist
deine Hand da aber nicht so gut untergebracht."

Flederschatten grinste. "Ich glaube, wenn du Orgasmen magst, und
ich dazu im Stande bin, dich dahinzubringen, würde ich dich
gern noch ein bisschen dort anfassen."

Mirash wagte einige Augenblicke nicht, sich zu rühren. Dann nickte
as vorsichtig. "Wenn du das wirklich willst."

Flederschatten zog Mirashs Hose halb herunter, sodass sie Mirash
etwas oberhalb der Kniehöhe in der Bewegungsfreiheit einschränkte,
drehte as auf den Rücken und presste mit dem anderen Arm
sainen Oberkörper nach unten. Der eine verweilte unten. Die
Hand daran fand abermals den Weg vorsichtig zwischen Mirashs
Beine, erforschten dieses Mal die Vulvalippen, bis sie ihren
Platz dazwischen gefunden hatten und vorsichtig rieben. Mirashs
Atem zitterte, aber es fühlte sich nicht richtig an. As fragte
sich, wie as irgendwelche Hinweise geben konnte, aber wusste
selbst nicht genau, was falsch war -- und schüttelte schließlich
den Kopf.

Flederschatten fasste es wohl als Gelb auf, denn sha verharrte. Das
war auch nicht verkehrt. "Nicht gut?", fragte sha.

"Ich würde auch gern lieber aufhören.", sagte Mirash. Die Libido
verflog irgendwohin. "Magst du noch einmal in den Arm genommen
und auf die Wange geküsst werden, oder wäre das etwas, was
auch von der Luft betroffen ist, die raus ist?"

"Wenn ich dich aufgeräumt habe, darfst du das sehr gern.", beschloss
Flederschatten. Sha richtete sich auf, zog die Hand zwischen Mirashs
Beinen hervor und betrachtete sie skeptisch. "Dieses Spiel hat nicht
ernsthaft Körperschleim so perfekt implementiert, dass er an den Fingern
bleibt.", murrte sha. "Es ist kein Erotik-Spiel."

"Ich würde schon vermuten, dass wir nicht die ersten sind, die auf
diese Idee kommen.", überlegte Mirash. "Es passt zur Atmosphäre. Und
wo soll der Schleim sonst hin."

Flederschattens Gesicht entspannte sich. "Immerhin schmeckt
Schleim hier nach nichts. Und Infektionsgefahren oder so
gibt es auch nicht. Du erlaubst, dass ich
mich sauber lutsche?" Sha wartete Mirashs
Nicken ab, bevor sha sich den virtuellen Schleim von der Hand
leckte. Anschließend zog Flederschatten Mirash die Hose
wieder hoch und legte sich, als Mirash untenherum wieder ordentlich
gekleidet war, neben as. "Ich wüsste trotzdem gern: War dir
etwas sehr unangenehm?"

Mirash schüttelte den Kopf. "Ich fand alles sehr schön.", sagte
as. "Nur war irgendwann halt auch bei mir vorbei. Einfach so. Libido
weg. Und bei dir?"

"Ich glaube, du hast sehr deutlich gemerkt, was mir wie gut
gefallen hat.", sagte Flederschatten und lächelte.

Mirash nickte. "Ich glaube schon. Ich frage dich bei Gelegenheit
noch einmal detailliert, wie genau meine Interpretationen passen, wenn
das okay ist."

Flederschatten hob wieder zwei Finger, um Mirash näher zu winken. "Du
wolltest mich noch einmal umarmen und auf die Wange küssen."

Mirash nickte abermals, atmete einmal tief durch und tat es. Es
war anders als vorhin, der Sog war weniger, aber die weiche
Wangenhaut auf den Lippen war immer noch wunderschön, und Flederschattens
zufriedenes Atmen. "Danke."
